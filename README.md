This repository contains the relevant files for the analytic, and numerical, evaluation of the master integrals presented in the paper arXiv:2206.03848: 
# Files  
## analytics.m:  
* This file contains a substitution list for the master integrals m[i] in terms of their analytic expressions
* The analytic expressions for the integrals written in terms of MPLs are given explicitly as an epsilon expansion
* The analytic expressions for the integrals that admit a solution in terms of elliptic functions are given as an epsilon expansion. The coefficients of the expansion can be obtained through the files contained in the folder "elliptics".
## numerics.m:
* This file contains a substitution list for the master integrals m[i] in terms of their numerical evaluation.
## elliptics (folder):  
* This folder contains a number of files FI_k.m, which give replacements lists for the coefficients of the epsilon expansion of integral I in terms of elliptic functions.
* This directory contains a README-file with a description of the functions appearing in the files FI_k.m.
## diffexp (folder):
* This folder contains the relevant definitions to evaluate numerically the masters integrals of topologies 3,4 and 5 using diffexp, as described in section 4.1 of the paper.
