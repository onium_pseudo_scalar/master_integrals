This folder contains the relevant definitions to evaluate numerically the masters integrals of topologies 3,4 and 5 using diffexp, as described in section 4.1 of the paper.
## basisTopo files:
* This files contain the definitions of the master integrals basis used to within the diffexp setup;
* lambda is the parameter with respect to the differential equations are obtained;
* ep is the dimensional regularisation parameter;
* mq2 is the quark mass squared.
## inputTopo files:
* this files contain the definitions of topologies 3, 4 and 5
