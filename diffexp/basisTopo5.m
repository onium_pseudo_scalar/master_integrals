(* ::Package:: *)

{ep^2 topo5[{0,2,0,2,0,0,0}],ep^2 lambda2 topo5[{2,2,1,0,0,0,0}],ep^2 lambda2 topo5[{0,2,1,0,2,0,0}],ep^2 Sqrt[-lambda2] Sqrt[-lambda2+4 mq2] (1/2 topo5[{0,2,1,0,2,0,0}]+topo5[{0,2,2,0,1,0,0}]),ep^3 lambda2 topo5[{1,2,1,1,0,0,0}],ep^2 Sqrt[-lambda2] Sqrt[-lambda2-4 mq2] (-((ep topo5[{0,2,0,2,0,0,0}])/(2 (1+2 ep) mq2))+lambda2 topo5[{2,2,1,1,0,0,0}]),ep^3 lambda2 topo5[{0,2,1,1,1,0,0}],ep^4 lambda2 topo5[{1,1,0,1,1,1,0}],ep^4 lambda2 topo5[{1,1,1,1,1,0,0}],ep^4 lambda2^2 topo5[{1,1,1,1,1,1,0}],ep^4 lambda2^2 (lambda2+16 mq2) topo5[{1,2,1,1,1,1,0}]}
