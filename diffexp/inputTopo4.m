(* ::Package:: *)

Internal = {q1, q2};
External = {k1, k2};
Propagators = {q1^2-mq2,(q1+k1)^2-mq2,q2^2-mq2,(q1+q2)^2,(k1/2+k2/2-q2)^2,(q2+q1+k1/2-k2/2)^2-mq2,(q1+k2)^2};
Replacements ={k1^2->2*s,k2^2->0,k1*k2->2*mq2-lambda2/2};
