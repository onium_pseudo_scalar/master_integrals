(* ::Package:: *)

Internal = {k, l};
External = {p1, p2};
Propagators = {(k-p1)^2,(l-p1)^2-m2,(k+p2)^2,(k-l+p2)^2-m2,(k-l)^2-m2,l^2-m2,k^2}
Replacements ={p1^2->0,p2^2->0,p1 p2->lambda2/2} ;
