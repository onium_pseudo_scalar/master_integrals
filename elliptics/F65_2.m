(* Created with the Wolfram Language : www.wolfram.com *)
{F[65, 2] -> -49/2 - Pi^2/12 + (7*Pi^4)/720 + 
   ((((-38*I)*5^(1/4))/Pi - (I/3)*5^(1/4)*Pi)*IEI[{3, 6, 1, 0}, TauA])/
    EllipticK[(5 + Sqrt[5])/10] + ((((-38*I)*5^(1/4))/Pi - (I/3)*5^(1/4)*Pi)*
     IEI[{3, 6, 4, 3}, TauA])/EllipticK[(5 + Sqrt[5])/10] + 
   ((((95*I)*5^(1/4))/Pi + ((5*I)/6)*5^(1/4)*Pi)*IEI[{3, 6, 5, 3}, TauA])/
    EllipticK[(5 + Sqrt[5])/10] + 
   (((-1/6*5^(1/4) - (19*5^(1/4))/Pi^2)*EisensteinH[2, 1, 0, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + ((5^(1/4)/6 + (19*5^(1/4))/Pi^2)*
       EisensteinH[2, 2, 0, 1, TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     ((5^(1/4)/6 + (19*5^(1/4))/Pi^2)*EisensteinH[2, 2, 1, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + (-7/(9*5^(1/4)) - 46/(3*5^(1/4)*Pi^2))*
      EllipticK[(5 + Sqrt[5])/10])*IEI[{0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((-1/6*5^(1/4) - (19*5^(1/4))/Pi^2)*EisensteinH[2, 1, 0, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + ((5^(1/4)/6 + (19*5^(1/4))/Pi^2)*
       EisensteinH[2, 2, 0, 1, TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     ((5^(1/4)/6 + (19*5^(1/4))/Pi^2)*EisensteinH[2, 2, 1, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + (-7/(9*5^(1/4)) - 46/(3*5^(1/4)*Pi^2))*
      EllipticK[(5 + Sqrt[5])/10])*IEI[{0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((5*5^(1/4))/12 + (95*5^(1/4))/(2*Pi^2))*EisensteinH[2, 1, 0, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + 
     (((-5*5^(1/4))/12 - (95*5^(1/4))/(2*Pi^2))*EisensteinH[2, 2, 0, 1, 
        TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     (((-5*5^(1/4))/12 - (95*5^(1/4))/(2*Pi^2))*EisensteinH[2, 2, 1, 0, 
        TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     ((7*5^(3/4))/18 + (23*5^(3/4))/(3*Pi^2))*EllipticK[(5 + Sqrt[5])/10])*
    IEI[{0, 0, 0, 0}, {3, 6, 5, 3}, TauA] - 
   (20*5^(1/4)*IEI[{2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (20*5^(1/4)*IEI[{2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (50*5^(1/4)*IEI[{2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (10*5^(1/4)*IEI[{2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (10*5^(1/4)*IEI[{2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (25*5^(1/4)*IEI[{2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (30*5^(1/4)*IEI[{2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (30*5^(1/4)*IEI[{2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (75*5^(1/4)*IEI[{2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (20*5^(1/4)*IEI[{2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (20*5^(1/4)*IEI[{2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (50*5^(1/4)*IEI[{2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA] + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA] + 
   (((-25*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   (((-5*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA] + 
   (((-5*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA] + 
   ((((25*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((25*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((25*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/3)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   (((-15*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((26*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA] + 
   (((-15*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((26*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA] + 
   ((((75*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((75*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((75*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((13*I)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA] + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA] + 
   (((-25*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   (((-25*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   (((-5*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((-5*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((5*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((25*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((25*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((25*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/3)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((2*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((2*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((5*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((15*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   (((-15*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((26*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((-15*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((15*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((26*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((75*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((75*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((75*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((13*I)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((15*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((18*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((18*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((45*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((10*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((10*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((52*I)/3)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   (((-25*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((25*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((26*I)/3)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((180*I)*5^(1/4)*IEI[{4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((135*I)*5^(1/4)*IEI[{4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((135*I)*5^(1/4)*IEI[{4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((180*I)*5^(1/4)*IEI[{4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((135*I)*5^(1/4)*IEI[{4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((18*I)*5^(1/4)*IEI[{4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((18*I)*5^(1/4)*IEI[{4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((45*I)*5^(1/4)*IEI[{4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (21*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (252*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (168*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (168*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((90*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (84*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (126*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (126*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((135*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (63*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (252*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (252*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (126*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (126*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((135*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (63*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (252*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (252*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (168*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (168*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((90*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (84*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (126*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (126*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((135*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (63*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (504*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (252*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (21*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (21*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (14*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (42*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (21*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (28*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (14*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (56*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (28*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((7*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((14*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((14*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((14*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((14*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((14*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((14*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((14*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((14*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((14*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((14*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((14*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((14*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((14*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((14*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((14*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((14*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((14*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((14*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((14*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((14*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((14*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((-7*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((7*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((7*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^3) - 
       ((7*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^3) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((7*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((7*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((7*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       ((((-3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((-7*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((7*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((7*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^3) - 
       ((7*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^3) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((7*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((7*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((7*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       ((((-3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-28*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((28*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (28*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (28*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((28*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((28*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((28*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-28*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((28*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (28*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (28*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((28*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((28*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((28*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-28*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((28*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (28*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (28*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((28*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((28*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((28*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-28*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((28*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((28*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (28*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (28*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((28*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((28*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((28*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((14*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((14*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((14*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((14*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((14*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((14*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((14*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((14*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((14*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((14*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((14*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((14*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((14*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((14*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((-21*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((21*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((21*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((21*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (21*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (21*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((21*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((21*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((21*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       ((((9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       ((((-9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((21*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((21*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((21*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((21*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (21*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (21*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((21*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((21*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((21*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((63*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((63*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((63*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((63*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (63*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (63*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((63*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((63*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((63*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((63*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((63*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((63*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((63*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (63*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (63*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((63*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((63*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((63*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((63*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((63*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((63*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((63*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (63*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (63*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((63*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((63*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((63*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((84*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((84*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((84*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((84*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (84*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (84*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((84*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((84*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((84*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((84*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((84*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((84*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((84*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (84*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (84*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((84*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((84*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((84*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((252*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((252*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (252*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (252*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((252*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((252*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((252*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (252*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (252*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((252*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((252*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((252*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (252*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (252*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((252*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((252*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((252*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (252*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (252*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((252*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((252*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((252*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (252*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (252*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((252*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((252*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((252*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((252*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (252*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (252*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((252*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((252*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/6)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/6)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((7*I)/2)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/2)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/2)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (((7*I)/2)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (((7*I)/2)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/2)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/2)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((7*I)/2)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/2)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((7*I)/2)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (((7*I)/2)*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (((7*I)/2)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/2)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/2)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10]) + 
   (IEI[{4, 6, 5, 3}, TauA]*((-9*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
      (9*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (9*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (9*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((9*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((9*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (9*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (9*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (9*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 1, 1}, TauA]*((-27*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (27*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (27*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (27*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((27*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((27*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (27*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 3, 1}, TauA]*((-27*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (27*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (27*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (27*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((27*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((27*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (27*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 5, 1}, TauA]*((-27*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (27*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (27*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (27*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((27*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((27*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (27*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 1, 0}, TauA]*((-36*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (36*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (36*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (36*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((36*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((36*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (36*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 4, 3}, TauA]*((-36*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (36*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (36*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (36*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((36*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((36*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (36*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 0, 1}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 1, 2}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 2, 1}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 3, 2}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 4, 1}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 5, 2}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + IEI[{2, 6, 3, 3}, TauA]*
    (EllipticK[(5 - Sqrt[5])/10]*((((-14*I)/3)*Pi)/(Sqrt[3]*5^(1/4)) - 
       ((14*I)*Sqrt[3]*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       ((14*I)*Sqrt[3]*G[-1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       ((14*I)*Sqrt[3]*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       ((14*I)*Sqrt[3]*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (14*Sqrt[3]*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (14*Sqrt[3]*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((14*I)*Sqrt[3]*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((14*I)*Sqrt[3]*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       ((14*I)*Sqrt[3]*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (((-2*I)*5^(1/4)*Pi^2)/Sqrt[3] + 
       (3*5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (3*5^(1/4)*(1 - (4*I)*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1])/2 + 
       (3*5^(1/4)*(1 + (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (3*5^(1/4)*(-1 + (4*I)*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1])/2 + 
       (3*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*(1 - (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*(-1 - (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*Log[2]*((-4*I)*Sqrt[3]*Log[4] + (1 + (4*I)*Sqrt[3])*
           Log[3 - I*Sqrt[3]] + (-1 + (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/2)/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((-I)*5^(1/4)*Pi)/Sqrt[3] - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(2*I - 3*Sqrt[3])*Pi)/9 + 
         (15*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (3*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (15*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
            2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), -1, 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
            1])/(4*Pi^2) + (((3*I)/2)*5^(1/4)*GI[-1, -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 - (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
          Pi^2 - (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/4)*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 + (3*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (3*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (2*Pi^2) - (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), 1]*Log[2])/(4*Pi^2) - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi^2) + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((3*Sqrt[3]*5^(1/4))/Pi - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi - (((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi + (3*5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[-2/(1 + I*Sqrt[3]), -1, 1]*((((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          ((((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + (3*5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(4*Pi^2) - 
         (3*Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-3*I)*Sqrt[3]*5^(1/4))/Pi + (((3*I)/2)*5^(1/4)*
             (5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*((-3*Sqrt[3]*5^(1/4))/Pi + 
           (3*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/
            (2*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi - (3*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 + (I/2)*Sqrt[3], -1]*(((-3*I)*Sqrt[3]*5^(1/4))/Pi - 
           (((3*I)/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((I*5^(1/4)*Pi)/Sqrt[3] + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/9 - 
         (15*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (3*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (15*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
            2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
            1])/(4*Pi^2) - (((3*I)/2)*5^(1/4)*GI[-1, -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 + (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
          Pi^2 + (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/4)*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 - (3*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (3*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (2*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), 1]*Log[2])/(4*Pi^2) + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi^2) - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-3*Sqrt[3]*5^(1/4))/Pi - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi + (3*5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*((((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          ((((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) - (3*5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (3*Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (3*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*(((3*I)*Sqrt[3]*5^(1/4))/Pi - 
           (((3*I)/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - 
                (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((3*I)*Sqrt[3]*5^(1/4))/Pi + (((3*I)/2)*5^(1/4)*
             (Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 + (I/2)*Sqrt[3], -1]*((3*Sqrt[3]*5^(1/4))/Pi + 
           (3*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            (2*Pi^2)))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((I*5^(1/4)*Pi)/Sqrt[3] + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/9 - 
         (15*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (3*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (15*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
            2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
            1])/(4*Pi^2) - (((3*I)/2)*5^(1/4)*GI[-1, -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 + (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
          Pi^2 + (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/4)*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 - (3*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (3*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (2*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), 1]*Log[2])/(4*Pi^2) + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi^2) - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-3*Sqrt[3]*5^(1/4))/Pi - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi + (3*5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*((((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          ((((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) - (3*5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (3*Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (3*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*(((3*I)*Sqrt[3]*5^(1/4))/Pi - 
           (((3*I)/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - 
                (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((3*I)*Sqrt[3]*5^(1/4))/Pi + (((3*I)/2)*5^(1/4)*
             (Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 + (I/2)*Sqrt[3], -1]*((3*Sqrt[3]*5^(1/4))/Pi + 
           (3*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            (2*Pi^2)))/EllipticK[(5 + Sqrt[5])/10]) + 
     EllipticK[(5 + Sqrt[5])/10]*((-14*(-2*I + 3*Sqrt[3])*Pi)/(27*5^(1/4)) + 
       (13*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (13*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (13*G[0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (13*G[0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (13*G[(I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) + 
       (7*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (7*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (7*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (7*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (7*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) - 
       (7*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (7*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) - (7*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (5^(1/4)*Pi^2) - (7*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) + (7*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(5^(1/4)*Pi^2) - (7*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^2) + (7*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (7*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
       (14*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (14*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (7*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
       (7*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (7*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (7*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi^2) - 
       (7*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (7*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) + 
       (7*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (7*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (7*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) + 
       (7*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (7*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (7*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi^2) + 
       (7*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (7*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (7*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*5^(1/4)*Pi^2) + 
       (7*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi^2) - 
       (7*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) - (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          1])/(2*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
        (5^(1/4)*Pi^2) + (7*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) + (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/(2*5^(1/4)*Pi^2) + (7*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) + 
       (7*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*5^(1/4)*Pi^2) + 
       ((7*I)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       ((7*I)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       ((7*I)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((7*I)/2)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - ((7*I)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((7*I)/2)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       ((7*I)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) + 
       (((7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - ((7*I)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - ((7*I)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) - (((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + ((7*I)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (7*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (7*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (7*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (7*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi^2) - (7*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (7*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) + 
       (7*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) - 
       (7*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
        (2*5^(1/4)*Pi^2) + (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
         Log[2])/(2*5^(1/4)*Pi^2) - ((7*I)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) + ((7*I)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (((7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) + 
       (((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (7*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (7*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*5^(1/4)*Pi^2) + 
       (((7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (7*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*5^(1/4)*Pi^2) + 
       G[0, 2/(-1 + I*Sqrt[3]), 1]*((14*Sqrt[3])/(5^(1/4)*Pi) - 
         (7*Log[2])/(5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((((-7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (7*GR[-1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
         (7*Log[2])/(5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-14*Sqrt[3])/(5^(1/4)*Pi) - (((7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
         (7*Log[2])/(5^(1/4)*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        ((14*Sqrt[3])/(5^(1/4)*Pi) + (7*Log[2])/(5^(1/4)*Pi^2)) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*((((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
         (7*Log[2])/(5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        ((-14*Sqrt[3])/(5^(1/4)*Pi) + (((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
         (7*Log[2])/(5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((-7*I)/2)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((7*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (7*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
         (7*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi^2) - 
         (7*Log[2]^2)/(2*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((7*I)/2)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((7*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (7*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
         (7*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi^2) + 
         (7*Log[2]^2)/(2*5^(1/4)*Pi^2)) + 
       (Log[2]*(7*Log[(3 - I*Sqrt[3])/2]^2 + 26*Log[3 - I*Sqrt[3]] - 
          7*Log[(3 + I*Sqrt[3])/2]^2 - 26*Log[3 + I*Sqrt[3]]))/
        (2*5^(1/4)*Pi^2) - (7*Sqrt[3]*Log[2]*(Log[256] - 
          4*(Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])))/(2*5^(1/4)*Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*(((-14*I)*Sqrt[3])/(5^(1/4)*Pi) + 
         (I*(13 + 7*Log[3 + I*Sqrt[3]] - 7*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        ((-14*Sqrt[3])/(5^(1/4)*Pi) + (13 + 7*Log[3 + I*Sqrt[3]] - 
           7*Log[6 - (2*I)*Sqrt[3]])/(5^(1/4)*Pi^2)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*(((-14*I)*Sqrt[3])/(5^(1/4)*Pi) - 
         ((7*I)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
        ((-14*Sqrt[3])/(5^(1/4)*Pi) + (7*(-Log[3 - I*Sqrt[3]] + 
            Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)))) + 
   IEI[{2, 6, 2, 0}, TauA]*(EllipticK[(5 - Sqrt[5])/10]*
      ((((-14*I)/9)*Pi)/(Sqrt[3]*5^(1/4)) - 
       ((14*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((14*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((14*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((14*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((14*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((14*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((14*I)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)*Pi)) + ((((-2*I)/3)*5^(1/4)*Pi^2)/Sqrt[3] + 
       (5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (5^(1/4)*(1 - (4*I)*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1])/2 + 
       (5^(1/4)*(1 + (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (5^(1/4)*(-1 + (4*I)*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1])/2 + 
       (5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*(1 - (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*Log[2]*((-4*I)*Sqrt[3]*Log[4] + (1 + (4*I)*Sqrt[3])*
           Log[3 - I*Sqrt[3]] + (-1 + (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/2)/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((I/3)*5^(1/4)*Pi)/Sqrt[3] + 
          (I*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            -1, 1])/(4*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) + ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(4*Pi^2) - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*(-((Sqrt[3]*5^(1/4))/Pi) - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/4*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi - 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + (5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi + 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/4*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + (5^(1/4)*Log[2]*
           (-Log[(3 - I*Sqrt[3])/2]^2 - 10*Log[3 - I*Sqrt[3]] + 
            Log[(3 + I*Sqrt[3])/2]^2 + 10*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*((I*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/
            Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*((I*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*((Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)))/
        EllipticK[(5 + Sqrt[5])/10]) + EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((I/3)*5^(1/4)*Pi)/Sqrt[3] + 
          (I*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            -1, 1])/(4*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) + ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(4*Pi^2) - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*(-((Sqrt[3]*5^(1/4))/Pi) - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/4*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi - 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + (5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi + 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/4*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + (5^(1/4)*Log[2]*
           (-Log[(3 - I*Sqrt[3])/2]^2 - 10*Log[3 - I*Sqrt[3]] + 
            Log[(3 + I*Sqrt[3])/2]^2 + 10*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*((I*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/
            Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*((I*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*((Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)))/
        EllipticK[(5 + Sqrt[5])/10]) + EllipticK[(5 + Sqrt[5])/10]*
      ((-14*(-2*I + 3*Sqrt[3])*Pi)/(81*5^(1/4)) + 
       (13*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (13*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (13*G[0, (2*I)/(-I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (13*G[0, (-2*I)/(I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (13*G[(I/2)*(I + Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (7*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) - (7*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) - (7*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) + (7*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(6*Pi^2) - 
       (14*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(6*Pi^2) + 
       (7*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (7*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (7*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (7*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (7*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (7*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(6*5^(1/4)*Pi^2) + 
       (7*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        (6*5^(1/4)*Pi^2) - (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          1])/(6*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
        (3*5^(1/4)*Pi^2) + (7*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) + (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -1, 1])/(6*5^(1/4)*Pi^2) + 
       (7*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) + 
       (7*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(6*5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((7*I)/6)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((7*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((7*I)/6)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) + 
       (((7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) - (((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (7*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (7*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi^2) - (7*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) - (7*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
       (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
        (6*5^(1/4)*Pi^2) + (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
         Log[2])/(6*5^(1/4)*Pi^2) - (((7*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) - (((7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) + 
       (((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (7*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) + (7*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(6*5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(6*5^(1/4)*Pi^2) + 
       (((7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (7*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(6*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(6*5^(1/4)*Pi^2) + 
       G[0, 2/(-1 + I*Sqrt[3]), 1]*(14/(Sqrt[3]*5^(1/4)*Pi) - 
         (7*Log[2])/(3*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((((-7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (7*GR[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
         (7*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (-14/(Sqrt[3]*5^(1/4)*Pi) - (((7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (7*GR[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
         (7*Log[2])/(3*5^(1/4)*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        (14/(Sqrt[3]*5^(1/4)*Pi) + (7*Log[2])/(3*5^(1/4)*Pi^2)) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*((((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
         (7*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (-14/(Sqrt[3]*5^(1/4)*Pi) + (((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
         (7*Log[2])/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((-7*I)/6)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((7*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (7*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
         (7*GR[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi^2) - 
         (7*Log[2]^2)/(6*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((7*I)/6)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((7*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (7*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
         (7*GR[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi^2) + 
         (7*Log[2]^2)/(6*5^(1/4)*Pi^2)) + 
       (Log[2]*(7*Log[(3 - I*Sqrt[3])/2]^2 + 26*Log[3 - I*Sqrt[3]] - 
          7*Log[(3 + I*Sqrt[3])/2]^2 - 26*Log[3 + I*Sqrt[3]]))/
        (6*5^(1/4)*Pi^2) - (7*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
            Log[3 + I*Sqrt[3]])))/(2*Sqrt[3]*5^(1/4)*Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*((-14*I)/(Sqrt[3]*5^(1/4)*Pi) + 
         ((I/3)*(13 + 7*Log[3 + I*Sqrt[3]] - 7*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        (-14/(Sqrt[3]*5^(1/4)*Pi) + (13 + 7*Log[3 + I*Sqrt[3]] - 
           7*Log[6 - (2*I)*Sqrt[3]])/(3*5^(1/4)*Pi^2)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*(-14/(Sqrt[3]*5^(1/4)*Pi) - 
         (7*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2)) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
        ((-14*I)/(Sqrt[3]*5^(1/4)*Pi) - (((7*I)/3)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2))) + 
     EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((-1/3*I)*5^(1/4)*Pi)/Sqrt[3] - 
          (I*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(2*I - 3*Sqrt[3])*Pi)/27 + 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            -1, 1])/(4*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) + 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) - ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(4*Pi^2) + ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/4*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
            Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
          ((Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
            Pi^2 + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/4*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(4*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-I)*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*
             (5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*(-((Sqrt[3]*5^(1/4))/Pi) + 
           (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/
            (2*Pi^2)) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-I)*Sqrt[3]*5^(1/4))/Pi - ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + (5^(1/4)*(-Log[3 - I*Sqrt[3]] + 
              Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)))/
        EllipticK[(5 + Sqrt[5])/10])) + IEI[{2, 6, 1, 0}, TauA]*
    (EllipticK[(5 - Sqrt[5])/10]*((((28*I)/9)*Pi)/(Sqrt[3]*5^(1/4)) + 
       ((28*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((28*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((28*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((28*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (28*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (28*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((28*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((28*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((28*I)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)*Pi)) + ((((4*I)/3)*5^(1/4)*Pi^2)/Sqrt[3] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(-1 + (4*I)*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(1 - (4*I)*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1] - 
       5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(I - 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(-1 + (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*Log[2]*((4*I)*Sqrt[3]*Log[4] + (-1 - (4*I)*Sqrt[3])*
          Log[3 - I*Sqrt[3]] + (1 - (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((2*I)/3)*5^(1/4)*Pi)/Sqrt[3] + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 + (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) + (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (-Log[(3 - I*Sqrt[3])/2]^2 - 10*Log[3 - I*Sqrt[3]] + 
            Log[(3 + I*Sqrt[3])/2]^2 + 10*Log[3 + I*Sqrt[3]]))/(2*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(-5 - Log[3 + I*Sqrt[3]] + 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EllipticK[(5 + Sqrt[5])/10]*((28*(-2*I + 3*Sqrt[3])*Pi)/(81*5^(1/4)) - 
       (26*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (26*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (26*G[0, (2*I)/(-I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (26*G[0, (-2*I)/(I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (26*G[(I/2)*(I + Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (7*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (14*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) + (7*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (14*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(3*Pi^2) + 
       (28*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (28*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(3*Pi^2) - 
       (7*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (14*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (14*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) - (14*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (14*G[0, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          1])/(3*5^(1/4)*Pi^2) + (14*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
        (3*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (((14*I)/3)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((14*I)/3)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((14*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((14*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((14*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (14*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (14*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (14*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
       (7*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (14*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) + (7*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (14*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (14*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (14*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (7*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) - (14*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
        (3*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
         Log[2])/(3*5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) - (((14*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (14*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) - (14*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(3*5^(1/4)*Pi^2) - (7*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(3*5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (7*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       G[0, -2/(1 + I*Sqrt[3]), 1]*(-28/(Sqrt[3]*5^(1/4)*Pi) - 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((((-7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (7*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (28/(Sqrt[3]*5^(1/4)*Pi) - (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (7*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        (-28/(Sqrt[3]*5^(1/4)*Pi) + (14*Log[2])/(3*5^(1/4)*Pi^2)) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*((((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (28/(Sqrt[3]*5^(1/4)*Pi) + (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((-7*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (7*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (7*GR[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
         (7*Log[2]^2)/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((7*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (7*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (7*GR[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
         (7*Log[2]^2)/(3*5^(1/4)*Pi^2)) + 
       (Log[2]*(-7*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
          7*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/
        (3*5^(1/4)*Pi^2) + (7*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
            Log[3 + I*Sqrt[3]])))/(Sqrt[3]*5^(1/4)*Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*(28/(Sqrt[3]*5^(1/4)*Pi) - 
         (2*(13 + 7*Log[3 + I*Sqrt[3]] - 7*Log[6 - (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2)) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((28*I)/(Sqrt[3]*5^(1/4)*Pi) - 
         (((2*I)/3)*(13 + 7*Log[3 + I*Sqrt[3]] - 7*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
        ((28*I)/(Sqrt[3]*5^(1/4)*Pi) + (((14*I)/3)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*(28/(Sqrt[3]*5^(1/4)*Pi) + 
         (14*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2))) + EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(-Log[3 - I*Sqrt[3]] + 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(-Log[3 - I*Sqrt[3]] + 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10])) + 
   IEI[{2, 6, 4, 3}, TauA]*(EllipticK[(5 - Sqrt[5])/10]*
      ((((28*I)/9)*Pi)/(Sqrt[3]*5^(1/4)) + 
       ((28*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((28*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((28*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((28*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (28*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (28*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((28*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((28*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((28*I)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)*Pi)) + ((((4*I)/3)*5^(1/4)*Pi^2)/Sqrt[3] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(-1 + (4*I)*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(1 - (4*I)*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1] - 
       5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(I - 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(-1 + (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*Log[2]*((4*I)*Sqrt[3]*Log[4] + (-1 - (4*I)*Sqrt[3])*
          Log[3 - I*Sqrt[3]] + (1 - (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((2*I)/3)*5^(1/4)*Pi)/Sqrt[3] + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 + (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) + (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (-Log[(3 - I*Sqrt[3])/2]^2 - 10*Log[3 - I*Sqrt[3]] + 
            Log[(3 + I*Sqrt[3])/2]^2 + 10*Log[3 + I*Sqrt[3]]))/(2*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(-5 - Log[3 + I*Sqrt[3]] + 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EllipticK[(5 + Sqrt[5])/10]*((28*(-2*I + 3*Sqrt[3])*Pi)/(81*5^(1/4)) - 
       (26*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (26*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (26*G[0, (2*I)/(-I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (26*G[0, (-2*I)/(I + Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (26*G[(I/2)*(I + Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (7*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (14*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) + (7*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (14*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(3*Pi^2) + 
       (28*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (28*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(3*Pi^2) - 
       (7*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (14*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (14*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (14*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) - (14*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (14*G[0, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (14*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(3*5^(1/4)*Pi^2) + 
       (7*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          1])/(3*5^(1/4)*Pi^2) + (14*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
        (3*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -1, 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (7*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (((14*I)/3)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((14*I)/3)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((14*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((14*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((14*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (14*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (14*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (14*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
       (7*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (14*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) + (7*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (14*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi^2) - 
       (7*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (14*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (14*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (7*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) - (14*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (7*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
        (3*5^(1/4)*Pi^2) - (7*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
         Log[2])/(3*5^(1/4)*Pi^2) + (((14*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) - (((14*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(5^(1/4)*Pi^2) + (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (14*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) - (14*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(3*5^(1/4)*Pi^2) - (7*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(3*5^(1/4)*Pi^2) - 
       (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (7*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       (7*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       G[0, -2/(1 + I*Sqrt[3]), 1]*(-28/(Sqrt[3]*5^(1/4)*Pi) - 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((((-7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (7*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (28/(Sqrt[3]*5^(1/4)*Pi) - (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (7*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        (-28/(Sqrt[3]*5^(1/4)*Pi) + (14*Log[2])/(3*5^(1/4)*Pi^2)) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*((((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (28/(Sqrt[3]*5^(1/4)*Pi) + (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (7*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (14*Log[2])/(3*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((-7*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((7*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (7*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (7*GR[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
         (7*Log[2]^2)/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((7*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((7*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (7*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (7*GR[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
         (7*Log[2]^2)/(3*5^(1/4)*Pi^2)) + 
       (Log[2]*(-7*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
          7*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/
        (3*5^(1/4)*Pi^2) + (7*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
            Log[3 + I*Sqrt[3]])))/(Sqrt[3]*5^(1/4)*Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*(28/(Sqrt[3]*5^(1/4)*Pi) - 
         (2*(13 + 7*Log[3 + I*Sqrt[3]] - 7*Log[6 - (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2)) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((28*I)/(Sqrt[3]*5^(1/4)*Pi) - 
         (((2*I)/3)*(13 + 7*Log[3 + I*Sqrt[3]] - 7*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
        ((28*I)/(Sqrt[3]*5^(1/4)*Pi) + (((14*I)/3)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*(28/(Sqrt[3]*5^(1/4)*Pi) + 
         (14*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2))) + EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(-Log[3 - I*Sqrt[3]] + 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (5*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 10*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 10*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
              Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(-Log[3 - I*Sqrt[3]] + 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10])) - 
   Zeta[3]/3 + EllipticK[(5 + Sqrt[5])/10]*
    ((26*(2 + (3*I)*Sqrt[3])*Pi^2)/(81*5^(1/4)) - 
     (7*Pi^3)/(9*Sqrt[3]*5^(1/4)) + 
     (((13*I)/3)*G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/3)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/3)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((13*I)/3)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((13*I)/6)*G[-1, (2*I)/(-I + Sqrt[3]), 
        (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/6)*G[-1, (-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((13*I)/3)*G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((13*I)/6)*G[-1, (-2*I)/(I + Sqrt[3]), 
        (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[-1, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((13*I)/3)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((13*I)/6)*G[-1, (I/2)*(I + Sqrt[3]), 
        (-1/2*I)*(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/3)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     ((7*I)*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/Sqrt[3] - 
     ((7*I)*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/Sqrt[3] - 
     (((13*I)/6)*5^(3/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
     (((26*I)/3)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((26*I)/3)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/6)*5^(3/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi + 
     ((7*I)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)) + 
     ((7*I)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)) + 
     (((13*I)/6)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/6)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     ((14*I)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(Sqrt[3]*5^(1/4)) + 
     ((7*I)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
      (Sqrt[3]*5^(1/4)) - ((14*I)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/
      (Sqrt[3]*5^(1/4)) + ((7*I)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1])/(Sqrt[3]*5^(1/4)) - (((13*I)/3)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 
        1])/(5^(1/4)*Pi) - (((13*I)/6)*G[0, (-2*I)/(-I + Sqrt[3]), 
        (2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/3)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[0, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((13*I)/3)*G[0, (2*I)/(-I + Sqrt[3]), 
        (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[0, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((13*I)/3)*G[0, (-2*I)/(I + Sqrt[3]), 
        (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/3)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((13*I)/6)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + ((7*I)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
      (Sqrt[3]*5^(1/4)) + ((7*I)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
        1])/(Sqrt[3]*5^(1/4)) + ((7*I)*G[2/(1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)) + 
     ((7*I)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
      (Sqrt[3]*5^(1/4)) - (((13*I)/6)*G[(-2*I)/(-I + Sqrt[3]), 1, 
        (2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/6)*G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((13*I)/3)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((13*I)/6)*G[(2*I)/(-I + Sqrt[3]), -1, 
        (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/6)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((13*I)/6)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
        (I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/6)*G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((13*I)/3)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) + (((13*I)/6)*G[(-2*I)/(I + Sqrt[3]), -1, 
        (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/6)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((13*I)/6)*G[(2*I)/(I + Sqrt[3]), 1, 
        (-2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((13*I)/6)*G[(2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((13*I)/3)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) + (((13*I)/6)*G[(I/2)*(I + Sqrt[3]), -1, 
        (-1/2*I)*(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((13*I)/6)*G[(I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((13*I)/3)*G[(I/2)*(I + Sqrt[3]), (I/2)*(I + Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) + (((7*I)/3)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) - (((7*I)/3)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[-1, -1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((14*I)/3)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((14*I)/3)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, 0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + ((7*I)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((14*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) + (((7*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     ((7*I)*G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((14*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*5^(3/4)*G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
     (((7*I)/3)*5^(3/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
     (((14*I)/3)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((14*I)/3)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[0, -1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[0, -1, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((14*I)/3)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((14*I)/3)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/2)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((28*I)/3)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((28*I)/3)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/2)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
      Pi - (((7*I)/2)*G[0, 0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((14*I)/3)*G[0, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((14*I)/3)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((14*I)/3)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((14*I)/3)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((14*I)/3)*G[0, 0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((14*I)/3)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((7*I)/2)*G[0, 0, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
      Pi - (((7*I)/6)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) + (((7*I)/3)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[0, -2/(-1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[0, -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) + 
     ((7*I)*G[0, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - ((7*I)*G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) + (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) + 
     ((7*I)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - ((7*I)*G[0, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) + (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((7*I)/3)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) + (((7*I)/6)*G[0, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) + ((7*I)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/
      (5^(1/4)*Pi) + (((7*I)/3)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*5^(3/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -2/(1 + I*Sqrt[3]), 1])/
      Pi + (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/2)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) + (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) - (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) + (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     ((7*I)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*5^(3/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
      Pi - (((7*I)/3)*G[-2/(1 + I*Sqrt[3]), -1, -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((7*I)/2)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) - (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) + (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -1, 1])/(5^(1/4)*Pi) - (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 1, 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
      (5^(1/4)*Pi) - (((7*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) - (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) + (7*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (7*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (7*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (7*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (3*5^(1/4)*Pi) + (7*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (7*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (6*5^(1/4)*Pi) + (7*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
     (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi) + 
     (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
      (2*5^(1/4)*Pi) - (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(3*5^(1/4)*Pi) - (7*GI[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (6*5^(1/4)*Pi) - (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi) - 
     (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
      (2*5^(1/4)*Pi) - (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) + (7*GI[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (3*5^(1/4)*Pi) + (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(3*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (3*5^(1/4)*Pi) + (7*5^(3/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*Pi) - 
     (14*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
      (3*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (6*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (3*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(3*5^(1/4)*Pi) + (7*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (6*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) + (7*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(3*5^(1/4)*Pi) - 
     (7*5^(3/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (6*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 + (I/2)*Sqrt[3]])/
      (3*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (14*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (3*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (3*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
     ((7*I)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     ((7*I)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     ((7*I)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     ((7*I)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((7*I)/3)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi) - 
     (((7*I)/2)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) + (((7*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) + (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi) + 
     (((7*I)/2)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) + (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((7*I)/6)*5^(3/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
        -1/2 + (I/2)*Sqrt[3]])/Pi + 
     (((14*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*5^(3/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
        -1/2 - (I/2)*Sqrt[3]])/Pi + 
     (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 + (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((14*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(14/(Sqrt[3]*5^(1/4)) + 
       (13 - 21*Log[2])/(3*5^(1/4)*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*
      (-14/(Sqrt[3]*5^(1/4)) + (13 - 7*Log[2])/(3*5^(1/4)*Pi)) - 
     (((13*I)/3)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/(5^(1/4)*Pi) - 
     (((13*I)/6)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*Log[2])/
      (5^(1/4)*Pi) + (((13*I)/6)*G[(-1/2*I)*(-I + Sqrt[3]), 
        (I/2)*(I + Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (((13*I)/6)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*Log[2])/
      (5^(1/4)*Pi) - (((13*I)/6)*G[(I/2)*(I + Sqrt[3]), 
        (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        1]*Log[2])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        1]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        1]*Log[2])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        1]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        1]*Log[2])/(5^(1/4)*Pi) - 
     (((7*I)/6)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        1]*Log[2])/(5^(1/4)*Pi) + 
     (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
      (3*5^(1/4)*Pi) - (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(3*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - 
     (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - (((7*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) - 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) - 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
      (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2]^2)/(6*5^(1/4)*Pi) + 
     (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
      (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
      (-7/(Sqrt[3]*5^(1/4)) - (7*Log[2])/(2*5^(1/4)*Pi)) + 
     GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      (-7/(Sqrt[3]*5^(1/4)) - (7*Log[2])/(6*5^(1/4)*Pi)) + 
     GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
      ((7*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
      ((-7*I)/(Sqrt[3]*5^(1/4)) + (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-7*I)/(Sqrt[3]*5^(1/4)) - (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((7*I)/(Sqrt[3]*5^(1/4)) - (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      ((7*I)/(Sqrt[3]*5^(1/4)) + (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
      ((-7*I)/(Sqrt[3]*5^(1/4)) + (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((7*I)/(Sqrt[3]*5^(1/4)) + (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
      ((-7*I)/(Sqrt[3]*5^(1/4)) - (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*Log[2])/(5^(1/4)*Pi)) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*((14*I)/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*
      ((14*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*(((14*I)*Sqrt[3])/5^(1/4) - 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-28*I)/(Sqrt[3]*5^(1/4)) + (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi) - (((7*I)/2)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
      ((7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((14*I)/(Sqrt[3]*5^(1/4)) + (7*GI[-1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) + (7*5^(3/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) - 
       (((7*I)/6)*5^(3/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) + (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) + (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) + (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -1, -2/(1 + I*Sqrt[3]), 1]*((14*I)/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[0, -2/(1 + I*Sqrt[3]), -1, 1]*
      (((14*I)*Sqrt[3])/5^(1/4) + (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-7*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((14*I)/(Sqrt[3]*5^(1/4)) - (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*
      ((-28*I)/(Sqrt[3]*5^(1/4)) - (7*GI[-1/2 + (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi) + (((7*I)/2)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) - (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi) - (7*5^(3/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(6*Pi) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*5^(3/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((7*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
      ((7*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/2)*Log[2])/(5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
      ((7*I)/(Sqrt[3]*5^(1/4)) + (((7*I)/2)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*((28*I)/(Sqrt[3]*5^(1/4)) - 
       (((14*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
      ((7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((14*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[0, 0, -2/(1 + I*Sqrt[3]), 1]*
      ((28*I)/(Sqrt[3]*5^(1/4)) + (((14*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
      ((-7*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((14*I)/3)*Log[2])/(5^(1/4)*Pi)) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) - ((7*I)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*((14*I)/(Sqrt[3]*5^(1/4)) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       ((7*I)*Log[2])/(5^(1/4)*Pi)) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*
      ((14*I)/(Sqrt[3]*5^(1/4)) + (7*GI[-1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       ((7*I)*Log[2])/(5^(1/4)*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
       -1/2 - (I/2)*Sqrt[3]]*(-7/(Sqrt[3]*5^(1/4)) + 
       (7*Log[2])/(6*5^(1/4)*Pi)) + GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*
      (-14/(Sqrt[3]*5^(1/4)) + (7*Log[2])/(3*5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
      (-7/(Sqrt[3]*5^(1/4)) + (7*Log[2])/(2*5^(1/4)*Pi)) + 
     GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*(14/(Sqrt[3]*5^(1/4)) + 
       (7*Log[2])/(5^(1/4)*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3]]*
      ((-14*Log[2])/(Sqrt[3]*5^(1/4)) - (7*Log[2]^2)/(2*5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((-7*Pi)/(36*5^(1/4)) + 
       (7*Log[2])/(Sqrt[3]*5^(1/4)) - (7*Log[2]^2)/(12*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      ((((-7*I)/36)*Pi)/5^(1/4) - ((7*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/12)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((((-7*I)/36)*Pi)/5^(1/4) - 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((7*I)*Log[2])/(Sqrt[3]*5^(1/4)) - (((7*I)/12)*Log[2]^2)/
        (5^(1/4)*Pi)) + GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
      ((((7*I)/36)*Pi)/5^(1/4) - ((7*I)*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/12)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((((7*I)/36)*Pi)/5^(1/4) + 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((7*I)*Log[2])/(Sqrt[3]*5^(1/4)) + (((7*I)/12)*Log[2]^2)/
        (5^(1/4)*Pi)) + G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((7*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
      ((7*(-I + 3*Sqrt[3])*Pi)/(9*5^(1/4)) + ((14*I)*Log[2])/
        (Sqrt[3]*5^(1/4)) - (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-1, -2/(1 + I*Sqrt[3]), 1]*((-7*(I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
       (7*5^(3/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*Pi) - 
       (14*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((7*I)*GR[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*5^(3/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((14*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((14*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
      ((-7*GI[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (7*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((7*I)*GR[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/2)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((28*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((-7*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
      ((7*(I + 3*Sqrt[3])*Pi)/(9*5^(1/4)) + ((14*I)*Log[2])/
        (Sqrt[3]*5^(1/4)) + (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-7*(-I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (7*5^(3/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*Pi) - 
       (7*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (14*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((7*I)*GR[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/6)*5^(3/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       (((7*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((14*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((14*I)*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-7*GI[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((7*I)*GR[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/2)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((28*I)*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[(-2*I)/(I + Sqrt[3]), 1]*
      ((13*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (13*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((13*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((13*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((13*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[(I/2)*(I + Sqrt[3]), 1]*
      ((((-13*I)/6)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
       (((13*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[(2*I)/(-I + Sqrt[3]), 1]*
      ((((13*I)/6)*G[-1, (I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (((13*I)/6)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
       (((13*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3]]*
      (((14*I)*Log[2])/(Sqrt[3]*5^(1/4)) + (((7*I)/2)*Log[2]^2)/
        (5^(1/4)*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      ((7*Pi)/(36*5^(1/4)) + (7*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (7*Log[2]^2)/(12*5^(1/4)*Pi)) + GI[-1/2 - (I/2)*Sqrt[3]]*
      ((7*Log[2]^2)/(Sqrt[3]*5^(1/4)) + ((13 - 7*Log[2])*Log[2]^2)/
        (6*5^(1/4)*Pi)) + GR[-1/2 + (I/2)*Sqrt[3]]*
      (((-7*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) - (((7*I)/6)*Log[2]^3)/
        (5^(1/4)*Pi)) + G[-2/(1 + I*Sqrt[3]), 1]*
      ((7*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)) - 
       (7*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) - 
       (7*5^(3/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(6*Pi) + 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (2*5^(1/4)*Pi) + (7*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi) + (7*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi) + (7*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       ((7*I)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       ((7*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/3)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*5^(3/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((7*I)/2)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) - ((7*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/6)*Log[2]^3)/(5^(1/4)*Pi)) + G[2/(-1 + I*Sqrt[3]), 1]*
      ((7*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)) + 
       (7*5^(3/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(6*Pi) + 
       (7*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (7*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi) - (7*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi) - 
       (7*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (7*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi) - ((7*I)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/
        (Sqrt[3]*5^(1/4)) - ((7*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/
        (Sqrt[3]*5^(1/4)) - (((7*I)/6)*5^(3/4)*GR[-1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((7*I)/3)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((7*I)/2)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((7*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) + (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((7*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) - ((7*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) + 
       (((7*I)/6)*Log[2]^3)/(5^(1/4)*Pi)) + GI[-1/2 + (I/2)*Sqrt[3]]*
      ((7*Log[2]^2)/(Sqrt[3]*5^(1/4)) + (7*Log[2]^3)/(6*5^(1/4)*Pi)) + 
     GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*((-14*I)/(Sqrt[3]*5^(1/4)) + 
       ((I/3)*(-13 + 21*Log[2]))/(5^(1/4)*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3]]*
      (((14*I)*Log[2])/(Sqrt[3]*5^(1/4)) - ((I/6)*(21*Log[2]^2 - 13*Log[4]))/
        (5^(1/4)*Pi)) + GI[-1, -1/2 - (I/2)*Sqrt[3]]*
      ((-14*Log[2])/(Sqrt[3]*5^(1/4)) + (21*Log[2]^2 - 13*Log[4])/
        (6*5^(1/4)*Pi)) - (((13*I)/6)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/
      (5^(1/4)*Pi) + G[(-2*I)/(I + Sqrt[3]), -1, 1]*
      ((-13*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((13*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((13*I)/6)*Log[4])/(5^(1/4)*Pi)) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
      ((-26*I)/(Sqrt[3]*5^(1/4)) + ((I/6)*(-46 + 13*Log[4]))/(5^(1/4)*Pi)) + 
     G[-1, (-2*I)/(I + Sqrt[3]), 1]*((26*I)/(Sqrt[3]*5^(1/4)) - 
       (13*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((13*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       ((I/6)*(46 + 13*Log[4]))/(5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*((14*I)/(Sqrt[3]*5^(1/4)) + 
       ((I/3)*(-13 + Log[128]))/(5^(1/4)*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
      (((-7*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) + 
       ((I/6)*Log[2]^2*(-13 + Log[128]))/(5^(1/4)*Pi)) + 
     G[0, (2*I)/(-I + Sqrt[3]), 1]*((-26*I)/(Sqrt[3]*5^(1/4)) - 
       ((I/3)*(-23 + Log[8192]))/(5^(1/4)*Pi)) + 
     G[-1, (2*I)/(-I + Sqrt[3]), 1]*((26*I)/(Sqrt[3]*5^(1/4)) - 
       (((13*I)/6)*G[(I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       ((I/3)*(23 + Log[8192]))/(5^(1/4)*Pi)) + 
     GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*((28*I)/(Sqrt[3]*5^(1/4)) + 
       ((I/3)*(-13 + 7*Log[8] + 14*Log[3 - I*Sqrt[3]] - 
          14*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      ((-7*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/6)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
          2*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      (7/(Sqrt[3]*5^(1/4)) + (7*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
          2*Log[3 + I*Sqrt[3]]))/(6*5^(1/4)*Pi)) - 
     ((I/18)*Log[2]*(7*Log[(3 - I*Sqrt[3])/2]^3 + 
        (138 + 28*Log[2]^2 - 39*Log[4])*Log[3 - I*Sqrt[3]] + 
        39*Log[3 - I*Sqrt[3]]^2 - 7*Log[(3 + I*Sqrt[3])/2]^3 - 
        138*Log[3 + I*Sqrt[3]] - 28*Log[2]^2*Log[3 + I*Sqrt[3]] + 
        39*Log[4]*Log[3 + I*Sqrt[3]] - 39*Log[3 + I*Sqrt[3]]^2))/
      (5^(1/4)*Pi) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
      ((7*(-1 - (6*I)*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (7*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - Log[(3 + I*Sqrt[3])/2]^2))/
        (6*5^(1/4)*Pi) - (14*(Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4))) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
      ((-7*(-I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) + 
       (((7*I)/6)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
          Log[(3 + I*Sqrt[3])/2]^2))/(5^(1/4)*Pi) + 
       ((14*I)*(Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4))) + GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
       -1]*(7/(Sqrt[3]*5^(1/4)) - (7*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
          2*Log[3 + I*Sqrt[3]]))/(6*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
      ((-7*I)/(Sqrt[3]*5^(1/4)) + (((7*I)/6)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
          2*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*((28*I)/(Sqrt[3]*5^(1/4)) - 
       (((7*I)/3)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
      (-28/(Sqrt[3]*5^(1/4)) + (7*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
          2*Log[3 + I*Sqrt[3]]))/(3*5^(1/4)*Pi)) + 
     GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*(-28/(Sqrt[3]*5^(1/4)) + 
       (13 - 7*Log[8] - 14*Log[3 - I*Sqrt[3]] + 14*Log[3 + I*Sqrt[3]])/
        (3*5^(1/4)*Pi)) + G[(I/2)*(I + Sqrt[3]), -1, 1]*
      ((26*I)/(Sqrt[3]*5^(1/4)) - ((I/6)*(-46 + 13*Log[4] - 
          26*Log[3 - I*Sqrt[3]] + 26*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1]*((-7*(I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       ((2*I)*(-13 + 28*Log[2] - 7*Log[3 - I*Sqrt[3]] - 
          7*Log[3 + I*Sqrt[3]]))/(Sqrt[3]*5^(1/4)) - 
       ((I/6)*(46 + 21*Log[2]^2 - 13*Log[4] - 7*Log[(3 - I*Sqrt[3])/2]^2 - 
          26*Log[3 - I*Sqrt[3]] + 7*Log[(3 + I*Sqrt[3])/2]^2 + 
          26*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1]*((7*(1 - (6*I)*Sqrt[3])*Pi)/(18*5^(1/4)) + 
       (2*(-13 + 2*Log[16384] - 7*Log[3 - I*Sqrt[3]] - 7*Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)) + (46 + 21*Log[2]^2 - 13*Log[4] - 
         7*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
         7*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]])/
        (6*5^(1/4)*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      (14/(Sqrt[3]*5^(1/4)) - (13 + 7*Log[3 + I*Sqrt[3]] - 
         7*Log[6 - (2*I)*Sqrt[3]])/(3*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) + ((I/3)*(13 + 7*Log[3 + I*Sqrt[3]] - 
          7*Log[6 - (2*I)*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
      ((-14*I)/(Sqrt[3]*5^(1/4)) - (((7*I)/3)*(Log[3 - I*Sqrt[3]] - 
          Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
      (14/(Sqrt[3]*5^(1/4)) + (7*(Log[3 - I*Sqrt[3]] - 
          Log[6 + (2*I)*Sqrt[3]]))/(3*5^(1/4)*Pi)) + 
     (7*Pi*(-(Sqrt[3]*Log[4]*Log[4096]) + Sqrt[3]*Log[4096]*
         Log[3 - I*Sqrt[3]] + I*Log[4]*(Log[3 - I*Sqrt[3]] - 
          Log[3 + I*Sqrt[3]]) + Sqrt[3]*Log[4096]*Log[3 + I*Sqrt[3]] - 
        I*Log[3 - I*Sqrt[3]]*Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + 
        I*Log[3 + I*Sqrt[3]]*Log[(3*I + Sqrt[3])/(I + Sqrt[3])] - 
        I*Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
          Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + 
          Log[(3*I + Sqrt[3])/(I + Sqrt[3])])))/(36*5^(1/4)) - 
     (I*(56*Log[2]^2 + 14*Log[2]^3 + Log[2]*(26*Log[3 - I*Sqrt[3]] + 
          7*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
           (26 + 7*Log[3 + I*Sqrt[3]]) - Log[4]*(54 + 7*Log[3 - I*Sqrt[3]] + 
            7*Log[3 + I*Sqrt[3]])) + 56*Zeta[3]))/(Sqrt[3]*5^(1/4))) + 
   EllipticK[(5 - Sqrt[5])/10]*((-26*Pi^2)/(9*Sqrt[3]*5^(1/4)) + 
     (7*(2 - (3*I)*Sqrt[3])*Pi^3)/(81*5^(1/4)) - 
     (13*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(6*5^(1/4)) - 
     (13*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(6*5^(1/4)) + 
     (13*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/(6*5^(1/4)) + 
     (13*(-I + 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/(6*5^(1/4)) - 
     (13*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/(6*5^(1/4)) + 
     (7*(-I + 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*(-I + 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*(-I + 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(6*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) - (7*(I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(12*5^(1/4)) + 
     (7*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/(6*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) - (7*(I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*5^(3/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/12 - 
     (7*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)) + 
     (7*5^(3/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/12 - 
     (7*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(12*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(12*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(6*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) + (7*(I - 12*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (6*5^(1/4)) + (7*(-I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) - (7*(I + 12*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
      (6*5^(1/4)) + (7*(-I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (7*(-I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/
      (6*5^(1/4)) - (7*(I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(12*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) - (7*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1, 1])/(12*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) + (7*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), -1, 1])/(12*5^(1/4)) - 
     (7*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/(6*5^(1/4)) + 
     (7*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) + (7*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(12*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) - (7*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(12*5^(1/4)) + 
     (7*(1 + (4*I)*Sqrt[3])*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)) + 
     (((7*I)/6)*(I + 4*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/5^(1/4) + 
     (7*(1 - (8*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (7*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (7*(-1 - (8*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (7*(-1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (7*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(6*5^(1/4)) + 
     (7*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (7*(-1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)) + (((7*I)/12)*(I + 4*Sqrt[3])*
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/5^(1/4) + 
     (7*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(6*5^(1/4)) + 
     (7*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (7*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(12*5^(1/4)) + (7*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (7*(-I + 4*Sqrt[3])*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)) - 
     (7*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
      (12*5^(1/4)) - (7*(-I + 8*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
      (6*5^(1/4)) - (7*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(6*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
      (12*5^(1/4)) + (7*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (12*5^(1/4)) - (7*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
      (6*5^(1/4)) - (7*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (7*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
      (12*5^(1/4)) + (7*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
      (12*5^(1/4)) + (7*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1]*Log[2])/(12*5^(1/4)) + 
     (7*(-1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
      (6*5^(1/4)) + (7*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)) + (((7*I)/12)*(I + 4*Sqrt[3])*
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/5^(1/4) + 
     (7*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(12*5^(1/4)) - (7*(-I + 4*Sqrt[3])*
       GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(6*5^(1/4)) - 
     (7*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(6*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(12*5^(1/4)) + (7*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(12*5^(1/4)) + 
     (7*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(12*5^(1/4)) + 
     (((7*I)/12)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/5^(1/4) + 
     (7*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(12*5^(1/4)) + 
     (7*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(12*5^(1/4)) + 
     G[0, 2/(-1 + I*Sqrt[3]), 1]*(((7*I)*Pi)/(Sqrt[3]*5^(1/4)) - 
       (7*(-I + 4*Sqrt[3])*Log[2])/(6*5^(1/4))) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 1]*(((-7*I)*Pi)/(Sqrt[3]*5^(1/4)) + 
       (7*(-1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(-I + 4*Sqrt[3])*Log[2])/(6*5^(1/4))) + G[0, -2/(1 + I*Sqrt[3]), 1]*
      (((7*I)*Pi)/(Sqrt[3]*5^(1/4)) - (7*(I + 4*Sqrt[3])*Log[2])/
        (6*5^(1/4))) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
      (((-7*I)*Pi)/(Sqrt[3]*5^(1/4)) + (7*(1 - (4*I)*Sqrt[3])*
         GI[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(I + 4*Sqrt[3])*Log[2])/(6*5^(1/4))) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
      ((7*(-1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(-I + 8*Sqrt[3])*Log[2])/(6*5^(1/4))) + 
     G[-2/(1 + I*Sqrt[3]), -1, 1]*
      ((7*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (7*(I + 8*Sqrt[3])*Log[2])/(6*5^(1/4))) + G[2/(-1 + I*Sqrt[3]), 1]*
      ((7*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
       (7*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/(12*5^(1/4)) + 
       (7*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
       (7*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/(12*5^(1/4)) + 
       (7*(-I + 4*Sqrt[3])*Log[2]^2)/(12*5^(1/4))) + 
     G[-2/(1 + I*Sqrt[3]), 1]*((((7*I)/12)*(I + 4*Sqrt[3])*
         GI[-1, -1/2 + (I/2)*Sqrt[3]])/5^(1/4) + 
       (((7*I)/12)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/5^(1/4) + 
       (7*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
       (7*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/(12*5^(1/4)) + 
       (7*(I + 4*Sqrt[3])*Log[2]^2)/(12*5^(1/4))) + 
     ((7*I)*Pi*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
      (Sqrt[3]*5^(1/4)) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
      (((-7*I)*Pi)/(Sqrt[3]*5^(1/4)) + (-13*I - 52*Sqrt[3] + (7*I)*Log[2] + 
         2*Sqrt[3]*Log[72057594037927936] + (7*I)*Log[3 - I*Sqrt[3]] - 
         28*Sqrt[3]*Log[3 - I*Sqrt[3]] - (7*I)*Log[3 + I*Sqrt[3]] - 
         28*Sqrt[3]*Log[3 + I*Sqrt[3]])/(6*5^(1/4))) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1]*(((-7*I)*Pi)/(Sqrt[3]*5^(1/4)) + 
       (7*((-I)*Log[2] + 2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/(6*5^(1/4))) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1]*((7*Pi)/(Sqrt[3]*5^(1/4)) + 
       (13 - (52*I)*Sqrt[3] + (112*I)*Sqrt[3]*Log[2] - Log[128] - 
         7*Log[3 - I*Sqrt[3]] - (28*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] + 
         7*Log[3 + I*Sqrt[3]] - (28*I)*Sqrt[3]*Log[3 + I*Sqrt[3]])/
        (6*5^(1/4))) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
      ((7*Pi)/(Sqrt[3]*5^(1/4)) + (7*((16*I)*Sqrt[3]*Log[2] + 
          (-1 - (4*I)*Sqrt[3])*Log[3 - I*Sqrt[3]] - (4*I)*Sqrt[3]*
           Log[3 + I*Sqrt[3]] + Log[6 + (2*I)*Sqrt[3]]))/(6*5^(1/4))) + 
     (168*Sqrt[3]*Log[2]^3 - (13*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
         Log[3 + I*Sqrt[3]]) + Log[2]*
        (-(Sqrt[3]*(-312 + Log[
             374144419156711147060143317175368453031918731001856])*
           Log[3 - I*Sqrt[3]]) + 21*(-I + 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 - 
         3*Log[4]*(104*Sqrt[3] - (7*I)*Log[3 - I*Sqrt[3]] + 
           (7*I)*Log[3 + I*Sqrt[3]]) + Log[3 + I*Sqrt[3]]*
          (312*Sqrt[3] - Sqrt[3]*
            Log[374144419156711147060143317175368453031918731001856] + 
           21*(I + 4*Sqrt[3])*Log[3 + I*Sqrt[3]])) + 672*Sqrt[3]*Zeta[3])/
      (36*5^(1/4))) + ((-10*5^(1/4)*Pi^3)/(3*Sqrt[3]) + 
     (5^(1/4)*(2 - (3*I)*Sqrt[3])*Pi^4)/27 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(-I + 8*Sqrt[3])*Pi*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/2 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[-1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/2 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
     (5^(1/4)*(I + 8*Sqrt[3])*Pi*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/2 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[-1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5*5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/4 + 
     5^(1/4)*(I - 4*Sqrt[3])*Pi*G[0, 0, 2/(-1 + I*Sqrt[3]), 1] - 
     5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 0, -2/(1 + I*Sqrt[3]), 1] + 
     (5*5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/4 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/4 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/4 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/2 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1])/4 + (5^(1/4)*(I - 12*Sqrt[3])*Pi*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
      2 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/2 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/2 - 
     (5^(1/4)*(I + 12*Sqrt[3])*Pi*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/2 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[-2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*Pi*
       G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/4 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/4 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
        1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/4 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*
       GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/2 + 
     (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/2 + 
     (5^(1/4)*(1 - (8*I)*Sqrt[3])*Pi*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(-1 - (8*I)*Sqrt[3])*Pi*
       GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/2 + 
     (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/2 + 
     (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
     (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/2 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/2 - 
     (5^(1/4)*(I + 8*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(I - 8*Sqrt[3])*Pi*
       GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/4 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*Pi*
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/2 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/2 + G[0, 2/(-1 + I*Sqrt[3]), 1]*
      (I*Sqrt[3]*5^(1/4)*Pi^2 - (5^(1/4)*(-I + 4*Sqrt[3])*Pi*(-5 + Log[2]))/
        2) + G[0, -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4)*Pi^2 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*(-5 + Log[2]))/2) + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
       Log[2])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1]*Log[2])/4 + 
     (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
      2 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/2 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
     (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
     (5^(1/4)*(I - 4*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
     (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     G[2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*
         GI[-1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(I - 4*Sqrt[3])*Pi*
         GR[-1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(I - 8*Sqrt[3])*Pi*Log[2])/
        2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
      ((5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 8*Sqrt[3])*Pi*Log[2])/2) + G[2/(-1 + I*Sqrt[3]), 1]*
      ((5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 - (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*Log[2]^2)/4) + G[-2/(1 + I*Sqrt[3]), 1]*
      ((5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 + (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1/2 + (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*Log[2]^2)/4) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4)*Pi^2 + 
       (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(I - 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*(5 + Log[2]))/2) + 
     G[-1, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4)*Pi^2 + 
       (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*(5 + Log[2]))/2) + 
     I*Sqrt[3]*5^(1/4)*Pi^2*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
       Log[3 + I*Sqrt[3]]) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
      ((-I)*Sqrt[3]*5^(1/4)*Pi^2 + (5^(1/4)*Pi*(5*I - 20*Sqrt[3] - I*Log[2] + 
          2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/2) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
      ((-I)*Sqrt[3]*5^(1/4)*Pi^2 + (5^(1/4)*Pi*(-5*I - 20*Sqrt[3] + 
          I*Log[2] + 2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/2) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
      (Sqrt[3]*5^(1/4)*Pi^2 + (5^(1/4)*Pi*(5 - (20*I)*Sqrt[3] + 
          (16*I)*Sqrt[3]*Log[2] + (-1 - (4*I)*Sqrt[3])*Log[3 - I*Sqrt[3]] + 
          Log[(3 + I*Sqrt[3])/2] - (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/2) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1]*(Sqrt[3]*5^(1/4)*Pi^2 + 
       (5^(1/4)*Pi*(-5 - (20*I)*Sqrt[3] + (16*I)*Sqrt[3]*Log[2] + 
          (-1 - (4*I)*Sqrt[3])*Log[3 - I*Sqrt[3]] - (4*I)*Sqrt[3]*
           Log[3 + I*Sqrt[3]] + Log[6 + (2*I)*Sqrt[3]]))/2) + 
     (5^(1/4)*Pi*(8*Sqrt[3]*Log[2]^3 - 
        Log[2]*(Sqrt[3]*Log[5192296858534827628530496329220096] + 
          (10*I - 40*Sqrt[3] - I*Log[4] + Sqrt[3]*Log[256])*
           Log[3 - I*Sqrt[3]] + I*Log[3 - I*Sqrt[3]]^2 + 
          (-10*I - 40*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - 
            I*Log[3 + I*Sqrt[3]])*Log[3 + I*Sqrt[3]]) + 
        Sqrt[3]*(Log[4]*Log[65536] + Log[16]*(Log[3 - I*Sqrt[3]]^2 + 
            Log[3 + I*Sqrt[3]]^2) + 32*Zeta[3])))/4)/
    EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 2, 0, 1, TauA]*
    ((((-5*I)/27)*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi^2 + 
       (5^(1/4)*Pi^3)/(6*Sqrt[3]) - (((5*I)/2)*5^(1/4)*
         G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((5*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1] + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1] + 
       (((25*I)/4)*5^(1/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       ((5*I)*5^(1/4)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       ((5*I)*5^(1/4)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((25*I)/4)*5^(1/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1] - 
       (((5*I)/4)*5^(1/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/Pi + 
       I*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
         1] + I*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
         1] + (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 
          1])/Pi - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 
         2/(1 + I*Sqrt[3]), 1] - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
         2/(1 + I*Sqrt[3]), 1, 1] - (I/2)*Sqrt[3]*5^(1/4)*
        G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
         1] + (((5*I)/4)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), 1, 
          (2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), -1, 
          1])/Pi + (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
          (I/2)*(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, 
          (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), -1, 
          1])/Pi - (((5*I)/4)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 1, 
          (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 1, 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, (-1/2*I)*(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + (I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((5*I)/2)*5^(1/4)*
         G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + (I*5^(1/4)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((2*I)*5^(1/4)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((2*I)*5^(1/4)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          -1, 1])/Pi - (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 
          -1, -1, 1])/Pi - (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/4)*5^(1/4)*
         G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/4)*5^(1/4)*
         G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (2*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) + 
       (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (((3*I)/2)*5^(1/4)*
         GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        Pi + (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/Pi - 
       (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*
        ((-I)*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*(-5 + Log[2]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(-5 + Log[2]))/(2*Pi)) + 
       (((5*I)/2)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*
         Log[2])/Pi - (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1]*Log[2])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*
         Log[2])/Pi + (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (2*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/Pi - ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/(4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/Pi + GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)) - (3*5^(1/4)*Log[2])/(2*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 - (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) - 
         (5^(1/4)*Log[2])/(2*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 - (5^(1/4)*Log[2])/
          (4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[0, -1, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, -2/(1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*((2*I)*Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((2*I)*Sqrt[3]*5^(1/4) - (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), -1, 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
           Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       G[0, 0, -2/(1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         (I*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, 
         2/(-1 + I*Sqrt[3]), 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*Log[2])/
          Pi) + G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) + 
         (I*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), -1, 
         -2/(1 + I*Sqrt[3]), 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (I*Sqrt[3]*5^(1/4) + (((3*I)/2)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 + (5^(1/4)*Log[2])/
          (4*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 + (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*(-1/24*(5^(1/4)*Pi) - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 - (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((-1/24*I)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((-1/24*I)*5^(1/4)*Pi - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((I/24)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((I/24)*5^(1/4)*Pi + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        (-1/6*(5^(1/4)*(I + 3*Sqrt[3])*Pi) - I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        (-1/4*(5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I - 3*Sqrt[3])*Pi)/6 - I*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 + (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4)*Log[2] - 
         (((3*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(I/2)*(I + Sqrt[3]), 1]*
        ((((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(2*I)/(-I + Sqrt[3]), 1]*
        ((((-5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
         (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(-2*I)/(I + Sqrt[3]), 1]*
        ((-5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((5^(1/4)*Pi)/24 - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 + (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GI[-1, -1/2 + (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4)*Log[2] + 
         (3*5^(1/4)*Log[2]^2)/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/4)*5^(1/4)*(-5 + Log[2])*
           Log[2]^2)/Pi) + GI[-1/2 - (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) + (5^(1/4)*(-5 + Log[2])*Log[2]^2)/
          (4*Pi)) + GI[-1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) - (5^(1/4)*Log[2]^3)/(4*Pi)) + 
       G[2/(-1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 - (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1])/2 - 
         (5*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 - (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 - (I/2)*Sqrt[3], -1] + (((5*I)/4)*5^(1/4)*
           GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + GR[-1/2 + (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
         (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) + (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 + (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 + (I/2)*Sqrt[3], -1] - 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi - (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/
          Pi) + GR[-1, -1/2 - (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*(3*Log[2]^2 - 5*Log[4]))/Pi) + 
       (((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/Pi + 
       G[(-2*I)/(I + Sqrt[3]), -1, 1]*((5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[4])/Pi) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
        ((5*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-38 + 5*Log[4]))/Pi) + 
       G[-1, (-2*I)/(I + Sqrt[3]), 1]*((-5*I)*Sqrt[3]*5^(1/4) + 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*(38 + 5*Log[4]))/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3]]*
        (Sqrt[3]*5^(1/4)*Log[2] + (5^(1/4)*(-3*Log[2]^2 + 5*Log[4]))/
          (4*Pi)) + GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(-5 + Log[8]))/Pi) + GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*(-5 + Log[8]))/(2*Pi)) + 
       G[0, (2*I)/(-I + Sqrt[3]), 1]*((5*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*(-19 + Log[32]))/Pi) + G[-1, (2*I)/(-I + Sqrt[3]), 1]*
        ((-5*I)*Sqrt[3]*5^(1/4) + (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
            1])/Pi + ((I/2)*5^(1/4)*(19 + Log[32]))/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (5^(1/4)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(Log[2] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(-5 + Log[8] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        (2*Sqrt[3]*5^(1/4) + (5^(1/4)*(-5 + Log[8] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + 
       ((I/12)*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/2]^3 + 
          (114 + 4*Log[2]^2 - 15*Log[4])*Log[3 - I*Sqrt[3]] + 
          15*Log[3 - I*Sqrt[3]]^2 - Log[(3 + I*Sqrt[3])/2]^3 - 
          114*Log[3 + I*Sqrt[3]] - 4*Log[2]^2*Log[3 + I*Sqrt[3]] + 
          15*Log[4]*Log[3 + I*Sqrt[3]] - 15*Log[3 + I*Sqrt[3]]^2))/Pi + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 - 
         ((I/4)*5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/Pi - I*Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(1 + (6*I)*Sqrt[3])*Pi)/12 + 
         (5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/(4*Pi) + Sqrt[3]*5^(1/4)*
          (-2*Log[4] + Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(Log[2] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) + (5^(1/4)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        (2*Sqrt[3]*5^(1/4) - (5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        ((-2*I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[8] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       G[(I/2)*(I + Sqrt[3]), -1, 1]*((-5*I)*Sqrt[3]*5^(1/4) + 
         ((I/4)*5^(1/4)*(-38 + 5*Log[4] - 10*Log[3 - I*Sqrt[3]] + 
            10*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((5^(1/4)*(-1 + (6*I)*Sqrt[3])*Pi)/12 + Sqrt[3]*5^(1/4)*
          (5 - 2*Log[4] + Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) - 
         (5^(1/4)*(38 + 3*Log[2]^2 - 5*Log[4] - Log[(3 - I*Sqrt[3])/2]^2 - 
            10*Log[3 - I*Sqrt[3]] + Log[(3 + I*Sqrt[3])/2]^2 + 
            10*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        ((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 - I*Sqrt[3]*5^(1/4)*
          (5 + Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]]) + 
         ((I/4)*5^(1/4)*(38 + 3*Log[2]^2 - 5*Log[4] - Log[(3 - I*Sqrt[3])/2]^
             2 - 10*Log[3 - I*Sqrt[3]] + Log[(3 + I*Sqrt[3])/2]^2 + 
            10*Log[3 + I*Sqrt[3]]))/Pi) + GR[-1/2 - (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3], -1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/
          Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
            Log[6 - (2*I)*Sqrt[3]]))/(2*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/Pi) + GI[-1/2 + (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3], -1]*(-(Sqrt[3]*5^(1/4)) + 
         (5^(1/4)*(-Log[3 - I*Sqrt[3]] + Log[6 + (2*I)*Sqrt[3]]))/(2*Pi)) + 
       (5^(1/4)*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
              Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
             (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
             Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
             Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
            Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
              Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
                (I + Sqrt[3])]))))/24 + (I/2)*Sqrt[3]*5^(1/4)*
        (8*Log[2]^2 + 2*Log[2]^3 + Log[2]*(10*Log[3 - I*Sqrt[3]] + 
           Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
            (10 + Log[3 + I*Sqrt[3]]) - Log[4]*(14 + Log[3 - I*Sqrt[3]] + 
             Log[3 + I*Sqrt[3]])) + 8*Zeta[3]))/EllipticK[(5 + Sqrt[5])/10] + 
     (EllipticK[(5 - Sqrt[5])/10]*((5*5^(1/4)*Pi^2)/(3*Sqrt[3]) + 
        (I/54)*5^(1/4)*(2*I + 3*Sqrt[3])*Pi^3 + 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/4 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I - 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I - 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/2 + 
        (5*5^(1/4)*(I - 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 12*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
           -2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 12*Sqrt[3])*
          G[0, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
           -2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*
          G[0, 2/(1 + I*Sqrt[3]), 1, 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
           -2/(1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
           -2/(-1 + I*Sqrt[3]), 1, 1])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(-1 + (8*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(1 + (8*I)*Sqrt[3])*
          GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 8*Sqrt[3])*
          GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
          Log[2])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
         4 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        G[0, 2/(-1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/4) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
            GI[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/
           4) + G[0, -2/(1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/4) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*
            GI[-1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
            GR[-1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/
           4) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 8*Sqrt[3])*Log[2])/4) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 8*Sqrt[3])*Log[2])/4) + G[2/(-1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I - 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I - 4*Sqrt[3])*Log[2]^2)/8) + G[-2/(1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2]^2)/8) - (I/2)*Sqrt[3]*5^(1/4)*Pi*
         Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]) + 
        GI[-1/2 + (I/2)*Sqrt[3], -1]*(-1/2*(Sqrt[3]*5^(1/4)*Pi) + 
          (5^(1/4)*((-16*I)*Sqrt[3]*Log[2] + Log[(3 - I*Sqrt[3])/2] + 
             (4*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]] + 
             (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GR[-1/2 - (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(5*I + 20*Sqrt[3] - I*Log[2] - 2*Sqrt[3]*Log[256] - 
             I*Log[3 - I*Sqrt[3]] + 4*Sqrt[3]*Log[3 - I*Sqrt[3]] + 
             I*Log[3 + I*Sqrt[3]] + 4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GR[-1/2 + (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I*Log[2] - 2*Sqrt[3]*Log[256] - I*Log[3 - I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] + I*Log[3 + I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
         (-1/2*(Sqrt[3]*5^(1/4)*Pi) + (5^(1/4)*(-5 + (20*I)*Sqrt[3] - 
             (16*I)*Sqrt[3]*Log[2] + (4*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]] + (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]] + 
             Log[6 - (2*I)*Sqrt[3]]))/4) + 
        (5^(1/4)*(-24*Sqrt[3]*Log[2]^3 + (5*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]]) + 3*Log[2]*
            (Sqrt[3]*Log[1208925819614629174706176] + 
             ((-I)*Log[4] + Sqrt[3]*(-40 + Log[256]))*Log[3 - I*Sqrt[3]] + 
             (I - 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
              (-40*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - I*
                Log[3 + I*Sqrt[3]] - 4*Sqrt[3]*Log[3 + I*Sqrt[3]])) - 
           96*Sqrt[3]*Zeta[3]))/24))/EllipticK[(5 + Sqrt[5])/10]^2) + 
   EisensteinH[2, 2, 1, 0, TauA]*
    ((((-5*I)/27)*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi^2 + 
       (5^(1/4)*Pi^3)/(6*Sqrt[3]) - (((5*I)/2)*5^(1/4)*
         G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((5*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1] + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1] + 
       (((25*I)/4)*5^(1/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       ((5*I)*5^(1/4)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       ((5*I)*5^(1/4)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((25*I)/4)*5^(1/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1] - 
       (((5*I)/4)*5^(1/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/Pi + 
       I*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
         1] + I*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
         1] + (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 
          1])/Pi - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 
         2/(1 + I*Sqrt[3]), 1] - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
         2/(1 + I*Sqrt[3]), 1, 1] - (I/2)*Sqrt[3]*5^(1/4)*
        G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
         1] + (((5*I)/4)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), 1, 
          (2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), -1, 
          1])/Pi + (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
          (I/2)*(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, 
          (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), -1, 
          1])/Pi - (((5*I)/4)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 1, 
          (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 1, 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, (-1/2*I)*(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + (I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((5*I)/2)*5^(1/4)*
         G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + (I*5^(1/4)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((2*I)*5^(1/4)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((2*I)*5^(1/4)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          -1, 1])/Pi - (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 
          -1, -1, 1])/Pi - (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/4)*5^(1/4)*
         G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/4)*5^(1/4)*
         G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (2*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) + 
       (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (((3*I)/2)*5^(1/4)*
         GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        Pi + (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/Pi - 
       (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*
        ((-I)*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*(-5 + Log[2]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(-5 + Log[2]))/(2*Pi)) + 
       (((5*I)/2)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*
         Log[2])/Pi - (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1]*Log[2])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*
         Log[2])/Pi + (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (2*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/Pi - ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/(4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/Pi + GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)) - (3*5^(1/4)*Log[2])/(2*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 - (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) - 
         (5^(1/4)*Log[2])/(2*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 - (5^(1/4)*Log[2])/
          (4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[0, -1, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, -2/(1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*((2*I)*Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((2*I)*Sqrt[3]*5^(1/4) - (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), -1, 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
           Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       G[0, 0, -2/(1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         (I*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, 
         2/(-1 + I*Sqrt[3]), 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*Log[2])/
          Pi) + G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) + 
         (I*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), -1, 
         -2/(1 + I*Sqrt[3]), 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (I*Sqrt[3]*5^(1/4) + (((3*I)/2)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 + (5^(1/4)*Log[2])/
          (4*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 + (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*(-1/24*(5^(1/4)*Pi) - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 - (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((-1/24*I)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((-1/24*I)*5^(1/4)*Pi - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((I/24)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((I/24)*5^(1/4)*Pi + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        (-1/6*(5^(1/4)*(I + 3*Sqrt[3])*Pi) - I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        (-1/4*(5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I - 3*Sqrt[3])*Pi)/6 - I*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 + (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4)*Log[2] - 
         (((3*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(I/2)*(I + Sqrt[3]), 1]*
        ((((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(2*I)/(-I + Sqrt[3]), 1]*
        ((((-5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
         (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(-2*I)/(I + Sqrt[3]), 1]*
        ((-5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((5^(1/4)*Pi)/24 - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 + (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GI[-1, -1/2 + (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4)*Log[2] + 
         (3*5^(1/4)*Log[2]^2)/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/4)*5^(1/4)*(-5 + Log[2])*
           Log[2]^2)/Pi) + GI[-1/2 - (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) + (5^(1/4)*(-5 + Log[2])*Log[2]^2)/
          (4*Pi)) + GI[-1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) - (5^(1/4)*Log[2]^3)/(4*Pi)) + 
       G[2/(-1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 - (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1])/2 - 
         (5*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 - (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 - (I/2)*Sqrt[3], -1] + (((5*I)/4)*5^(1/4)*
           GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + GR[-1/2 + (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
         (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) + (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 + (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 + (I/2)*Sqrt[3], -1] - 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi - (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/
          Pi) + GR[-1, -1/2 - (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*(3*Log[2]^2 - 5*Log[4]))/Pi) + 
       (((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/Pi + 
       G[(-2*I)/(I + Sqrt[3]), -1, 1]*((5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[4])/Pi) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
        ((5*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-38 + 5*Log[4]))/Pi) + 
       G[-1, (-2*I)/(I + Sqrt[3]), 1]*((-5*I)*Sqrt[3]*5^(1/4) + 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*(38 + 5*Log[4]))/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3]]*
        (Sqrt[3]*5^(1/4)*Log[2] + (5^(1/4)*(-3*Log[2]^2 + 5*Log[4]))/
          (4*Pi)) + GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(-5 + Log[8]))/Pi) + GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*(-5 + Log[8]))/(2*Pi)) + 
       G[0, (2*I)/(-I + Sqrt[3]), 1]*((5*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*(-19 + Log[32]))/Pi) + G[-1, (2*I)/(-I + Sqrt[3]), 1]*
        ((-5*I)*Sqrt[3]*5^(1/4) + (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
            1])/Pi + ((I/2)*5^(1/4)*(19 + Log[32]))/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (5^(1/4)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(Log[2] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(-5 + Log[8] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        (2*Sqrt[3]*5^(1/4) + (5^(1/4)*(-5 + Log[8] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + 
       ((I/12)*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/2]^3 + 
          (114 + 4*Log[2]^2 - 15*Log[4])*Log[3 - I*Sqrt[3]] + 
          15*Log[3 - I*Sqrt[3]]^2 - Log[(3 + I*Sqrt[3])/2]^3 - 
          114*Log[3 + I*Sqrt[3]] - 4*Log[2]^2*Log[3 + I*Sqrt[3]] + 
          15*Log[4]*Log[3 + I*Sqrt[3]] - 15*Log[3 + I*Sqrt[3]]^2))/Pi + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 - 
         ((I/4)*5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/Pi - I*Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(1 + (6*I)*Sqrt[3])*Pi)/12 + 
         (5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/(4*Pi) + Sqrt[3]*5^(1/4)*
          (-2*Log[4] + Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(Log[2] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) + (5^(1/4)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        (2*Sqrt[3]*5^(1/4) - (5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        ((-2*I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[8] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       G[(I/2)*(I + Sqrt[3]), -1, 1]*((-5*I)*Sqrt[3]*5^(1/4) + 
         ((I/4)*5^(1/4)*(-38 + 5*Log[4] - 10*Log[3 - I*Sqrt[3]] + 
            10*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((5^(1/4)*(-1 + (6*I)*Sqrt[3])*Pi)/12 + Sqrt[3]*5^(1/4)*
          (5 - 2*Log[4] + Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) - 
         (5^(1/4)*(38 + 3*Log[2]^2 - 5*Log[4] - Log[(3 - I*Sqrt[3])/2]^2 - 
            10*Log[3 - I*Sqrt[3]] + Log[(3 + I*Sqrt[3])/2]^2 + 
            10*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        ((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 - I*Sqrt[3]*5^(1/4)*
          (5 + Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]]) + 
         ((I/4)*5^(1/4)*(38 + 3*Log[2]^2 - 5*Log[4] - Log[(3 - I*Sqrt[3])/2]^
             2 - 10*Log[3 - I*Sqrt[3]] + Log[(3 + I*Sqrt[3])/2]^2 + 
            10*Log[3 + I*Sqrt[3]]))/Pi) + GR[-1/2 - (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3], -1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - Log[6 - (2*I)*Sqrt[3]]))/
          Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
            Log[6 - (2*I)*Sqrt[3]]))/(2*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/Pi) + GI[-1/2 + (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3], -1]*(-(Sqrt[3]*5^(1/4)) + 
         (5^(1/4)*(-Log[3 - I*Sqrt[3]] + Log[6 + (2*I)*Sqrt[3]]))/(2*Pi)) + 
       (5^(1/4)*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
              Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
             (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
             Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
             Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
            Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
              Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
                (I + Sqrt[3])]))))/24 + (I/2)*Sqrt[3]*5^(1/4)*
        (8*Log[2]^2 + 2*Log[2]^3 + Log[2]*(10*Log[3 - I*Sqrt[3]] + 
           Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
            (10 + Log[3 + I*Sqrt[3]]) - Log[4]*(14 + Log[3 - I*Sqrt[3]] + 
             Log[3 + I*Sqrt[3]])) + 8*Zeta[3]))/EllipticK[(5 + Sqrt[5])/10] + 
     (EllipticK[(5 - Sqrt[5])/10]*((5*5^(1/4)*Pi^2)/(3*Sqrt[3]) + 
        (I/54)*5^(1/4)*(2*I + 3*Sqrt[3])*Pi^3 + 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/4 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I - 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I - 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/2 + 
        (5*5^(1/4)*(I - 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 12*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
           -2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 12*Sqrt[3])*
          G[0, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
           -2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*
          G[0, 2/(1 + I*Sqrt[3]), 1, 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
           -2/(1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
           -2/(-1 + I*Sqrt[3]), 1, 1])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(-1 + (8*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(1 + (8*I)*Sqrt[3])*
          GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 8*Sqrt[3])*
          GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
          Log[2])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
         4 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        G[0, 2/(-1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/4) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
            GI[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/
           4) + G[0, -2/(1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/4) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*
            GI[-1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
            GR[-1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/
           4) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 8*Sqrt[3])*Log[2])/4) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 8*Sqrt[3])*Log[2])/4) + G[2/(-1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I - 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I - 4*Sqrt[3])*Log[2]^2)/8) + G[-2/(1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2]^2)/8) - (I/2)*Sqrt[3]*5^(1/4)*Pi*
         Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]) + 
        GI[-1/2 + (I/2)*Sqrt[3], -1]*(-1/2*(Sqrt[3]*5^(1/4)*Pi) + 
          (5^(1/4)*((-16*I)*Sqrt[3]*Log[2] + Log[(3 - I*Sqrt[3])/2] + 
             (4*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]] + 
             (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GR[-1/2 - (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(5*I + 20*Sqrt[3] - I*Log[2] - 2*Sqrt[3]*Log[256] - 
             I*Log[3 - I*Sqrt[3]] + 4*Sqrt[3]*Log[3 - I*Sqrt[3]] + 
             I*Log[3 + I*Sqrt[3]] + 4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GR[-1/2 + (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I*Log[2] - 2*Sqrt[3]*Log[256] - I*Log[3 - I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] + I*Log[3 + I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
         (-1/2*(Sqrt[3]*5^(1/4)*Pi) + (5^(1/4)*(-5 + (20*I)*Sqrt[3] - 
             (16*I)*Sqrt[3]*Log[2] + (4*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]] + (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]] + 
             Log[6 - (2*I)*Sqrt[3]]))/4) + 
        (5^(1/4)*(-24*Sqrt[3]*Log[2]^3 + (5*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]]) + 3*Log[2]*
            (Sqrt[3]*Log[1208925819614629174706176] + 
             ((-I)*Log[4] + Sqrt[3]*(-40 + Log[256]))*Log[3 - I*Sqrt[3]] + 
             (I - 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
              (-40*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - I*
                Log[3 + I*Sqrt[3]] - 4*Sqrt[3]*Log[3 + I*Sqrt[3]])) - 
           96*Sqrt[3]*Zeta[3]))/24))/EllipticK[(5 + Sqrt[5])/10]^2) + 
   EisensteinH[2, 1, 0, 0, TauA]*
    (((5*5^(1/4)*(2 + (3*I)*Sqrt[3])*Pi^2)/27 - (5^(1/4)*Pi^3)/(6*Sqrt[3]) + 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((5*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1] - 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1] - 
       (((25*I)/4)*5^(1/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
       ((5*I)*5^(1/4)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       ((5*I)*5^(1/4)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((25*I)/4)*5^(1/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1] + 
       (((5*I)/4)*5^(1/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/Pi - 
       I*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
         1] - I*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
         1] - (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 
          1])/Pi + (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 
         2/(1 + I*Sqrt[3]), 1] + (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
         2/(1 + I*Sqrt[3]), 1, 1] + (I/2)*Sqrt[3]*5^(1/4)*
        G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
         1] - (((5*I)/4)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), 1, 
          (2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), -1, 
          1])/Pi - (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
          (I/2)*(I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 
          1])/Pi - (((5*I)/2)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, 
          (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), -1, 
          1])/Pi + (((5*I)/4)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 1, 
          (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 1, 
          1])/Pi + (((5*I)/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, (-1/2*I)*(-I + Sqrt[3]), 
          1])/Pi - (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), -1, 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - (I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/2)*5^(1/4)*
         G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/Pi + ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((5*I)/2)*5^(1/4)*
         G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - (I*5^(1/4)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((2*I)*5^(1/4)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((2*I)*5^(1/4)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          -1, 1])/Pi + (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/4)*5^(1/4)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        Pi - ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 
          -1, -1, 1])/Pi + (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - (((3*I)/2)*5^(1/4)*
         G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi - ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/4)*5^(1/4)*
         G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/4)*5^(1/4)*
         G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) + (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (2*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) + (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) - 
       (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (((3*I)/2)*5^(1/4)*
         GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi - ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        Pi - (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/Pi + 
       (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*
        (-(Sqrt[3]*5^(1/4)) - (5^(1/4)*(-5 + Log[2]))/(2*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*(I*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*(-5 + Log[2]))/Pi) - 
       (((5*I)/2)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/Pi - 
       (((5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*
         Log[2])/Pi + (((5*I)/4)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1]*Log[2])/Pi + 
       (((5*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*
         Log[2])/Pi - (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (2*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/Pi + ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/(4*Pi) + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/Pi + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (5^(1/4)*Log[2])/(4*Pi)) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((3*I)*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*((-2*I)*Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, -2/(1 + I*Sqrt[3]), -1, 1]*((3*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), -1, 1]*(-1/4*(5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
           Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
        ((-I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + GR[-1/2 + (I/2)*Sqrt[3], -1, 
         -1/2 - (I/2)*Sqrt[3]]*((I/2)*Sqrt[3]*5^(1/4) - 
         (((3*I)/4)*5^(1/4)*Log[2])/Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, 
         -1/2 + (I/2)*Sqrt[3]]*((I/2)*Sqrt[3]*5^(1/4) + 
         (((3*I)/4)*5^(1/4)*Log[2])/Pi) + G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*
        ((2*I)*Sqrt[3]*5^(1/4) - (I*5^(1/4)*Log[2])/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*Log[2])/
          Pi) + G[0, 0, -2/(1 + I*Sqrt[3]), 1]*((2*I)*Sqrt[3]*5^(1/4) + 
         (I*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, 
         2/(-1 + I*Sqrt[3]), 1]*(-1/4*(5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4) - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3]]*(-1/2*(Sqrt[3]*5^(1/4)) + 
         (5^(1/4)*Log[2])/(4*Pi)) + GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*Log[2])/(2*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)) + (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*Log[2])/(2*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)*Log[2]) - (3*5^(1/4)*Log[2]^2)/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*(-1/24*(5^(1/4)*Pi) + 
         (Sqrt[3]*5^(1/4)*Log[2])/2 - (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((-1/24*I)*5^(1/4)*Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((-1/24*I)*5^(1/4)*Pi - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((I/24)*5^(1/4)*Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((I/24)*5^(1/4)*Pi + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(-I + 3*Sqrt[3])*Pi)/6 + I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (-1/12*(5^(1/4)*(I + 6*Sqrt[3])*Pi) - 
         (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/2 + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] - 
         ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         I*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*
        (-1/2*(Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]) + 
         (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        (-1/4*(5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I + 3*Sqrt[3])*Pi)/6 + I*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I - 6*Sqrt[3])*Pi)/12 - (Sqrt[3]*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3]])/2 - 
         (5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         I*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*
        (-1/2*(Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]) - 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4)*Log[2] + 
         (((3*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(-2*I)/(I + Sqrt[3]), 1]*
        ((5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(I/2)*(I + Sqrt[3]), 1]*
        ((((-5*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(2*I)/(-I + Sqrt[3]), 1]*
        ((((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/Pi + 
         (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
         (((5*I)/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((5^(1/4)*Pi)/24 + 
         (Sqrt[3]*5^(1/4)*Log[2])/2 + (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4)*Log[2]^2)/2 - 
         (5^(1/4)*(-5 + Log[2])*Log[2]^2)/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*(-5 + Log[2])*
           Log[2]^2)/Pi) + GR[-1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/4)*5^(1/4)*Log[2]^3)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/2 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/2 - 
         (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 + (I/2)*Sqrt[3]] - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 + (I/2)*Sqrt[3], -1] + 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi + (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi - (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/4)*5^(1/4)*Log[2]^3)/
          Pi) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/2 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
         (5*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 - (I/2)*Sqrt[3]] - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 - (I/2)*Sqrt[3], -1] - (((5*I)/4)*5^(1/4)*
           GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi + ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi - (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + GI[-1/2 + (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4)*Log[2]^2)/2 + (5^(1/4)*Log[2]^3)/(4*Pi)) + 
       GR[-1, -1/2 - (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*(3*Log[2]^2 - 5*Log[4]))/Pi) + 
       GI[-1, -1/2 - (I/2)*Sqrt[3]]*(-(Sqrt[3]*5^(1/4)*Log[2]) + 
         (5^(1/4)*(3*Log[2]^2 - 5*Log[4]))/(4*Pi)) - 
       (((5*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/Pi + 
       G[(-2*I)/(I + Sqrt[3]), -1, 1]*((-5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*Log[4])/Pi) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
        ((-5*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(-38 + 5*Log[4]))/Pi) + 
       G[-1, (-2*I)/(I + Sqrt[3]), 1]*((5*I)*Sqrt[3]*5^(1/4) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*(38 + 5*Log[4]))/Pi) + 
       GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4) - 
         (5^(1/4)*(-5 + Log[8]))/(2*Pi)) + GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(-5 + Log[8]))/Pi) + 
       G[0, (2*I)/(-I + Sqrt[3]), 1]*((-5*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(-19 + Log[32]))/Pi) + G[-1, (2*I)/(-I + Sqrt[3]), 1]*
        ((5*I)*Sqrt[3]*5^(1/4) - (((5*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
            1])/Pi - ((I/2)*5^(1/4)*(19 + Log[32]))/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(Log[2] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((Sqrt[3]*5^(1/4))/2 + (5^(1/4)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        (-2*Sqrt[3]*5^(1/4) - (5^(1/4)*(-5 + Log[8] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        ((2*I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(-5 + Log[8] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) - 
       ((I/12)*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/2]^3 + 
          (114 + 4*Log[2]^2 - 15*Log[4])*Log[3 - I*Sqrt[3]] + 
          15*Log[3 - I*Sqrt[3]]^2 - Log[(3 + I*Sqrt[3])/2]^3 - 
          114*Log[3 + I*Sqrt[3]] - 4*Log[2]^2*Log[3 + I*Sqrt[3]] + 
          15*Log[4]*Log[3 + I*Sqrt[3]] - 15*Log[3 + I*Sqrt[3]]^2))/Pi + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(-1 - (6*I)*Sqrt[3])*Pi)/12 - 
         (5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/(4*Pi) - Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(I - 6*Sqrt[3])*Pi)/12 + 
         ((I/4)*5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/Pi + I*Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((Sqrt[3]*5^(1/4))/2 - (5^(1/4)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(Log[2] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*((2*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        (-2*Sqrt[3]*5^(1/4) + (5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + G[(I/2)*(I + Sqrt[3]), -1, 1]*
        ((5*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-38 + 5*Log[4] - 
            10*Log[3 - I*Sqrt[3]] + 10*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*(-1/12*(5^(1/4)*(I + 6*Sqrt[3])*Pi) + 
         I*Sqrt[3]*5^(1/4)*(5 + Log[(3 - I*Sqrt[3])/16] + 
           Log[3 + I*Sqrt[3]]) - ((I/4)*5^(1/4)*(38 + 3*Log[2]^2 - 5*Log[4] - 
            Log[(3 - I*Sqrt[3])/2]^2 - 10*Log[3 - I*Sqrt[3]] + 
            Log[(3 + I*Sqrt[3])/2]^2 + 10*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*((5^(1/4)*(1 - (6*I)*Sqrt[3])*Pi)/12 - 
         Sqrt[3]*5^(1/4)*(5 + Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]]) + 
         (5^(1/4)*(38 + 3*Log[2]^2 - 5*Log[4] - Log[(3 - I*Sqrt[3])/2]^2 - 
            10*Log[3 - I*Sqrt[3]] + Log[(3 + I*Sqrt[3])/2]^2 + 
            10*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (Sqrt[3]*5^(1/4) - (5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
            Log[6 - (2*I)*Sqrt[3]]))/(2*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(5 + Log[3 + I*Sqrt[3]] - 
            Log[6 - (2*I)*Sqrt[3]]))/Pi) + GR[-1/2 + (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3], -1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (Sqrt[3]*5^(1/4) + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(2*Pi)) - 
       (5^(1/4)*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
              Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
             (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
             Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
             Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
            Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
              Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
                (I + Sqrt[3])]))))/24 - (I/2)*Sqrt[3]*5^(1/4)*
        (8*Log[2]^2 + 2*Log[2]^3 + Log[2]*(10*Log[3 - I*Sqrt[3]] + 
           Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
            (10 + Log[3 + I*Sqrt[3]]) - Log[4]*(14 + Log[3 - I*Sqrt[3]] + 
             Log[3 + I*Sqrt[3]])) + 8*Zeta[3]))/EllipticK[(5 + Sqrt[5])/10] + 
     (EllipticK[(5 - Sqrt[5])/10]*((-5*5^(1/4)*Pi^2)/(3*Sqrt[3]) + 
        (5^(1/4)*(2 - (3*I)*Sqrt[3])*Pi^3)/54 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I - 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I - 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/2 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/2 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(I - 12*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
         4 + (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 12*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(I - 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
           -2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
           1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
           -2/(-1 + I*Sqrt[3]), 1, 1])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(1 - (8*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 - (8*I)*Sqrt[3])*
          GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 - 
        (5^(1/4)*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 8*Sqrt[3])*
          GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1]*Log[2])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(I - 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        G[2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*(-1 - (4*I)*Sqrt[3])*
            GI[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 8*Sqrt[3])*Log[2])/
           4) + G[0, 2/(-1 + I*Sqrt[3]), 1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I - 4*Sqrt[3])*Log[2])/4) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
         ((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
            GI[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I - 4*Sqrt[3])*Log[2])/
           4) + G[0, -2/(1 + I*Sqrt[3]), 1]*((I/2)*Sqrt[3]*5^(1/4)*Pi - 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/4) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
         ((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
            GI[-1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
            GR[-1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/
           4) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 8*Sqrt[3])*Log[2])/4) + G[2/(-1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2]^2)/8) + G[-2/(1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2]^2)/8) + (I/2)*Sqrt[3]*5^(1/4)*Pi*
         Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]) + 
        GR[-1/2 + (I/2)*Sqrt[3], -1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*((-I)*Log[2] + 2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
         ((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(-5*I - 20*Sqrt[3] + 
             I*Log[2] + 2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
         ((Sqrt[3]*5^(1/4)*Pi)/2 + (5^(1/4)*(5 - (20*I)*Sqrt[3] + 
             (16*I)*Sqrt[3]*Log[2] + (-1 - (4*I)*Sqrt[3])*
              Log[3 - I*Sqrt[3]] + Log[(3 + I*Sqrt[3])/2] - 
             (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GI[-1/2 + (I/2)*Sqrt[3], -1]*((Sqrt[3]*5^(1/4)*Pi)/2 + 
          (5^(1/4)*((16*I)*Sqrt[3]*Log[2] + (-1 - (4*I)*Sqrt[3])*
              Log[3 - I*Sqrt[3]] - (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]] + 
             Log[6 + (2*I)*Sqrt[3]]))/4) + 
        (5^(1/4)*(24*Sqrt[3]*Log[2]^3 - (5*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]]) - 3*Log[2]*
            (Sqrt[3]*Log[1208925819614629174706176] + 
             ((-I)*Log[4] + Sqrt[3]*(-40 + Log[256]))*Log[3 - I*Sqrt[3]] + 
             (I - 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
              (-40*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - I*
                Log[3 + I*Sqrt[3]] - 4*Sqrt[3]*Log[3 + I*Sqrt[3]])) + 
           96*Sqrt[3]*Zeta[3]))/24))/EllipticK[(5 + Sqrt[5])/10]^2)}
