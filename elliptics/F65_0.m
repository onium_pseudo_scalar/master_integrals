(* Created with the Wolfram Language : www.wolfram.com *)
{F[65, 0] -> -1/2 + Pi^2/12 - ((2*I)*5^(1/4)*IEI[{3, 6, 1, 0}, TauA])/
    (Pi*EllipticK[(5 + Sqrt[5])/10]) - 
   ((2*I)*5^(1/4)*IEI[{3, 6, 4, 3}, TauA])/(Pi*EllipticK[(5 + Sqrt[5])/10]) + 
   ((5*I)*5^(1/4)*IEI[{3, 6, 5, 3}, TauA])/(Pi*EllipticK[(5 + Sqrt[5])/10]) + 
   (-((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
       (Pi^2*EllipticK[(5 + Sqrt[5])/10])) + 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^2))*IEI[{0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (-((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
       (Pi^2*EllipticK[(5 + Sqrt[5])/10])) + 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - (14*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^2))*IEI[{0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
     (7*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^2))*
    IEI[{0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + EllipticK[(5 + Sqrt[5])/10]*
    ((((-7*I)/3)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((7*I)/3)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (7*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
     (((7*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((7*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
      (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
     (((-1/2*I)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
      ((I/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
      ((I/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
      ((I/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
      (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
      ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
      ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
    EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
     (((I/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
      ((I/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
      (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
      ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
      ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
    EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
     (((I/2)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
      ((I/2)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
      ((I/2)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
      (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
      ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
      ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
    EllipticK[(5 + Sqrt[5])/10]}
