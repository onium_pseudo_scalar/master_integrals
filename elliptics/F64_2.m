(* Created with the Wolfram Language : www.wolfram.com *)
{F[64, 2] -> (85*Pi^2)/48 + (7*Pi^4)/240 + 
   (((((-115*I)/2)*5^(1/4))/Pi - (I/3)*5^(1/4)*Pi)*IEI[{3, 6, 1, 0}, TauA])/
    EllipticK[(5 + Sqrt[5])/10] + 
   (((((-115*I)/2)*5^(1/4))/Pi - (I/3)*5^(1/4)*Pi)*IEI[{3, 6, 4, 3}, TauA])/
    EllipticK[(5 + Sqrt[5])/10] + 
   (((((575*I)/4)*5^(1/4))/Pi + ((5*I)/6)*5^(1/4)*Pi)*
     IEI[{3, 6, 5, 3}, TauA])/EllipticK[(5 + Sqrt[5])/10] + 
   (((-1/6*5^(1/4) - (115*5^(1/4))/(4*Pi^2))*EisensteinH[2, 1, 0, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + ((5^(1/4)/6 + (115*5^(1/4))/(4*Pi^2))*
       EisensteinH[2, 2, 0, 1, TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     ((5^(1/4)/6 + (115*5^(1/4))/(4*Pi^2))*EisensteinH[2, 2, 1, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + (-17/(9*5^(1/4)) - 201/(2*5^(1/4)*Pi^2))*
      EllipticK[(5 + Sqrt[5])/10])*IEI[{0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((-1/6*5^(1/4) - (115*5^(1/4))/(4*Pi^2))*EisensteinH[2, 1, 0, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + ((5^(1/4)/6 + (115*5^(1/4))/(4*Pi^2))*
       EisensteinH[2, 2, 0, 1, TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     ((5^(1/4)/6 + (115*5^(1/4))/(4*Pi^2))*EisensteinH[2, 2, 1, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + (-17/(9*5^(1/4)) - 201/(2*5^(1/4)*Pi^2))*
      EllipticK[(5 + Sqrt[5])/10])*IEI[{0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((5*5^(1/4))/12 + (575*5^(1/4))/(8*Pi^2))*EisensteinH[2, 1, 0, 0, TauA])/
      EllipticK[(5 + Sqrt[5])/10] + 
     (((-5*5^(1/4))/12 - (575*5^(1/4))/(8*Pi^2))*EisensteinH[2, 2, 0, 1, 
        TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     (((-5*5^(1/4))/12 - (575*5^(1/4))/(8*Pi^2))*EisensteinH[2, 2, 1, 0, 
        TauA])/EllipticK[(5 + Sqrt[5])/10] + 
     ((17*5^(3/4))/18 + (201*5^(3/4))/(4*Pi^2))*EllipticK[(5 + Sqrt[5])/10])*
    IEI[{0, 0, 0, 0}, {3, 6, 5, 3}, TauA] - 
   (26*5^(1/4)*IEI[{2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (26*5^(1/4)*IEI[{2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (65*5^(1/4)*IEI[{2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (13*5^(1/4)*IEI[{2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (13*5^(1/4)*IEI[{2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (65*5^(1/4)*IEI[{2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (2*Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (39*5^(1/4)*IEI[{2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (39*5^(1/4)*IEI[{2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (195*5^(1/4)*IEI[{2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (2*Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (26*5^(1/4)*IEI[{2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) - 
   (26*5^(1/4)*IEI[{2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (65*5^(1/4)*IEI[{2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^2*EllipticK[(5 + Sqrt[5])/10]) + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA] + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA] + 
   ((((-65*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((((-13*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA] + 
   ((((-13*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA] + 
   ((((65*I)/4)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((65*I)/4)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((65*I)/4)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((((-39*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((117*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA] + 
   ((((-39*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((117*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA] + 
   ((((195*I)/4)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((195*I)/4)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((195*I)/4)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((117*I)/2)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA] + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA] + 
   ((((-65*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((-65*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((((-13*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   ((((-13*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((13*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((65*I)/4)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((65*I)/4)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((65*I)/4)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((2*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((2*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((5*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((15*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((((-39*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((117*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   ((((-39*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((39*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((117*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((195*I)/4)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((195*I)/4)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     (((195*I)/4)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((117*I)/2)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((6*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((15*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((18*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((18*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((45*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA] + 
   (((13*I)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((13*I)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     ((78*I)*EllipticK[(5 + Sqrt[5])/10])/(5^(1/4)*Pi^3))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA] + 
   ((((-65*I)/2)*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
     (((65*I)/2)*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
     ((39*I)*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^3)*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((4*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((10*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((12*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((30*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((8*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((20*I)*5^(1/4)*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((180*I)*5^(1/4)*IEI[{4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((135*I)*5^(1/4)*IEI[{4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((135*I)*5^(1/4)*IEI[{4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((72*I)*5^(1/4)*IEI[{4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((180*I)*5^(1/4)*IEI[{4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((54*I)*5^(1/4)*IEI[{4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((135*I)*5^(1/4)*IEI[{4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((216*I)*5^(1/4)*IEI[{4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((540*I)*5^(1/4)*IEI[{4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((18*I)*5^(1/4)*IEI[{4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 1, 0}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) - 
   ((18*I)*5^(1/4)*IEI[{4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 4, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((45*I)*5^(1/4)*IEI[{4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA])/
    (Pi^3*EllipticK[(5 + Sqrt[5])/10]) + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (51*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (612*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (408*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (408*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((90*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (204*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (306*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (306*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((135*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (153*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (612*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (612*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (306*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (306*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((135*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (153*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (612*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (612*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (408*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-36*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (36*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (408*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((90*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (90*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (204*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (306*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-27*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (27*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (306*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((135*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (135*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (153*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-108*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (108*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (1224*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((270*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (270*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (612*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (51*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((-45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (51*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((3*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (3*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (34*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (17*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((9*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (9*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (102*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-45*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (45*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (2*Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (51*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 1, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 2, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 3, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, {2, 6, 4, 3}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-2*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (2*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((5*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (5*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((-6*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (6*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - (68*EllipticK[(5 + Sqrt[5])/10])/
      (5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((15*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (15*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (34*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/Pi^4)*
    IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 1, 0}, TauA] + ((4*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (4*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + (136*EllipticK[(5 + Sqrt[5])/10])/
      (3*5^(1/4)*Pi^4))*IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, 
     {3, 6, 4, 3}, TauA] + ((-10*5^(1/4)*EisensteinH[2, 1, 0, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 0, 1, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) + 
     (10*5^(1/4)*EisensteinH[2, 2, 1, 0, TauA])/
      (Pi^4*EllipticK[(5 + Sqrt[5])/10]) - 
     (68*5^(3/4)*EllipticK[(5 + Sqrt[5])/10])/(3*Pi^4))*
    IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, {0, 0, 0, 0}, {3, 6, 5, 3}, TauA] + 
   IEI[{2, 6, 2, 0}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((17*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((34*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((34*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((34*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((34*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((34*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((34*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((34*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((34*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((34*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((34*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((34*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((34*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((34*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((34*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((34*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 4, 3}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((34*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((34*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((34*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) - 
       (((34*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((34*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((34*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (I*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (I*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (I*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^3))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((-17*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((17*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((17*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((17*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((17*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((17*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((17*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       ((((-3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((-17*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((17*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((17*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((17*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((17*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((17*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((17*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       ((((-3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((3*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((3*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((3*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-68*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((68*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (68*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (68*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((68*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((68*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((68*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-68*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((68*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (68*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (68*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((68*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((68*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((68*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 4, 3}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-68*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((68*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (68*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (68*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((68*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((68*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((68*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 4, 3}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-68*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (((68*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (((68*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (68*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (68*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^3) + 
       (((68*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (((68*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (((68*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       (((-2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((2*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((2*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (2*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (2*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((2*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((2*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 1, 0}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((34*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((34*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((34*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((34*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((34*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((34*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((34*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((34*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((34*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 4, 3}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((34*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((34*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((34*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((34*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((34*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-3*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((3*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((3*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((3*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((3*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((-51*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((51*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((51*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((51*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - (51*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       (51*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((51*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((51*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((51*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 2, 0, 1, TauA]*
       ((((9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 1, 0, 0, TauA]*
       ((((-9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 5, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((51*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((51*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((51*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((51*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (51*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (51*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((51*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((51*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((51*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-9*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (9*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (9*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((9*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((9*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((9*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 1, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((153*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((153*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((153*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((153*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (153*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (153*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((153*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((153*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((153*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 3, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((153*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((153*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((153*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((153*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (153*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (153*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((153*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((153*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((153*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 5, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((153*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((153*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((153*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((153*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (153*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (153*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((153*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((153*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((153*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-27*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (27*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (27*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^3) + 
        (((27*I)/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (((27*I)/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        (((27*I)/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((204*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((204*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((204*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((204*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (204*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (204*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((204*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((204*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((204*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((204*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((204*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((204*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((204*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (204*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (204*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((204*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((204*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((204*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-18*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((18*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((18*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (18*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (18*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((18*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((18*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 0, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((612*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((612*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (612*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (612*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((612*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 1, 2}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((612*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((612*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (612*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (612*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((612*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 2, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((612*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((612*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (612*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (612*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((612*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 3, 2}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((612*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((612*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (612*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (612*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((612*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 4, 1}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((612*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((612*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (612*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (612*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((612*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {4, 6, 5, 2}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*(((612*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) - ((612*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + ((612*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^3) + (612*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       (612*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) + 
       ((612*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^3) - 
       ((612*I)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi^3)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       (((-54*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^3 + 
        ((54*I)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^3 - 
        ((54*I)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^3 - 
        (54*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 + 
        (54*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^3 - 
        ((54*I)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^3 + 
        ((54*I)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi^3))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{0, 0, 0, 0}, {2, 6, 2, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/6)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/6)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 2, 0}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/6)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/6)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       (((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 0, 1, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((-1/4*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{0, 0, 0, 0}, {2, 6, 1, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{0, 0, 0, 0}, {2, 6, 4, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 1, 0}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{2, 6, 4, 3}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((-17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 2, 0, 1, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 2, 1, 0, TauA]*
       (((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10] + (EisensteinH[2, 1, 0, 0, TauA]*
       (((-1/2*I)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
        (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
        ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        ((I/2)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi))/
      EllipticK[(5 + Sqrt[5])/10]) + IEI[{0, 0, 0, 0}, {2, 6, 3, 3}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((17*I)/2)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/2)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/2)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/2)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (((17*I)/2)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/2)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/2)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10]) + 
   IEI[{2, 6, 3, 3}, {0, 0, 0, 0}, TauA]*
    (EllipticK[(5 + Sqrt[5])/10]*((((17*I)/2)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/2)*G[-1, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) - (((17*I)/2)*G[0, 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (((17*I)/2)*G[0, -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (((17*I)/2)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/2)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/2)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (EisensteinH[2, 1, 0, 0, TauA]*
       ((((3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 0, 1, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10] + 
     (EisensteinH[2, 2, 1, 0, TauA]*
       ((((-3*I)/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
        (((3*I)/4)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
        (((3*I)/4)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
        (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
        (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
        (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
        (((3*I)/4)*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/
         Pi))/EllipticK[(5 + Sqrt[5])/10]) + 
   (IEI[{4, 6, 5, 3}, TauA]*((-9*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
      (9*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (9*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (9*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((9*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((9*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (9*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (9*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (9*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 1, 1}, TauA]*((-27*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (27*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (27*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (27*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((27*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((27*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (27*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 3, 1}, TauA]*((-27*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (27*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (27*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (27*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((27*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((27*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (27*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 5, 1}, TauA]*((-27*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (27*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (27*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (27*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((27*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((27*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (27*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (27*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 1, 0}, TauA]*((-36*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (36*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (36*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (36*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((36*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((36*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (36*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 4, 3}, TauA]*((-36*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (36*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (36*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (36*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((36*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((36*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (36*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (36*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 0, 1}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 1, 2}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 2, 1}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 3, 2}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 4, 1}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + 
   (IEI[{4, 6, 5, 2}, TauA]*((-108*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/
       Pi^2 + (108*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      (108*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
      (108*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
      ((108*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      ((108*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
      (108*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
      (108*5^(1/4)*Log[2]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]))/Pi^2))/
    EllipticK[(5 + Sqrt[5])/10] + IEI[{2, 6, 2, 0}, TauA]*
    (EllipticK[(5 - Sqrt[5])/10]*((((-34*I)/9)*Pi)/(Sqrt[3]*5^(1/4)) - 
       ((34*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((34*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((34*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((34*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((34*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((34*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((34*I)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)*Pi)) + ((((-2*I)/3)*5^(1/4)*Pi^2)/Sqrt[3] + 
       (5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (5^(1/4)*(1 - (4*I)*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1])/2 + 
       (5^(1/4)*(1 + (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (I/2)*5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1] + 
       (5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*(1 - (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (5^(1/4)*Log[2]*((-4*I)*Sqrt[3]*Log[4] + (1 + (4*I)*Sqrt[3])*
           Log[3 - I*Sqrt[3]] + (-1 + (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/2)/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((-1/3*I)*5^(1/4)*Pi)/Sqrt[3] - 
          (I*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       (-1/27*(5^(1/4)*(-2*I + 3*Sqrt[3])*Pi) + 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) - 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) - 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) - 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(4*Pi^2) + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            -1, 1])/(4*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) + 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) - ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(4*Pi^2) + ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/4*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
            Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
          ((Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
            Pi^2 + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/4*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(4*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-I)*Sqrt[3]*5^(1/4))/Pi + ((I/4)*5^(1/4)*
             (13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*(-((Sqrt[3]*5^(1/4))/Pi) + 
           (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/
            (4*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          (-((Sqrt[3]*5^(1/4))/Pi) - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 + (I/2)*Sqrt[3], -1]*(((-I)*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((I/3)*5^(1/4)*Pi)/Sqrt[3] + 
          (I*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) - 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            -1, 1])/(4*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) + ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(4*Pi^2) - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*(-((Sqrt[3]*5^(1/4))/Pi) - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/4*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi - 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + (5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi + 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/4*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) - (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*((I*Sqrt[3]*5^(1/4))/Pi - 
           ((I/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          ((I*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)))/
        EllipticK[(5 + Sqrt[5])/10]) + EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((I/3)*5^(1/4)*Pi)/Sqrt[3] + 
          (I*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (I*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          (I*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) - 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            -1, 1])/(4*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (4*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) + ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(4*Pi^2) - ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*(-((Sqrt[3]*5^(1/4))/Pi) - 
           (5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/4*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi - 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          (-((Sqrt[3]*5^(1/4))/Pi) + (5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((Sqrt[3]*5^(1/4))/Pi + 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + (5^(1/4)*Log[2])/
            (2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/4*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(4*Pi^2)) - (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*((I*Sqrt[3]*5^(1/4))/Pi - 
           ((I/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          ((I*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)))/
        EllipticK[(5 + Sqrt[5])/10]) + EllipticK[(5 + Sqrt[5])/10]*
      ((-34*(-2*I + 3*Sqrt[3])*Pi)/(81*5^(1/4)) + 
       (39*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (39*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (39*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) + 
       (39*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (39*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*5^(1/4)*Pi^2) + 
       (17*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (17*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) - (17*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) - (17*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) + (17*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(6*Pi^2) - 
       (34*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(6*Pi^2) + 
       (17*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (17*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (17*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
        (6*5^(1/4)*Pi^2) - (17*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) + (17*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (17*G[0, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (17*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (17*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) + (17*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) - (17*G[-2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) - 
       (17*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        (6*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        (3*5^(1/4)*Pi^2) - (17*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/(6*5^(1/4)*Pi^2) - (17*G[2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/(6*5^(1/4)*Pi^2) - 
       (17*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        (6*5^(1/4)*Pi^2) + (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/(6*5^(1/4)*Pi^2) + (17*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)*Pi^2) + 
       (17*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
        (6*5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((17*I)/6)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((17*I)/6)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) + (((17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) - 
       (((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (17*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
       (17*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
       (17*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
       (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
       (17*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (6*5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) - (17*G[2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/(6*5^(1/4)*Pi^2) + 
       (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*Log[2])/
        (6*5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (((17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) + 
       (((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (17*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(6*5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(6*5^(1/4)*Pi^2) + 
       (((17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (17*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(6*5^(1/4)*Pi^2) - 
       (17*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(6*5^(1/4)*Pi^2) + 
       G[0, 2/(-1 + I*Sqrt[3]), 1]*(34/(Sqrt[3]*5^(1/4)*Pi) - 
         (17*Log[2])/(3*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((((-17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (17*GR[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
         (17*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (-34/(Sqrt[3]*5^(1/4)*Pi) - (((17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
         (17*Log[2])/(3*5^(1/4)*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        (34/(Sqrt[3]*5^(1/4)*Pi) + (17*Log[2])/(3*5^(1/4)*Pi^2)) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*((((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
         (17*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (-34/(Sqrt[3]*5^(1/4)*Pi) + (((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
         (17*Log[2])/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((-17*I)/6)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((17*I)/6)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (17*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) - 
         (17*GR[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi^2) - 
         (17*Log[2]^2)/(6*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((17*I)/6)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((17*I)/6)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (17*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi^2) + 
         (17*GR[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi^2) + 
         (17*Log[2]^2)/(6*5^(1/4)*Pi^2)) + 
       (Log[2]*(17*Log[(3 - I*Sqrt[3])/2]^2 + 117*Log[3 - I*Sqrt[3]] - 
          17*Log[(3 + I*Sqrt[3])/2]^2 - 117*Log[3 + I*Sqrt[3]]))/
        (6*5^(1/4)*Pi^2) - (17*Log[2]*(Log[256] - 
          4*(Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])))/
        (2*Sqrt[3]*5^(1/4)*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((-34*I)/(Sqrt[3]*5^(1/4)*Pi) + ((I/6)*(117 + 34*Log[3 + I*Sqrt[3]] - 
            34*Log[6 - (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*(-34/(Sqrt[3]*5^(1/4)*Pi) + 
         (117 + 34*Log[3 + I*Sqrt[3]] - 34*Log[6 - (2*I)*Sqrt[3]])/
          (6*5^(1/4)*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
        (-34/(Sqrt[3]*5^(1/4)*Pi) - (17*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(3*5^(1/4)*Pi^2)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((-34*I)/(Sqrt[3]*5^(1/4)*Pi) - 
         (((17*I)/3)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)))) + IEI[{2, 6, 3, 3}, TauA]*
    (EllipticK[(5 - Sqrt[5])/10]*((((-34*I)/3)*Pi)/(Sqrt[3]*5^(1/4)) - 
       ((34*I)*Sqrt[3]*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       ((34*I)*Sqrt[3]*G[-1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       ((34*I)*Sqrt[3]*G[0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       ((34*I)*Sqrt[3]*G[0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (34*Sqrt[3]*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (34*Sqrt[3]*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((34*I)*Sqrt[3]*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((34*I)*Sqrt[3]*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       ((34*I)*Sqrt[3]*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + (((-2*I)*5^(1/4)*Pi^2)/Sqrt[3] + 
       (3*5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       (3*5^(1/4)*(1 - (4*I)*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1])/2 + 
       (3*5^(1/4)*(1 + (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
       ((3*I)/2)*5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1] + 
       (3*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*(1 - (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*(-1 - (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
       (3*5^(1/4)*Log[2]*((-4*I)*Sqrt[3]*Log[4] + (1 + (4*I)*Sqrt[3])*
           Log[3 - I*Sqrt[3]] + (-1 + (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/2)/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*(((-I)*5^(1/4)*Pi)/Sqrt[3] - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       (-1/9*(5^(1/4)*(-2*I + 3*Sqrt[3])*Pi) + 
         (39*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) - 
         (39*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) - 
         (39*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) - 
         (39*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (15*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (3*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (15*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
            2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), -1, 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
            1])/(4*Pi^2) + (((3*I)/2)*5^(1/4)*GI[-1, -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 - (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
          Pi^2 - (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/4)*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 + (3*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (3*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (2*Pi^2) - (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), 1]*Log[2])/(4*Pi^2) - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi^2) + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((3*Sqrt[3]*5^(1/4))/Pi - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi - (((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi + (3*5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[-2/(1 + I*Sqrt[3]), -1, 1]*((((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          ((((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + (3*5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(4*Pi^2) - 
         (3*Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-3*I)*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             (13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*((-3*Sqrt[3]*5^(1/4))/Pi + 
           (3*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/
            (4*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi - (3*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 + (I/2)*Sqrt[3], -1]*(((-3*I)*Sqrt[3]*5^(1/4))/Pi - 
           (((3*I)/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((I*5^(1/4)*Pi)/Sqrt[3] + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/9 - 
         (39*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) - 
         (39*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (3*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (15*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
            2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
            1])/(4*Pi^2) - (((3*I)/2)*5^(1/4)*GI[-1, -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 + (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
          Pi^2 + (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/4)*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 - (3*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (3*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (2*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), 1]*Log[2])/(4*Pi^2) + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi^2) - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-3*Sqrt[3]*5^(1/4))/Pi - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi + (3*5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*((((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          ((((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) - (3*5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (3*Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (3*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*(((3*I)*Sqrt[3]*5^(1/4))/Pi - 
           (((3*I)/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((3*I)*Sqrt[3]*5^(1/4))/Pi + (((3*I)/2)*5^(1/4)*
             (Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 + (I/2)*Sqrt[3], -1]*((3*Sqrt[3]*5^(1/4))/Pi + 
           (3*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            (2*Pi^2)))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((I*5^(1/4)*Pi)/Sqrt[3] + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (3*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((3*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((3*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/9 - 
         (39*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(4*Pi^2) - 
         (39*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(4*Pi^2) + 
         (39*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) + (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (15*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (3*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (15*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(4*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (3*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/(2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
            1])/(2*Pi^2) + (3*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 
            -2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (3*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
            2/(1 + I*Sqrt[3]), 1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(2*Pi^2) + 
         (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (4*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(4*Pi^2) + (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
          (2*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
            1])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), -1, 1])/(4*Pi^2) - 
         (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
          (4*Pi^2) - (3*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
            1])/(4*Pi^2) - (((3*I)/2)*5^(1/4)*GI[-1, -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi^2 + (((3*I)/2)*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 + (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
          Pi^2 + (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 - (I/2)*Sqrt[3]])/Pi^2 - (((3*I)/4)*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi^2 - (3*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (3*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (4*Pi^2) + (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          (2*Pi^2) + (3*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
           Log[2])/(4*Pi^2) - (3*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 
            2/(-1 + I*Sqrt[3]), 1]*Log[2])/(4*Pi^2) + 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (((3*I)/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) - 
         (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(2*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (4*Pi^2) - (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi^2) - 
         (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 + 
         (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/Pi^2 - 
         (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(4*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-3*Sqrt[3]*5^(1/4))/Pi - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (((3*I)/4)*5^(1/4)*
             GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
          ((-3*Sqrt[3]*5^(1/4))/Pi + (3*5^(1/4)*Log[2])/(2*Pi^2)) + 
         G[2/(-1 + I*Sqrt[3]), -1, 1]*((((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
          ((3*Sqrt[3]*5^(1/4))/Pi + (((3*I)/4)*5^(1/4)*
             GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2])/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          ((((-3*I)/4)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (((3*I)/4)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (3*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi^2) - 
           (3*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi^2) - 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          ((((3*I)/4)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (((3*I)/4)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (3*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi^2) + 
           (3*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi^2) + 
           (3*5^(1/4)*Log[2]^2)/(4*Pi^2)) - (3*5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(4*Pi^2) + 
         (3*Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(4*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((3*Sqrt[3]*5^(1/4))/Pi - (3*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*(((3*I)*Sqrt[3]*5^(1/4))/Pi - 
           (((3*I)/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((3*I)*Sqrt[3]*5^(1/4))/Pi + (((3*I)/2)*5^(1/4)*
             (Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 + (I/2)*Sqrt[3], -1]*((3*Sqrt[3]*5^(1/4))/Pi + 
           (3*5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
            (2*Pi^2)))/EllipticK[(5 + Sqrt[5])/10]) + 
     EllipticK[(5 + Sqrt[5])/10]*((-34*(-2*I + 3*Sqrt[3])*Pi)/(27*5^(1/4)) + 
       (117*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (117*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (117*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) + 
       (117*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (117*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*5^(1/4)*Pi^2) + 
       (17*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (17*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (17*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (17*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (17*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) - 
       (17*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (17*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) - (17*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (5^(1/4)*Pi^2) - (17*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(2*5^(1/4)*Pi^2) + (17*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (17*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (17*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (17*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
       (34*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (34*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (17*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
       (17*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (17*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*5^(1/4)*Pi^2) - 
       (17*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi^2) - 
       (17*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) - (17*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
        (5^(1/4)*Pi^2) + (17*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^2) - (17*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^2) + (17*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/
        (5^(1/4)*Pi^2) + (17*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^2) - (17*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
        (5^(1/4)*Pi^2) + (17*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi^2) + 
       (17*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) - (17*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/(2*5^(1/4)*Pi^2) - (17*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/(2*5^(1/4)*Pi^2) + 
       (17*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi^2) - 
       (17*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) - (17*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          1])/(2*5^(1/4)*Pi^2) - (17*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
        (5^(1/4)*Pi^2) + (17*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/(2*5^(1/4)*Pi^2) + (17*G[-2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/(2*5^(1/4)*Pi^2) + 
       (17*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        (2*5^(1/4)*Pi^2) + (17*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/(2*5^(1/4)*Pi^2) + ((17*I)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - ((17*I)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + ((17*I)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((17*I)/2)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       ((17*I)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((17*I)/2)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + ((17*I)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) + (((17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
       ((17*I)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       ((17*I)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) - 
       (((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       ((17*I)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (17*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (17*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (17*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (17*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
       (17*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (17*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
       (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(2*5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
       (17*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (2*5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(5^(1/4)*Pi^2) - (17*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1]*Log[2])/(2*5^(1/4)*Pi^2) + 
       (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*Log[2])/
        (2*5^(1/4)*Pi^2) - ((17*I)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + ((17*I)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (((17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) + 
       (((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (17*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (17*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(2*5^(1/4)*Pi^2) + 
       (((17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (17*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*5^(1/4)*Pi^2) - 
       (17*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*5^(1/4)*Pi^2) + 
       G[0, 2/(-1 + I*Sqrt[3]), 1]*((34*Sqrt[3])/(5^(1/4)*Pi) - 
         (17*Log[2])/(5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((((-17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (17*GR[-1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
         (17*Log[2])/(5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-34*Sqrt[3])/(5^(1/4)*Pi) - (((17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
         (17*Log[2])/(5^(1/4)*Pi^2)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        ((34*Sqrt[3])/(5^(1/4)*Pi) + (17*Log[2])/(5^(1/4)*Pi^2)) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*((((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
         (17*Log[2])/(5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        ((-34*Sqrt[3])/(5^(1/4)*Pi) + (((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
         (17*Log[2])/(5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((-17*I)/2)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((17*I)/2)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (17*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) - 
         (17*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi^2) - 
         (17*Log[2]^2)/(2*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((17*I)/2)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((17*I)/2)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (17*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi^2) + 
         (17*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi^2) + 
         (17*Log[2]^2)/(2*5^(1/4)*Pi^2)) + 
       (Log[2]*(17*Log[(3 - I*Sqrt[3])/2]^2 + 117*Log[3 - I*Sqrt[3]] - 
          17*Log[(3 + I*Sqrt[3])/2]^2 - 117*Log[3 + I*Sqrt[3]]))/
        (2*5^(1/4)*Pi^2) - (17*Sqrt[3]*Log[2]*(Log[256] - 
          4*(Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])))/(2*5^(1/4)*Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*(((-34*I)*Sqrt[3])/(5^(1/4)*Pi) + 
         ((I/2)*(117 + 34*Log[3 + I*Sqrt[3]] - 34*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        ((-34*Sqrt[3])/(5^(1/4)*Pi) + (117 + 34*Log[3 + I*Sqrt[3]] - 
           34*Log[6 - (2*I)*Sqrt[3]])/(2*5^(1/4)*Pi^2)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*((-34*Sqrt[3])/(5^(1/4)*Pi) - 
         (17*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*(((-34*I)*Sqrt[3])/(5^(1/4)*Pi) - 
         ((17*I)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)))) + IEI[{2, 6, 1, 0}, TauA]*
    (EllipticK[(5 - Sqrt[5])/10]*((((68*I)/9)*Pi)/(Sqrt[3]*5^(1/4)) + 
       ((68*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((68*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((68*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((68*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (68*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (68*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((68*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((68*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((68*I)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)*Pi)) + ((((4*I)/3)*5^(1/4)*Pi^2)/Sqrt[3] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1] + 
       I*5^(1/4)*(I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(1 - (4*I)*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1] - 
       5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1] - 
       5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(-1 + (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*Log[2]*((4*I)*Sqrt[3]*Log[4] + (-1 - (4*I)*Sqrt[3])*
          Log[3 - I*Sqrt[3]] + (1 - (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*
             (13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/
            (2*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*
             (13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/
            (2*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((2*I)/3)*5^(1/4)*Pi)/Sqrt[3] + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 + (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) + (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) - (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(2*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*(((2*I)*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EllipticK[(5 + Sqrt[5])/10]*((68*(-2*I + 3*Sqrt[3])*Pi)/(81*5^(1/4)) - 
       (39*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (39*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (39*G[0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (39*G[0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (39*G[(I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) - 
       (34*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (17*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (34*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) + (17*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (34*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(3*Pi^2) + 
       (68*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (68*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(3*Pi^2) - 
       (17*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (34*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) - (34*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (34*G[0, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (34*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (34*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) - (17*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (17*G[-2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) - (34*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        (3*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/(3*5^(1/4)*Pi^2) - (17*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((34*I)/3)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((34*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((34*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((34*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((34*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (34*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (34*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (34*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (17*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (34*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) + (34*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (17*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/(3*5^(1/4)*Pi^2) - 
       (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*Log[2])/
        (3*5^(1/4)*Pi^2) + (((34*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (34*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(3*5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(3*5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (17*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       (17*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       G[0, -2/(1 + I*Sqrt[3]), 1]*(-68/(Sqrt[3]*5^(1/4)*Pi) - 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((((-17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (17*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (68/(Sqrt[3]*5^(1/4)*Pi) - (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        (-68/(Sqrt[3]*5^(1/4)*Pi) + (34*Log[2])/(3*5^(1/4)*Pi^2)) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*((((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (68/(Sqrt[3]*5^(1/4)*Pi) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((-17*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (17*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (17*GR[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
         (17*Log[2]^2)/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((17*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (17*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (17*GR[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
         (17*Log[2]^2)/(3*5^(1/4)*Pi^2)) - 
       (Log[2]*(17*Log[(3 - I*Sqrt[3])/2]^2 + 117*Log[3 - I*Sqrt[3]] - 
          17*Log[(3 + I*Sqrt[3])/2]^2 - 117*Log[3 + I*Sqrt[3]]))/
        (3*5^(1/4)*Pi^2) + (17*Log[2]*(Log[256] - 
          4*(Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])))/
        (Sqrt[3]*5^(1/4)*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        (68/(Sqrt[3]*5^(1/4)*Pi) - (117 + 34*Log[3 + I*Sqrt[3]] - 
           34*Log[6 - (2*I)*Sqrt[3]])/(3*5^(1/4)*Pi^2)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*((68*I)/(Sqrt[3]*5^(1/4)*Pi) - 
         ((I/3)*(117 + 34*Log[3 + I*Sqrt[3]] - 34*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
        ((68*I)/(Sqrt[3]*5^(1/4)*Pi) + (((34*I)/3)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*(68/(Sqrt[3]*5^(1/4)*Pi) + 
         (34*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2)))) + IEI[{2, 6, 4, 3}, TauA]*
    (EllipticK[(5 - Sqrt[5])/10]*((((68*I)/9)*Pi)/(Sqrt[3]*5^(1/4)) + 
       ((68*I)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((68*I)*G[-1, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((68*I)*G[0, 2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((68*I)*G[0, -2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (68*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       (68*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((68*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) + 
       ((68*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)*Pi) - 
       ((68*I)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4)*Pi)) + ((((4*I)/3)*5^(1/4)*Pi^2)/Sqrt[3] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 1] + 
       I*5^(1/4)*(I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(-1 - (4*I)*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 1] + 
       5^(1/4)*(1 - (4*I)*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 1] - 
       5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1] - 
       5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(-1 + (4*I)*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1] + 
       5^(1/4)*(1 + (4*I)*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1] + 
       5^(1/4)*Log[2]*((4*I)*Sqrt[3]*Log[4] + (-1 - (4*I)*Sqrt[3])*
          Log[3 - I*Sqrt[3]] + (1 - (4*I)*Sqrt[3])*Log[3 + I*Sqrt[3]]))/
      EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 2, 0, 1, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*
             (13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/
            (2*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 2, 1, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((-2*I)/3)*5^(1/4)*Pi)/Sqrt[3] - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((-2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 + 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 - (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) + 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 + (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) - (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) - 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(2*Pi^2) - 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi + ((I/2)*5^(1/4)*
             (13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + 
         GR[-1/2 - (I/2)*Sqrt[3], -1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 2*Log[6 - (2*I)*Sqrt[3]]))/
            (2*Pi^2)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((-2*Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((-2*I)*Sqrt[3]*5^(1/4))/Pi - (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EisensteinH[2, 1, 0, 0, TauA]*
      ((EllipticK[(5 - Sqrt[5])/10]*((((2*I)/3)*5^(1/4)*Pi)/Sqrt[3] + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
          (2*Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
          ((2*I)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
          ((2*I)*Sqrt[3]*5^(1/4)*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
             Log[3 + I*Sqrt[3]]))/Pi))/EllipticK[(5 + Sqrt[5])/10]^2 + 
       ((2*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi)/27 - 
         (13*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 1])/(2*Pi^2) - 
         (13*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 1])/(2*Pi^2) + 
         (13*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 + 
         (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
            1])/Pi^2 + (5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (2*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi^2 - 
         (2*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi^2 - 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi^2 + 
         (5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi^2 - 
         (5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/(2*Pi^2) + 
         (5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
          (2*Pi^2) + (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) + (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi^2 - 
         (5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
            1])/(2*Pi^2) - (5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
            -2/(-1 + I*Sqrt[3]), 1])/(2*Pi^2) - 
         (5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/(2*Pi^2) - 
         (I*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (I*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi^2 + (I*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (I*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi^2 - (5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
         (5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi^2 - 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (2*Pi^2) + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi^2 + (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi^2 + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
         (5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
          (2*Pi^2) - (5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
           Log[2])/(2*Pi^2) + (I*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
          Pi^2 - (I*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
           Log[2])/Pi^2 - ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], 
            -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi^2 - 
         (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi^2 + 
         (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
          (2*Pi^2) - (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
           Log[2])/(2*Pi^2) - ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]*
           Log[2]^2)/Pi^2 + ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
          Pi^2 - (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(2*Pi^2) + 
         G[0, -2/(1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi - 
           (5^(1/4)*Log[2])/Pi^2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
          (((-1/2*I)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[-1, -2/(1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi^2 - 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) - (5^(1/4)*Log[2])/
            Pi^2) + G[0, 2/(-1 + I*Sqrt[3]), 1]*((-2*Sqrt[3]*5^(1/4))/Pi + 
           (5^(1/4)*Log[2])/Pi^2) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
          (((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*((2*Sqrt[3]*5^(1/4))/Pi + 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi^2 + 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) + (5^(1/4)*Log[2])/
            Pi^2) + G[2/(-1 + I*Sqrt[3]), 1]*
          (((-1/2*I)*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi^2 - 
           ((I/2)*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi^2 - 
           (5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi^2) - 
           (5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi^2) - 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
          (((I/2)*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi^2 + 
           ((I/2)*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi^2 + 
           (5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi^2) + 
           (5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi^2) + 
           (5^(1/4)*Log[2]^2)/(2*Pi^2)) - (5^(1/4)*Log[2]*
           (Log[(3 - I*Sqrt[3])/2]^2 + 13*Log[3 - I*Sqrt[3]] - 
            Log[(3 + I*Sqrt[3])/2]^2 - 13*Log[3 + I*Sqrt[3]]))/(2*Pi^2) + 
         (Sqrt[3]*5^(1/4)*Log[2]*(Log[256] - 4*(Log[3 - I*Sqrt[3]] + 
              Log[3 + I*Sqrt[3]])))/(2*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi - (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/(2*Pi^2)) + 
         GI[-1/2 - (I/2)*Sqrt[3], -1]*(((2*I)*Sqrt[3]*5^(1/4))/Pi - 
           ((I/2)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
              2*Log[6 - (2*I)*Sqrt[3]]))/Pi^2) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
          (((2*I)*Sqrt[3]*5^(1/4))/Pi + (I*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
          ((2*Sqrt[3]*5^(1/4))/Pi + (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
              Log[6 + (2*I)*Sqrt[3]]))/Pi^2))/EllipticK[(5 + Sqrt[5])/10]) + 
     EllipticK[(5 + Sqrt[5])/10]*((68*(-2*I + 3*Sqrt[3])*Pi)/(81*5^(1/4)) - 
       (39*G[-1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (39*G[-1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (39*G[0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) - 
       (39*G[0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi^2) + 
       (39*G[(I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi^2) - 
       (34*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (17*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (34*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) + (17*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (34*G[-1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/(3*Pi^2) + 
       (68*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (68*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/(3*Pi^2) - 
       (17*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (34*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/
        (3*5^(1/4)*Pi^2) - (34*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (34*G[0, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) - 
       (34*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) + (34*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) - (34*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) - (17*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (17*G[-2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) + 
       (17*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) - (34*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        (3*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/(3*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/(3*5^(1/4)*Pi^2) + 
       (34*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        (3*5^(1/4)*Pi^2) - (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/(3*5^(1/4)*Pi^2) - (17*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/(3*5^(1/4)*Pi^2) - 
       (17*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
        (3*5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) + (((34*I)/3)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((34*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
       (((34*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
       (((34*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi^2) - (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
       (((34*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi^2) - (34*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) + (34*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (34*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
       (17*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi^2) - (17*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
       (34*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) + (34*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
       (17*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1])/(3*5^(1/4)*Pi^2) + (17*G[2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/(3*5^(1/4)*Pi^2) - 
       (17*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*Log[2])/
        (3*5^(1/4)*Pi^2) + (((34*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) - (((34*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (5^(1/4)*Pi^2) + (34*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) - (34*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
        (3*5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(3*5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(3*5^(1/4)*Pi^2) - 
       (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) + 
       (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi^2) - 
       (17*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       (17*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/(3*5^(1/4)*Pi^2) + 
       G[0, -2/(1 + I*Sqrt[3]), 1]*(-68/(Sqrt[3]*5^(1/4)*Pi) - 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((((-17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (17*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (68/(Sqrt[3]*5^(1/4)*Pi) - (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) - (17*GR[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        (-68/(Sqrt[3]*5^(1/4)*Pi) + (34*Log[2])/(3*5^(1/4)*Pi^2)) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*((((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (68/(Sqrt[3]*5^(1/4)*Pi) + (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (5^(1/4)*Pi^2) + (17*GR[-1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (34*Log[2])/(3*5^(1/4)*Pi^2)) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((((-17*I)/3)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) - 
         (((17*I)/3)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) - 
         (17*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) - 
         (17*GR[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) - 
         (17*Log[2]^2)/(3*5^(1/4)*Pi^2)) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((((17*I)/3)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi^2) + 
         (((17*I)/3)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi^2) + 
         (17*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi^2) + 
         (17*GR[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi^2) + 
         (17*Log[2]^2)/(3*5^(1/4)*Pi^2)) - 
       (Log[2]*(17*Log[(3 - I*Sqrt[3])/2]^2 + 117*Log[3 - I*Sqrt[3]] - 
          17*Log[(3 + I*Sqrt[3])/2]^2 - 117*Log[3 + I*Sqrt[3]]))/
        (3*5^(1/4)*Pi^2) + (17*Log[2]*(Log[256] - 
          4*(Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]])))/
        (Sqrt[3]*5^(1/4)*Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
        (68/(Sqrt[3]*5^(1/4)*Pi) - (117 + 34*Log[3 + I*Sqrt[3]] - 
           34*Log[6 - (2*I)*Sqrt[3]])/(3*5^(1/4)*Pi^2)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*((68*I)/(Sqrt[3]*5^(1/4)*Pi) - 
         ((I/3)*(117 + 34*Log[3 + I*Sqrt[3]] - 34*Log[6 - (2*I)*Sqrt[3]]))/
          (5^(1/4)*Pi^2)) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
        ((68*I)/(Sqrt[3]*5^(1/4)*Pi) + (((34*I)/3)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi^2)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*(68/(Sqrt[3]*5^(1/4)*Pi) + 
         (34*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/
          (3*5^(1/4)*Pi^2)))) + (1839 - 304*Zeta[3])/96 + 
   EllipticK[(5 + Sqrt[5])/10]*((13*(2 + (3*I)*Sqrt[3])*Pi^2)/(9*5^(1/4)) - 
     (17*Pi^3)/(9*Sqrt[3]*5^(1/4)) + 
     (((39*I)/2)*G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
        (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((39*I)/2)*G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((39*I)/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
        (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[-1, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/2)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[-1, (I/2)*(I + Sqrt[3]), 
        (-1/2*I)*(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     ((17*I)*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/Sqrt[3] - 
     ((17*I)*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/Sqrt[3] - 
     (((39*I)/4)*5^(3/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
     ((39*I)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     ((39*I)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((39*I)/4)*5^(3/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi + 
     ((17*I)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)) + 
     ((17*I)*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)) + 
     (((39*I)/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     ((34*I)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/(Sqrt[3]*5^(1/4)) + 
     ((17*I)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
      (Sqrt[3]*5^(1/4)) - ((34*I)*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/
      (Sqrt[3]*5^(1/4)) + ((17*I)*G[0, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(Sqrt[3]*5^(1/4)) - 
     (((39*I)/2)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/(5^(1/4)*Pi) - 
     (((39*I)/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((39*I)/2)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((39*I)/2)*G[0, (2*I)/(-I + Sqrt[3]), 
        (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[0, (2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/2)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((39*I)/2)*G[0, (-2*I)/(I + Sqrt[3]), 
        (2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[0, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/2)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[0, (2*I)/(I + Sqrt[3]), 
        (-2*I)/(-I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     ((17*I)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
      (Sqrt[3]*5^(1/4)) + ((17*I)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1, 1])/(Sqrt[3]*5^(1/4)) + 
     ((17*I)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
      (Sqrt[3]*5^(1/4)) + ((17*I)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1, 1])/(Sqrt[3]*5^(1/4)) - 
     (((39*I)/4)*G[(-2*I)/(-I + Sqrt[3]), 1, (2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((39*I)/4)*G[(-2*I)/(-I + Sqrt[3]), 
        (2*I)/(I + Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) - 
     (((39*I)/4)*G[(2*I)/(-I + Sqrt[3]), -1, (-2*I)/(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((39*I)/4)*G[(2*I)/(-I + Sqrt[3]), 
        (-2*I)/(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((39*I)/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, (I/2)*(I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
        (I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) + 
     (((39*I)/4)*G[(-2*I)/(I + Sqrt[3]), -1, (2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[(-2*I)/(I + Sqrt[3]), 
        (2*I)/(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((39*I)/4)*G[(2*I)/(I + Sqrt[3]), 1, (-2*I)/(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[(2*I)/(I + Sqrt[3]), 
        (-2*I)/(-I + Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((39*I)/2)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) + 
     (((39*I)/4)*G[(I/2)*(I + Sqrt[3]), -1, (-1/2*I)*(-I + Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((39*I)/4)*G[(I/2)*(I + Sqrt[3]), 
        (-1/2*I)*(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[(I/2)*(I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((17*I)/3)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[-1, -1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((17*I)/3)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((34*I)/3)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((34*I)/3)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((17*I)/3)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, 0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + ((17*I)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((34*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     ((17*I)*G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((34*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[-1, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*5^(3/4)*G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
     (((17*I)/3)*5^(3/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
     (((34*I)/3)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((34*I)/3)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[0, -1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[0, -1, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((34*I)/3)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((34*I)/3)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/2)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((68*I)/3)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((68*I)/3)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/2)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*5^(3/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1])/Pi - (((17*I)/2)*G[0, 0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) - (((34*I)/3)*G[0, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((34*I)/3)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((34*I)/3)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((34*I)/3)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) + (((34*I)/3)*G[0, 0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((34*I)/3)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((17*I)/2)*G[0, 0, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*5^(3/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/
      Pi - (((17*I)/6)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[0, 1, 2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) + 
     ((17*I)*G[0, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - ((17*I)*G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) + (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/(5^(1/4)*Pi) + 
     ((17*I)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - ((17*I)*G[0, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        1])/(5^(1/4)*Pi) + (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/3)*G[0, 2/(1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) + ((17*I)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/3)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*5^(3/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -2/(1 + I*Sqrt[3]), 
        1])/Pi + (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/2)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) + (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -1, -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     ((17*I)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*5^(3/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
        1])/Pi - (((17*I)/3)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/2)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -1, 2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), -1, 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) - (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
      (5^(1/4)*Pi) - (((17*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) - (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1, 1])/(5^(1/4)*Pi) + (17*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (17*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (17*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (17*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (3*5^(1/4)*Pi) + (17*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (17*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (6*5^(1/4)*Pi) + (17*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
     (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi) + 
     (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
      (2*5^(1/4)*Pi) - (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(3*5^(1/4)*Pi) - (17*GI[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (6*5^(1/4)*Pi) - (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi) - 
     (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
      (2*5^(1/4)*Pi) - (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) + (17*GI[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (3*5^(1/4)*Pi) + (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(3*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (3*5^(1/4)*Pi) + (17*5^(3/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*Pi) - 
     (34*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
      (3*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (6*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (3*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(3*5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (6*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(3*5^(1/4)*Pi) - 
     (17*5^(3/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (6*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 + (I/2)*Sqrt[3]])/
      (3*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (34*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (3*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (3*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
     ((17*I)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     ((17*I)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     ((17*I)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     ((17*I)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((17*I)/3)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi) - 
     (((17*I)/2)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) + (((17*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) + (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(5^(1/4)*Pi) + 
     (((17*I)/2)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) + (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((17*I)/6)*5^(3/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
        -1/2 + (I/2)*Sqrt[3]])/Pi + 
     (((34*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*5^(3/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
        -1/2 - (I/2)*Sqrt[3]])/Pi + 
     (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 + (I/2)*Sqrt[3]])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((34*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
     (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
     (((39*I)/2)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/(5^(1/4)*Pi) - 
     (((39*I)/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*Log[2])/
      (5^(1/4)*Pi) + (((39*I)/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
        (I/2)*(I + Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (((39*I)/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*Log[2])/
      (5^(1/4)*Pi) - (((39*I)/4)*G[(I/2)*(I + Sqrt[3]), 
        (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) - 
     (((17*I)/6)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1]*Log[2])/(5^(1/4)*Pi) + 
     (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
      (3*5^(1/4)*Pi) - (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(3*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) + 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - 
     (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(6*5^(1/4)*Pi) - (((17*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/
      (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) + 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(5^(1/4)*Pi) - 
     (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
      (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2]^2)/(6*5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/(5^(1/4)*Pi) - 
     (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/
      (5^(1/4)*Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
      (-17/(Sqrt[3]*5^(1/4)) - (17*Log[2])/(2*5^(1/4)*Pi)) + 
     GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      (-17/(Sqrt[3]*5^(1/4)) - (17*Log[2])/(6*5^(1/4)*Pi)) + 
     GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
      ((17*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
      ((-17*I)/(Sqrt[3]*5^(1/4)) + (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-17*I)/(Sqrt[3]*5^(1/4)) - (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((17*I)/(Sqrt[3]*5^(1/4)) - (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      ((17*I)/(Sqrt[3]*5^(1/4)) + (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
      ((-17*I)/(Sqrt[3]*5^(1/4)) + (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((17*I)/(Sqrt[3]*5^(1/4)) + (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
      ((-17*I)/(Sqrt[3]*5^(1/4)) - (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*Log[2])/(5^(1/4)*Pi)) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*((34*I)/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*
      ((34*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*(((34*I)*Sqrt[3])/5^(1/4) - 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-68*I)/(Sqrt[3]*5^(1/4)) + (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi) - (((17*I)/2)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
      ((17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((34*I)/(Sqrt[3]*5^(1/4)) + (17*GI[-1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) + (17*5^(3/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) - 
       (((17*I)/6)*5^(3/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) + (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) + (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) + (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, -1, -2/(1 + I*Sqrt[3]), 1]*((34*I)/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[0, -2/(1 + I*Sqrt[3]), -1, 1]*
      (((34*I)*Sqrt[3])/5^(1/4) + (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-17*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((34*I)/(Sqrt[3]*5^(1/4)) - (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*
      ((-68*I)/(Sqrt[3]*5^(1/4)) - (17*GI[-1/2 + (I/2)*Sqrt[3]])/
        (2*5^(1/4)*Pi) + (((17*I)/2)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) - (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi) - (17*5^(3/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(6*Pi) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*5^(3/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((17*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
      ((17*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/2)*Log[2])/(5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
      ((17*I)/(Sqrt[3]*5^(1/4)) + (((17*I)/2)*Log[2])/(5^(1/4)*Pi)) + 
     G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*((68*I)/(Sqrt[3]*5^(1/4)) - 
       (((34*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
      ((17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((34*I)/3)*Log[2])/(5^(1/4)*Pi)) + G[0, 0, -2/(1 + I*Sqrt[3]), 1]*
      ((68*I)/(Sqrt[3]*5^(1/4)) + (((34*I)/3)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
      ((-17*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((34*I)/3)*Log[2])/(5^(1/4)*Pi)) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) - ((17*I)*Log[2])/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*((34*I)/(Sqrt[3]*5^(1/4)) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       ((17*I)*Log[2])/(5^(1/4)*Pi)) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*
      ((34*I)/(Sqrt[3]*5^(1/4)) + (17*GI[-1/2 - (I/2)*Sqrt[3]])/
        (3*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       ((17*I)*Log[2])/(5^(1/4)*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
       -1/2 - (I/2)*Sqrt[3]]*(-17/(Sqrt[3]*5^(1/4)) + 
       (17*Log[2])/(6*5^(1/4)*Pi)) + GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*
      (-34/(Sqrt[3]*5^(1/4)) + (17*Log[2])/(3*5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
      (-17/(Sqrt[3]*5^(1/4)) + (17*Log[2])/(2*5^(1/4)*Pi)) + 
     GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*(34/(Sqrt[3]*5^(1/4)) + 
       (17*Log[2])/(5^(1/4)*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3]]*
      ((-34*Log[2])/(Sqrt[3]*5^(1/4)) - (17*Log[2]^2)/(2*5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((-17*Pi)/(36*5^(1/4)) + 
       (17*Log[2])/(Sqrt[3]*5^(1/4)) - (17*Log[2]^2)/(12*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
      ((((-17*I)/36)*Pi)/5^(1/4) - ((17*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/12)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((((-17*I)/36)*Pi)/5^(1/4) - (17*GI[-1, -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((17*I)*Log[2])/(Sqrt[3]*5^(1/4)) - (((17*I)/12)*Log[2]^2)/
        (5^(1/4)*Pi)) + GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
      ((((17*I)/36)*Pi)/5^(1/4) - ((17*I)*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/12)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((((17*I)/36)*Pi)/5^(1/4) + 
       (17*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       ((17*I)*Log[2])/(Sqrt[3]*5^(1/4)) + (((17*I)/12)*Log[2]^2)/
        (5^(1/4)*Pi)) + G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
      ((17*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
      ((17*(-I + 3*Sqrt[3])*Pi)/(9*5^(1/4)) + ((34*I)*Log[2])/
        (Sqrt[3]*5^(1/4)) - (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-1, -2/(1 + I*Sqrt[3]), 1]*((-17*(I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (17*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
       (17*5^(3/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*Pi) - 
       (34*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((17*I)*GR[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/3)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*5^(3/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((34*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((34*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
      ((-17*GI[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (17*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((17*I)*GR[-1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/2)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((68*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
      ((-17*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[0, -2/(1 + I*Sqrt[3]), 1]*
      ((17*(I + 3*Sqrt[3])*Pi)/(9*5^(1/4)) + ((34*I)*Log[2])/
        (Sqrt[3]*5^(1/4)) + (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-17*(-I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (17*5^(3/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(6*Pi) - 
       (17*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (34*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((17*I)*GR[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/6)*5^(3/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       (((17*I)/3)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((34*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((34*I)*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
      ((-17*GI[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       (17*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*5^(1/4)*Pi) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(3*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       ((17*I)*GR[-1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/2)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + ((68*I)*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/6)*Log[2]^2)/(5^(1/4)*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3]]*
      (((34*I)*Log[2])/(Sqrt[3]*5^(1/4)) + (((17*I)/2)*Log[2]^2)/
        (5^(1/4)*Pi)) + G[(-2*I)/(I + Sqrt[3]), 1]*
      ((39*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*5^(1/4)*Pi) + 
       (39*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*5^(1/4)*Pi) - 
       (((39*I)/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((39*I)/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((39*I)/4)*Log[2]^2)/(5^(1/4)*Pi)) + G[(I/2)*(I + Sqrt[3]), 1]*
      ((((-39*I)/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
       (((39*I)/4)*Log[2]^2)/(5^(1/4)*Pi)) + G[(2*I)/(-I + Sqrt[3]), 1]*
      ((((39*I)/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) + 
       (((39*I)/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/(5^(1/4)*Pi) + 
       (((39*I)/4)*Log[2]^2)/(5^(1/4)*Pi)) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((17*Pi)/(36*5^(1/4)) + 
       (17*Log[2])/(Sqrt[3]*5^(1/4)) + (17*Log[2]^2)/(12*5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3]]*(((-17*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/6)*Log[2]^3)/(5^(1/4)*Pi)) + G[-2/(1 + I*Sqrt[3]), 1]*
      ((17*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)) - 
       (17*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) - 
       (17*5^(3/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(6*Pi) + 
       (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (2*5^(1/4)*Pi) + (17*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (3*5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) + 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi) + (17*GI[-1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       ((17*I)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) - 
       ((17*I)*GR[-1/2 + (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/3)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*5^(3/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((17*I)/2)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((17*I)/3)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) - 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) - ((17*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/6)*Log[2]^3)/(5^(1/4)*Pi)) + G[2/(-1 + I*Sqrt[3]), 1]*
      ((17*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1])/(Sqrt[3]*5^(1/4)) + 
       (17*5^(3/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(6*Pi) + 
       (17*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(3*5^(1/4)*Pi) + 
       (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(2*5^(1/4)*Pi) - 
       (17*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) + (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi) - (17*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(3*5^(1/4)*Pi) - 
       (17*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (6*5^(1/4)*Pi) - (17*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(6*5^(1/4)*Pi) - ((17*I)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/
        (Sqrt[3]*5^(1/4)) - ((17*I)*GR[-1/2 - (I/2)*Sqrt[3], -1])/
        (Sqrt[3]*5^(1/4)) - (((17*I)/6)*5^(3/4)*GR[-1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((17*I)/3)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) - 
       (((17*I)/2)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) - (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) + (((17*I)/3)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
        (5^(1/4)*Pi) + (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((17*I)/6)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        (5^(1/4)*Pi) - ((17*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/6)*Log[2]^3)/(5^(1/4)*Pi)) + GI[-1/2 + (I/2)*Sqrt[3]]*
      ((17*Log[2]^2)/(Sqrt[3]*5^(1/4)) + (17*Log[2]^3)/(6*5^(1/4)*Pi)) + 
     G[0, (2*I)/(-I + Sqrt[3]), 1]*(((-39*I)*Sqrt[3])/5^(1/4) - 
       (((3*I)/4)*(-67 + 26*Log[2]))/(5^(1/4)*Pi)) + 
     G[-1, (2*I)/(-I + Sqrt[3]), 1]*(((39*I)*Sqrt[3])/5^(1/4) - 
       (((39*I)/4)*G[(I/2)*(I + Sqrt[3]), 1])/(5^(1/4)*Pi) - 
       (((3*I)/4)*(67 + 26*Log[2]))/(5^(1/4)*Pi)) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*(-34/(Sqrt[3]*5^(1/4)) - 
       (-117 + 34*Log[2])/(6*5^(1/4)*Pi)) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*
      ((34*I)/(Sqrt[3]*5^(1/4)) + ((I/6)*(-117 + 34*Log[2]))/(5^(1/4)*Pi)) + 
     GI[-1/2 - (I/2)*Sqrt[3]]*((17*Log[2]^2)/(Sqrt[3]*5^(1/4)) - 
       (Log[2]^2*(-117 + 34*Log[2]))/(12*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3]]*(((-17*I)*Log[2]^2)/(Sqrt[3]*5^(1/4)) + 
       ((I/12)*Log[2]^2*(-117 + 34*Log[2]))/(5^(1/4)*Pi)) + 
     GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(34/(Sqrt[3]*5^(1/4)) - 
       (-39 + 34*Log[2])/(2*5^(1/4)*Pi)) + GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) + ((I/2)*(-39 + 34*Log[2]))/(5^(1/4)*Pi)) + 
     GR[-1, -1/2 - (I/2)*Sqrt[3]]*(((34*I)*Log[2])/(Sqrt[3]*5^(1/4)) - 
       ((I/4)*(34*Log[2]^2 - 39*Log[4]))/(5^(1/4)*Pi)) + 
     GI[-1, -1/2 - (I/2)*Sqrt[3]]*((-34*Log[2])/(Sqrt[3]*5^(1/4)) + 
       (34*Log[2]^2 - 39*Log[4])/(4*5^(1/4)*Pi)) - 
     (((39*I)/4)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/(5^(1/4)*Pi) + 
     G[(-2*I)/(I + Sqrt[3]), -1, 1]*((-39*GI[-1/2 - (I/2)*Sqrt[3]])/
        (4*5^(1/4)*Pi) + (((39*I)/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((39*I)/4)*Log[4])/(5^(1/4)*Pi)) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
      (((-39*I)*Sqrt[3])/5^(1/4) + (((3*I)/4)*(-67 + 13*Log[4]))/
        (5^(1/4)*Pi)) + G[-1, (-2*I)/(I + Sqrt[3]), 1]*
      (((39*I)*Sqrt[3])/5^(1/4) - (39*GI[-1/2 - (I/2)*Sqrt[3]])/
        (4*5^(1/4)*Pi) + (((39*I)/4)*GR[-1/2 - (I/2)*Sqrt[3]])/(5^(1/4)*Pi) + 
       (((3*I)/4)*(67 + 13*Log[4]))/(5^(1/4)*Pi)) + 
     GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*(-68/(Sqrt[3]*5^(1/4)) - 
       (-117 + 34*Log[8] + 68*Log[3 - I*Sqrt[3]] - 68*Log[3 + I*Sqrt[3]])/
        (6*5^(1/4)*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*
      ((68*I)/(Sqrt[3]*5^(1/4)) + ((I/6)*(-117 + 34*Log[8] + 
          68*Log[3 - I*Sqrt[3]] - 68*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      ((-17*I)/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/6)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      (17/(Sqrt[3]*5^(1/4)) + (17*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
          2*Log[3 + I*Sqrt[3]]))/(6*5^(1/4)*Pi)) - 
     ((I/36)*Log[2]*(34*Log[(3 - I*Sqrt[3])/2]^3 + 
        (1809 + 136*Log[2]^2 - 351*Log[4])*Log[3 - I*Sqrt[3]] + 
        351*Log[3 - I*Sqrt[3]]^2 - 34*Log[(3 + I*Sqrt[3])/2]^3 - 
        1809*Log[3 + I*Sqrt[3]] - 136*Log[2]^2*Log[3 + I*Sqrt[3]] + 
        351*Log[4]*Log[3 + I*Sqrt[3]] - 351*Log[3 + I*Sqrt[3]]^2))/
      (5^(1/4)*Pi) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
      ((17*(-1 - (6*I)*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (17*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - Log[(3 + I*Sqrt[3])/2]^
           2))/(6*5^(1/4)*Pi) - (34*(Log[(3 - I*Sqrt[3])/16] + 
          Log[3 + I*Sqrt[3]]))/(Sqrt[3]*5^(1/4))) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1]*((-17*(-I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) + 
       (((17*I)/6)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
          Log[(3 + I*Sqrt[3])/2]^2))/(5^(1/4)*Pi) + 
       ((34*I)*(Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]]))/
        (Sqrt[3]*5^(1/4))) + GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
       -1]*(17/(Sqrt[3]*5^(1/4)) - (17*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
          2*Log[3 + I*Sqrt[3]]))/(6*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
      ((-17*I)/(Sqrt[3]*5^(1/4)) + 
       (((17*I)/6)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*
      ((68*I)/(Sqrt[3]*5^(1/4)) - 
       (((17*I)/3)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
      (-68/(Sqrt[3]*5^(1/4)) + (17*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
          2*Log[3 + I*Sqrt[3]]))/(3*5^(1/4)*Pi)) + 
     G[(I/2)*(I + Sqrt[3]), -1, 1]*(((39*I)*Sqrt[3])/5^(1/4) - 
       (((3*I)/4)*(-67 + 13*Log[4] - 26*Log[3 - I*Sqrt[3]] + 
          26*Log[3 + I*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1]*((-17*(I + 6*Sqrt[3])*Pi)/(18*5^(1/4)) - 
       (I*(-117 + 136*Log[2] - 34*Log[3 - I*Sqrt[3]] - 
          34*Log[3 + I*Sqrt[3]]))/(Sqrt[3]*5^(1/4)) - 
       ((I/12)*(603 + 102*Log[2]^2 - 117*Log[4] - 
          34*Log[(3 - I*Sqrt[3])/2]^2 - 234*Log[3 - I*Sqrt[3]] + 
          34*Log[(3 + I*Sqrt[3])/2]^2 + 234*Log[3 + I*Sqrt[3]]))/
        (5^(1/4)*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
      ((17*(1 - (6*I)*Sqrt[3])*Pi)/(18*5^(1/4)) + 
       (-117 + 2*Log[295147905179352825856] - 34*Log[3 - I*Sqrt[3]] - 
         34*Log[3 + I*Sqrt[3]])/(Sqrt[3]*5^(1/4)) + 
       (603 + 102*Log[2]^2 - 117*Log[4] - 34*Log[(3 - I*Sqrt[3])/2]^2 - 
         234*Log[3 - I*Sqrt[3]] + 34*Log[(3 + I*Sqrt[3])/2]^2 + 
         234*Log[3 + I*Sqrt[3]])/(12*5^(1/4)*Pi)) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      (34/(Sqrt[3]*5^(1/4)) - (117 + 34*Log[3 + I*Sqrt[3]] - 
         34*Log[6 - (2*I)*Sqrt[3]])/(6*5^(1/4)*Pi)) + 
     GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) + ((I/6)*(117 + 34*Log[3 + I*Sqrt[3]] - 
          34*Log[6 - (2*I)*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
      ((-34*I)/(Sqrt[3]*5^(1/4)) - (((17*I)/3)*(Log[3 - I*Sqrt[3]] - 
          Log[6 + (2*I)*Sqrt[3]]))/(5^(1/4)*Pi)) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
      (34/(Sqrt[3]*5^(1/4)) + (17*(Log[3 - I*Sqrt[3]] - 
          Log[6 + (2*I)*Sqrt[3]]))/(3*5^(1/4)*Pi)) - 
     (17*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
            Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
           (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
           Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
           Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
          Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
            Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
              (I + Sqrt[3])]))))/(36*5^(1/4)) - 
     (I*(136*Log[2]^2 + 34*Log[2]^3 + Log[2]*(117*Log[3 - I*Sqrt[3]] + 
          17*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
           (117 + 17*Log[3 + I*Sqrt[3]]) - Log[4]*
           (185 + 17*Log[3 - I*Sqrt[3]] + 17*Log[3 + I*Sqrt[3]])) + 
        136*Zeta[3]))/(Sqrt[3]*5^(1/4))) + EllipticK[(5 - Sqrt[5])/10]*
    ((-13*Pi^2)/(Sqrt[3]*5^(1/4)) + (17*(2 - (3*I)*Sqrt[3])*Pi^3)/
      (81*5^(1/4)) - (39*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/
      (4*5^(1/4)) - (39*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/
      (4*5^(1/4)) + (39*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/
      (4*5^(1/4)) + (39*(-I + 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/
      (4*5^(1/4)) - (39*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/
      (4*5^(1/4)) + (17*(-I + 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*(-I + 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*(-I + 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/
      (6*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(6*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) + (17*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/
      (6*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/(12*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) - (17*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*5^(3/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 
        1])/12 - (17*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/
      (3*5^(1/4)) - (17*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/
      (3*5^(1/4)) + (17*5^(3/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 
        1])/12 - (17*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) + (17*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/
      (6*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/(12*5^(1/4)) - 
     (17*(-I + 12*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/(6*5^(1/4)) + 
     (17*(-I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*(I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)) - 
     (17*(I + 12*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/(6*5^(1/4)) + 
     (17*(-I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
      (6*5^(1/4)) + (17*(I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
        -2/(1 + I*Sqrt[3]), 1])/(6*5^(1/4)) + 
     (17*(-I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/(6*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
      (12*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/(12*5^(1/4)) - 
     (17*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
      (12*5^(1/4)) - (17*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/
      (6*5^(1/4)) + (17*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/(12*5^(1/4)) + 
     (17*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
      (12*5^(1/4)) - (17*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/
      (6*5^(1/4)) + (17*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/(12*5^(1/4)) + 
     (17*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
      (12*5^(1/4)) - (17*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, 
        -2/(-1 + I*Sqrt[3]), 1])/(12*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/
      (12*5^(1/4)) + (17*(1 + (4*I)*Sqrt[3])*GI[-1, -1, 
        -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)) + 
     (((17*I)/6)*(I + 4*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/5^(1/4) + 
     (17*(1 - (8*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (17*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
     (((17*I)/6)*(-I + 8*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/5^(1/4) - 
     (((17*I)/12)*(-I + 4*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/5^(1/4) + 
     (17*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(6*5^(1/4)) + 
     (17*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (17*(-1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(6*5^(1/4)) + (((17*I)/12)*(I + 4*Sqrt[3])*
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/5^(1/4) - 
     (((17*I)/6)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/5^(1/4) - 
     (((17*I)/12)*(-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/5^(1/4) + 
     (17*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
        -1])/(12*5^(1/4)) + (17*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (17*(-I + 4*Sqrt[3])*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(6*5^(1/4)) + 
     (17*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(6*5^(1/4)) - 
     (17*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
      (12*5^(1/4)) - (17*(-I + 8*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/
      (6*5^(1/4)) - (17*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/(6*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
      (12*5^(1/4)) + (17*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (17*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
      (12*5^(1/4)) - (17*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/
      (6*5^(1/4)) - (17*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
     (17*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
      (12*5^(1/4)) + (17*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/(6*5^(1/4)) + 
     (17*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/
      (12*5^(1/4)) + (17*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1]*Log[2])/(12*5^(1/4)) - 
     (((17*I)/6)*(-I + 4*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/
      5^(1/4) + (17*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/
      (6*5^(1/4)) + (((17*I)/12)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/5^(1/4) + 
     (17*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
       Log[2])/(12*5^(1/4)) - (17*(-I + 4*Sqrt[3])*
       GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/(6*5^(1/4)) - 
     (17*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/(6*5^(1/4)) + 
     (17*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
       Log[2])/(12*5^(1/4)) + (17*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/(12*5^(1/4)) + 
     (17*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
      (12*5^(1/4)) + (((17*I)/12)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*
       Log[2]^2)/5^(1/4) + (17*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*
       Log[2]^2)/(12*5^(1/4)) + (17*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*
       Log[2]^2)/(12*5^(1/4)) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
      (((17*I)*Pi)/(Sqrt[3]*5^(1/4)) - (17*(-I + 4*Sqrt[3])*Log[2])/
        (6*5^(1/4))) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
      (((-17*I)*Pi)/(Sqrt[3]*5^(1/4)) - (((17*I)/12)*(-I + 4*Sqrt[3])*
         GI[-1/2 - (I/2)*Sqrt[3]])/5^(1/4) - 
       (17*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (17*(-I + 4*Sqrt[3])*Log[2])/(6*5^(1/4))) + 
     G[0, -2/(1 + I*Sqrt[3]), 1]*(((17*I)*Pi)/(Sqrt[3]*5^(1/4)) - 
       (17*(I + 4*Sqrt[3])*Log[2])/(6*5^(1/4))) + 
     G[-1, -2/(1 + I*Sqrt[3]), 1]*(((-17*I)*Pi)/(Sqrt[3]*5^(1/4)) + 
       (17*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (17*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (17*(I + 4*Sqrt[3])*Log[2])/(6*5^(1/4))) + 
     G[2/(-1 + I*Sqrt[3]), -1, 1]*((((-17*I)/12)*(-I + 4*Sqrt[3])*
         GI[-1/2 - (I/2)*Sqrt[3]])/5^(1/4) - 
       (17*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (17*(-I + 8*Sqrt[3])*Log[2])/(6*5^(1/4))) + 
     G[-2/(1 + I*Sqrt[3]), -1, 1]*
      ((17*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (17*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) - 
       (17*(I + 8*Sqrt[3])*Log[2])/(6*5^(1/4))) + G[2/(-1 + I*Sqrt[3]), 1]*
      ((17*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
       (17*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/(12*5^(1/4)) + 
       (17*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
       (17*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/(12*5^(1/4)) + 
       (17*(-I + 4*Sqrt[3])*Log[2]^2)/(12*5^(1/4))) + 
     G[-2/(1 + I*Sqrt[3]), 1]*((((17*I)/12)*(I + 4*Sqrt[3])*
         GI[-1, -1/2 + (I/2)*Sqrt[3]])/5^(1/4) + 
       (((17*I)/12)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/5^(1/4) + 
       (17*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/(12*5^(1/4)) + 
       (17*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/(12*5^(1/4)) + 
       (17*(I + 4*Sqrt[3])*Log[2]^2)/(12*5^(1/4))) + 
     ((17*I)*Pi*Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]))/
      (Sqrt[3]*5^(1/4)) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
      (((-17*I)*Pi)/(Sqrt[3]*5^(1/4)) + 
       (17*((-I)*Log[2] + 2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
          4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/(6*5^(1/4))) + 
     GI[-1/2 - (I/2)*Sqrt[3], -1]*((17*Pi)/(Sqrt[3]*5^(1/4)) + 
       (117 - (468*I)*Sqrt[3] + (544*I)*Sqrt[3]*Log[2] - Log[17179869184] - 
         34*Log[3 - I*Sqrt[3]] - (136*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] + 
         34*Log[3 + I*Sqrt[3]] - (136*I)*Sqrt[3]*Log[3 + I*Sqrt[3]])/
        (12*5^(1/4))) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
      (((-17*I)*Pi)/(Sqrt[3]*5^(1/4)) + (-117*I - 468*Sqrt[3] + 
         136*(-I + 2*Sqrt[3])*Log[2] - 136*Sqrt[3]*Log[(3 - I*Sqrt[3])/2] - 
         (136*I)*Log[3 - I*Sqrt[3]] - 136*Sqrt[3]*Log[(3 + I*Sqrt[3])/2] - 
         (34*I)*Log[3 + I*Sqrt[3]] + (170*I)*Log[6 - (2*I)*Sqrt[3]])/
        (12*5^(1/4))) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
      ((17*Pi)/(Sqrt[3]*5^(1/4)) + (17*((16*I)*Sqrt[3]*Log[2] + 
          (-1 - (4*I)*Sqrt[3])*Log[3 - I*Sqrt[3]] - (4*I)*Sqrt[3]*
           Log[3 + I*Sqrt[3]] + Log[6 + (2*I)*Sqrt[3]]))/(6*5^(1/4))) - 
     ((39*I)*Log[64]*(Log[3 - I*Sqrt[3]] - Log[3 + I*Sqrt[3]]) + 
       2*Log[2]*(468*Sqrt[3]*Log[4] + 272*Sqrt[3]*Log[(3 - I*Sqrt[3])/2] + 
         (85*I)*Log[(3 - I*Sqrt[3])/2]^2 - 68*Sqrt[3]*Log[(3 - I*Sqrt[3])/2]^
           2 - 468*Sqrt[3]*Log[3 - I*Sqrt[3]] + 272*Sqrt[3]*
          Log[(3 + I*Sqrt[3])/2] - (85*I)*Log[(3 + I*Sqrt[3])/2]^2 - 
         68*Sqrt[3]*Log[(3 + I*Sqrt[3])/2]^2 - 68*Log[(3 - I*Sqrt[3])/4]*
          (4*Sqrt[3] + I*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]]) - 
         740*Sqrt[3]*Log[3 + I*Sqrt[3]] - (68*I)*Log[3 - I*Sqrt[3]]*
          Log[3 + I*Sqrt[3]] + (68*I)*Log[3 + I*Sqrt[3]]^2) - 
       1088*Sqrt[3]*Zeta[3])/(24*5^(1/4))) + 
   ((-13*5^(1/4)*Pi^3)/(3*Sqrt[3]) + (5^(1/4)*(2 - (3*I)*Sqrt[3])*Pi^4)/27 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(-I + 8*Sqrt[3])*Pi*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/2 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-1, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/2 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
     (5^(1/4)*(I + 8*Sqrt[3])*Pi*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/2 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-1, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/2 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5*5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/4 - 
     5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 0, 2/(-1 + I*Sqrt[3]), 1] - 
     5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 0, -2/(1 + I*Sqrt[3]), 1] + 
     (5*5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/4 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/4 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/4 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/2 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, -2/(-1 + I*Sqrt[3]), 
        2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I - 12*Sqrt[3])*Pi*
       G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 2/(-1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/2 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/2 - 
     (5^(1/4)*(I + 12*Sqrt[3])*Pi*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, -2/(1 + I*Sqrt[3]), 
        2/(-1 + I*Sqrt[3]), 1])/2 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
        1])/4 - (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-2/(-1 + I*Sqrt[3]), 1, 
        2/(1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/4 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(-1 + I*Sqrt[3]), -1, 
        -2/(1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/4 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-2/(1 + I*Sqrt[3]), -1, 
        2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/4 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
        1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*G[2/(1 + I*Sqrt[3]), 
        -2/(-1 + I*Sqrt[3]), 1, 1])/4 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*
       GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/2 + (I/2)*5^(1/4)*(I + 4*Sqrt[3])*Pi*
      GI[-1, -1, -1/2 + (I/2)*Sqrt[3]] + (5^(1/4)*(1 - (8*I)*Sqrt[3])*Pi*
       GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(-1 - (8*I)*Sqrt[3])*Pi*
       GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/2 + 
     (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*Pi*
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/2 + 
     (I/4)*5^(1/4)*(I + 4*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], 
       -1/2 + (I/2)*Sqrt[3], -1] + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/2 + 
     (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
     (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/2 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/2 - 
     (5^(1/4)*(I + 8*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]])/4 - (5^(1/4)*(-I + 8*Sqrt[3])*Pi*
       GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/2 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1, -1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]])/4 - (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], -1, 
        -1/2 + (I/2)*Sqrt[3]])/4 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/2 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], -1, 
        -1/2 - (I/2)*Sqrt[3]])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3], -1])/2 + (5^(1/4)*(I + 4*Sqrt[3])*Pi*
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/4 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
        1]*Log[2])/4 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*
       GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/2 + 
     (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/2 + 
     (I/4)*5^(1/4)*(I + 4*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], 
       -1/2 + (I/2)*Sqrt[3]]*Log[2] + (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 - 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/2 - 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/2 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], 
        -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], 
        -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
     (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     (I/4)*5^(1/4)*(I + 4*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2 + 
     (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/4 + 
     G[2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*
         GI[-1/2 - (I/2)*Sqrt[3]])/4 - (5^(1/4)*(-I + 4*Sqrt[3])*Pi*
         GR[-1/2 - (I/2)*Sqrt[3]])/4 - (5^(1/4)*(-I + 8*Sqrt[3])*Pi*Log[2])/
        2) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
      ((5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 8*Sqrt[3])*Pi*Log[2])/2) + G[2/(-1 + I*Sqrt[3]), 1]*
      ((5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1, -1/2 - (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(1 + (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1, -1/2 - (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*Log[2]^2)/4) + G[-2/(1 + I*Sqrt[3]), 1]*
      ((I/4)*5^(1/4)*(I + 4*Sqrt[3])*Pi*GI[-1, -1/2 + (I/2)*Sqrt[3]] + 
       (I/4)*5^(1/4)*(I + 4*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3], -1] + 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1, -1/2 + (I/2)*Sqrt[3]])/4 + 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3], -1])/4 + 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*Log[2]^2)/4) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
      (I*Sqrt[3]*5^(1/4)*Pi^2 - (5^(1/4)*(-I + 4*Sqrt[3])*Pi*(-13 + Log[4]))/
        4) + G[0, -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4)*Pi^2 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*(-13 + Log[4]))/4) + 
     G[-1, 2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4)*Pi^2 + 
       (5^(1/4)*(-1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 - (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*GR[-1/2 - (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(-I + 4*Sqrt[3])*Pi*(13 + Log[4]))/4) + 
     G[-1, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4)*Pi^2 + 
       (5^(1/4)*(1 - (4*I)*Sqrt[3])*Pi*GI[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*GR[-1/2 + (I/2)*Sqrt[3]])/4 - 
       (5^(1/4)*(I + 4*Sqrt[3])*Pi*(13 + Log[4]))/4) + 
     I*Sqrt[3]*5^(1/4)*Pi^2*Log[2]*(Log[(3 - I*Sqrt[3])/4] + 
       Log[3 + I*Sqrt[3]]) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
      ((-I)*Sqrt[3]*5^(1/4)*Pi^2 + (5^(1/4)*Pi*(13*I - 52*Sqrt[3] - 
          (2*I)*Log[2] + 2*Sqrt[3]*Log[65536] + (2*I)*Log[3 - I*Sqrt[3]] - 
          8*Sqrt[3]*Log[3 - I*Sqrt[3]] - (2*I)*Log[3 + I*Sqrt[3]] - 
          8*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GR[-1/2 - (I/2)*Sqrt[3], -1]*
      ((-I)*Sqrt[3]*5^(1/4)*Pi^2 + (5^(1/4)*Pi*(-13*I - 52*Sqrt[3] + 
          (2*I)*Log[2] + 2*Sqrt[3]*Log[65536] + (2*I)*Log[3 - I*Sqrt[3]] - 
          8*Sqrt[3]*Log[3 - I*Sqrt[3]] - (2*I)*Log[3 + I*Sqrt[3]] - 
          8*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
      (Sqrt[3]*5^(1/4)*Pi^2 + (5^(1/4)*Pi*(13 - (52*I)*Sqrt[3] + 
          (32*I)*Sqrt[3]*Log[2] - Log[4] - 2*Log[3 - I*Sqrt[3]] - 
          (8*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]] - 
          (8*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
     GI[-1/2 + (I/2)*Sqrt[3], -1]*(Sqrt[3]*5^(1/4)*Pi^2 + 
       (5^(1/4)*Pi*(-13 - (52*I)*Sqrt[3] + (32*I)*Sqrt[3]*Log[2] + Log[4] - 
          2*Log[3 - I*Sqrt[3]] - (8*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] + 
          2*Log[3 + I*Sqrt[3]] - (8*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
     (5^(1/4)*Pi*(8*Sqrt[3]*Log[2]^3 - 
        Log[2]*(Sqrt[3]*Log[87112285931760246646623899502532662132736] + 
          (13*I - 52*Sqrt[3] - I*Log[4] + Sqrt[3]*Log[256])*
           Log[3 - I*Sqrt[3]] + I*Log[3 - I*Sqrt[3]]^2 + 
          (-13*I - 52*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - 
            I*Log[3 + I*Sqrt[3]])*Log[3 + I*Sqrt[3]]) + 
        Sqrt[3]*(Log[4]*Log[65536] + Log[16]*(Log[3 - I*Sqrt[3]]^2 + 
            Log[3 + I*Sqrt[3]]^2) + 32*Zeta[3])))/4)/
    EllipticK[(5 + Sqrt[5])/10] + EisensteinH[2, 2, 0, 1, TauA]*
    ((((-13*I)/54)*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi^2 + 
       (5^(1/4)*Pi^3)/(6*Sqrt[3]) - (((13*I)/4)*5^(1/4)*
         G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1])/Pi + (((13*I)/4)*5^(1/4)*
         G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1] + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1] + 
       (((65*I)/8)*5^(1/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/2)*5^(1/4)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/2)*5^(1/4)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((65*I)/8)*5^(1/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1] - 
       (((13*I)/8)*5^(1/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/Pi + 
       I*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
         1] + I*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
         1] + (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 
          1])/Pi + (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/
        Pi - (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/
        Pi - (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 
          1])/Pi - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 
         2/(1 + I*Sqrt[3]), 1] - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
         2/(1 + I*Sqrt[3]), 1, 1] - (I/2)*Sqrt[3]*5^(1/4)*
        G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
         1] + (((13*I)/8)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), 1, 
          (2*I)/(I + Sqrt[3]), 1])/Pi + (((13*I)/8)*5^(1/4)*
         G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, (-2*I)/(I + Sqrt[3]), 
          1])/Pi + (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
          (I/2)*(I + Sqrt[3]), 1])/Pi - (((13*I)/8)*5^(1/4)*
         G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 1, (-2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 
          (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 
          (-1/2*I)*(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), 
          -1, 1])/Pi + (((13*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + (I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((5*I)/2)*5^(1/4)*
         G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + (I*5^(1/4)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((2*I)*5^(1/4)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((2*I)*5^(1/4)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          -1, 1])/Pi - (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 
          -1, -1, 1])/Pi - (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/4)*5^(1/4)*
         G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/4)*5^(1/4)*
         G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (2*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) + 
       (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (((3*I)/2)*5^(1/4)*
         GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        Pi + (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/Pi - 
       (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*
         Log[2])/Pi - (((13*I)/8)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1]*Log[2])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*
         Log[2])/Pi + (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (2*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/Pi - ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/(4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/Pi + GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)) - (3*5^(1/4)*Log[2])/(2*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 - (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) - 
         (5^(1/4)*Log[2])/(2*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 - (5^(1/4)*Log[2])/
          (4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[0, -1, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, -2/(1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*((2*I)*Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((2*I)*Sqrt[3]*5^(1/4) - (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), -1, 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
           Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       G[0, 0, -2/(1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         (I*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, 
         2/(-1 + I*Sqrt[3]), 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*Log[2])/
          Pi) + G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) + 
         (I*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), -1, 
         -2/(1 + I*Sqrt[3]), 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (I*Sqrt[3]*5^(1/4) + (((3*I)/2)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 + (5^(1/4)*Log[2])/
          (4*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 + (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*(-1/24*(5^(1/4)*Pi) - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 - (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((-1/24*I)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((-1/24*I)*5^(1/4)*Pi - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((I/24)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((I/24)*5^(1/4)*Pi + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        (-1/6*(5^(1/4)*(I + 3*Sqrt[3])*Pi) - I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        (-1/4*(5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        (-1/6*(5^(1/4)*(-I + 3*Sqrt[3])*Pi) - I*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 + (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4)*Log[2] - 
         (((3*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(I/2)*(I + Sqrt[3]), 1]*
        ((((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + G[(2*I)/(-I + Sqrt[3]), 1]*
        ((((-13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
         (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + G[(-2*I)/(I + Sqrt[3]), 1]*
        ((-13*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(8*Pi) - 
         (13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(8*Pi) + 
         (((13*I)/8)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((5^(1/4)*Pi)/24 - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 + (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GI[-1, -1/2 + (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4)*Log[2] + 
         (3*5^(1/4)*Log[2]^2)/(4*Pi)) + GI[-1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) - (5^(1/4)*Log[2]^3)/(4*Pi)) + 
       G[2/(-1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 - (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1])/2 - 
         (5*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 - (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 - (I/2)*Sqrt[3], -1] + (((5*I)/4)*5^(1/4)*
           GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + GR[-1/2 + (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
         (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) + (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 + (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 + (I/2)*Sqrt[3], -1] - 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi - (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/
          Pi) + G[0, (2*I)/(-I + Sqrt[3]), 1]*(((13*I)/2)*Sqrt[3]*5^(1/4) + 
         ((I/8)*5^(1/4)*(-115 + 26*Log[2]))/Pi) + 
       G[-1, (2*I)/(-I + Sqrt[3]), 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) + 
         (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 1])/Pi + 
         ((I/8)*5^(1/4)*(115 + 26*Log[2]))/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3]]*
        (Sqrt[3]*5^(1/4)*Log[2] - (5^(1/4)*(6*Log[2]^2 - 13*Log[4]))/
          (8*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3]]*
        ((-I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*(6*Log[2]^2 - 
            13*Log[4]))/Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*
        ((-I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-13 + Log[4]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(-13 + Log[4]))/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/8)*5^(1/4)*Log[2]^2*
           (-13 + Log[4]))/Pi) + GI[-1/2 - (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) + (5^(1/4)*Log[2]^2*(-13 + Log[4]))/
          (8*Pi)) + (((13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/
        Pi + G[(-2*I)/(I + Sqrt[3]), -1, 1]*
        ((13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(8*Pi) - 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[4])/Pi) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
        (((13*I)/2)*Sqrt[3]*5^(1/4) - ((I/8)*5^(1/4)*(-115 + 13*Log[4]))/
          Pi) + G[-1, (-2*I)/(I + Sqrt[3]), 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) + 
         (13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(8*Pi) - 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/8)*5^(1/4)*(115 + 13*Log[4]))/Pi) + 
       GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4) - 
         ((I/4)*5^(1/4)*(-13 + Log[64]))/Pi) + 
       GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(-(Sqrt[3]*5^(1/4)) + 
         (5^(1/4)*(-13 + Log[64]))/(4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        ((-2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-13 + Log[64] + 
            4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*(2*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(-13 + Log[64] + 4*Log[3 - I*Sqrt[3]] - 
            4*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (5^(1/4)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(Log[2] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       ((I/24)*5^(1/4)*Log[2]*(2*Log[(3 - I*Sqrt[3])/2]^3 + 
          (345 + 8*Log[2]^2 - 39*Log[4])*Log[3 - I*Sqrt[3]] + 
          39*Log[3 - I*Sqrt[3]]^2 - 2*Log[(3 + I*Sqrt[3])/2]^3 - 
          345*Log[3 + I*Sqrt[3]] - 8*Log[2]^2*Log[3 + I*Sqrt[3]] + 
          39*Log[4]*Log[3 + I*Sqrt[3]] - 39*Log[3 + I*Sqrt[3]]^2))/Pi + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 - 
         ((I/4)*5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/Pi - I*Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(1 + (6*I)*Sqrt[3])*Pi)/12 + 
         (5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/(4*Pi) + Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(Log[2] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) + (5^(1/4)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        (2*Sqrt[3]*5^(1/4) - (5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        ((-2*I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[8] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       G[(I/2)*(I + Sqrt[3]), -1, 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) + 
         ((I/8)*5^(1/4)*(-115 + 13*Log[4] - 26*Log[3 - I*Sqrt[3]] + 
            26*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((5^(1/4)*(-1 + (6*I)*Sqrt[3])*Pi)/12 - 
         (Sqrt[3]*5^(1/4)*(-13 + Log[256] - 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/2 - (5^(1/4)*(115 + 6*Log[2]^2 - 
            13*Log[4] - 2*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
            2*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/(8*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 + 
         (I/2)*Sqrt[3]*5^(1/4)*(-13 + Log[256] - 2*Log[3 - I*Sqrt[3]] - 
           2*Log[3 + I*Sqrt[3]]) + ((I/8)*5^(1/4)*(115 + 6*Log[2]^2 - 
            13*Log[4] - 2*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
            2*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (I*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
            2*Log[6 - (2*I)*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
            2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (-(Sqrt[3]*5^(1/4)) - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(2*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/Pi) + 
       (5^(1/4)*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
              Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
             (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
             Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
             Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
            Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
              Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
                (I + Sqrt[3])]))))/24 + (I/2)*Sqrt[3]*5^(1/4)*
        (8*Log[2]^2 + 2*Log[2]^3 + Log[2]*(13*Log[3 - I*Sqrt[3]] + 
           Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
            (13 + Log[3 + I*Sqrt[3]]) - Log[4]*(17 + Log[3 - I*Sqrt[3]] + 
             Log[3 + I*Sqrt[3]])) + 8*Zeta[3]))/EllipticK[(5 + Sqrt[5])/10] + 
     (EllipticK[(5 - Sqrt[5])/10]*((13*5^(1/4)*Pi^2)/(6*Sqrt[3]) + 
        (I/54)*5^(1/4)*(2*I + 3*Sqrt[3])*Pi^3 + 
        (13*5^(1/4)*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/8 + 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/8 - 
        (13*5^(1/4)*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/8 - 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/8 + 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/2 - 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 12*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 12*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
           -2/(1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
           2/(-1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
           -2/(-1 + I*Sqrt[3]), 1, 1])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (I/4)*5^(1/4)*(I + 8*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1] + 
        (I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]] + (5^(1/4)*(1 + (8*I)*Sqrt[3])*
          GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (I/4)*5^(1/4)*(I + 4*Sqrt[3])*
         GI[-1/2 - (I/2)*Sqrt[3], -1, -1] + (I/8)*5^(1/4)*(I + 4*Sqrt[3])*
         GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]] + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 - (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*
         GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1] + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 8*Sqrt[3])*
          GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1]*Log[2])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + (I/4)*5^(1/4)*
         (I + 4*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2] + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 - (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*
         GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2] + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 - 
        (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        G[0, 2/(-1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/4) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
            GI[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/
           4) + G[0, -2/(1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/4) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (I/8)*5^(1/4)*(I + 4*Sqrt[3])*
           GI[-1/2 + (I/2)*Sqrt[3]] + (5^(1/4)*(I + 4*Sqrt[3])*
            GR[-1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/
           4) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 8*Sqrt[3])*Log[2])/4) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
         ((I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]] + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 8*Sqrt[3])*Log[2])/4) + G[2/(-1 + I*Sqrt[3]), 1]*
         ((-1/8*I)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]] - 
          (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1] - 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2]^2)/8) + G[-2/(1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2]^2)/8) - (I/2)*Sqrt[3]*5^(1/4)*Pi*
         Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]) + 
        GR[-1/2 - (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi - 
          (5^(1/4)*(-13*I - 52*Sqrt[3] + (2*I)*Log[2] + 2*Sqrt[3]*
              Log[65536] + (2*I)*Log[3 - I*Sqrt[3]] - 8*Sqrt[3]*
              Log[3 - I*Sqrt[3]] - (2*I)*Log[3 + I*Sqrt[3]] - 
             8*Sqrt[3]*Log[3 + I*Sqrt[3]]))/8) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
         (-1/2*(Sqrt[3]*5^(1/4)*Pi) + (5^(1/4)*((-16*I)*Sqrt[3]*Log[2] + 
             Log[(3 - I*Sqrt[3])/2] + (4*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]] + (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GI[-1/2 - (I/2)*Sqrt[3], -1]*(-1/2*(Sqrt[3]*5^(1/4)*Pi) + 
          (5^(1/4)*(-13 + (52*I)*Sqrt[3] - (32*I)*Sqrt[3]*Log[2] + Log[4] + 
             2*Log[3 - I*Sqrt[3]] + (8*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - 
             2*Log[3 + I*Sqrt[3]] + (8*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/8) + 
        GR[-1/2 + (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I*Log[2] - 2*Sqrt[3]*Log[256] - I*Log[3 - I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] + I*Log[3 + I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) - 
        (5^(1/4)*(48*Sqrt[3]*Log[2]^3 - (13*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]]) - 6*Log[2]*
            (Sqrt[3]*Log[20282409603651670423947251286016] + 
             ((-I)*Log[4] + Sqrt[3]*(-52 + Log[256]))*Log[3 - I*Sqrt[3]] + 
             (I - 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
              (-52*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - I*
                Log[3 + I*Sqrt[3]] - 4*Sqrt[3]*Log[3 + I*Sqrt[3]])) + 
           192*Sqrt[3]*Zeta[3]))/48))/EllipticK[(5 + Sqrt[5])/10]^2) + 
   EisensteinH[2, 2, 1, 0, TauA]*
    ((((-13*I)/54)*5^(1/4)*(-2*I + 3*Sqrt[3])*Pi^2 + 
       (5^(1/4)*Pi^3)/(6*Sqrt[3]) - (((13*I)/4)*5^(1/4)*
         G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1])/Pi + (((13*I)/4)*5^(1/4)*
         G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1] + 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1] + 
       (((65*I)/8)*5^(1/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/2)*5^(1/4)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/2)*5^(1/4)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((65*I)/8)*5^(1/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1] - 
       (((13*I)/8)*5^(1/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/Pi + 
       I*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
         1] + I*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
         1] + (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 
          1])/Pi + (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/
        Pi - (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/
        Pi - (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 
          1])/Pi - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 
         2/(1 + I*Sqrt[3]), 1] - (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
         2/(1 + I*Sqrt[3]), 1, 1] - (I/2)*Sqrt[3]*5^(1/4)*
        G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1] - 
       (I/2)*Sqrt[3]*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
         1] + (((13*I)/8)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), 1, 
          (2*I)/(I + Sqrt[3]), 1])/Pi + (((13*I)/8)*5^(1/4)*
         G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, (-2*I)/(I + Sqrt[3]), 
          1])/Pi + (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
          (I/2)*(I + Sqrt[3]), 1])/Pi - (((13*I)/8)*5^(1/4)*
         G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, (2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 1, (-2*I)/(-I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 
          (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 
          (-1/2*I)*(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), 
          -1, 1])/Pi + (((13*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + (I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((5*I)/2)*5^(1/4)*
         G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (((5*I)/2)*5^(1/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + (I*5^(1/4)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((2*I)*5^(1/4)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((2*I)*5^(1/4)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          -1, 1])/Pi - (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 
          -1, -1, 1])/Pi - (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + (((3*I)/2)*5^(1/4)*
         G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + (((5*I)/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/4)*5^(1/4)*
         G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi - 
       (((5*I)/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/4)*5^(1/4)*
         G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (2*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) - (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) + 
       (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (((3*I)/2)*5^(1/4)*
         GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi + ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        Pi + (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/Pi - 
       (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*
         Log[2])/Pi - (((13*I)/8)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1]*Log[2])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*
         Log[2])/Pi + (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (2*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/Pi - ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/(4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/Pi + GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)) - (3*5^(1/4)*Log[2])/(2*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 - (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) - 
         (5^(1/4)*Log[2])/(2*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 - (5^(1/4)*Log[2])/
          (4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[0, -1, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, -2/(1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*((2*I)*Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*((-3*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((2*I)*Sqrt[3]*5^(1/4) - (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), -1, 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
           Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (((3*I)/4)*5^(1/4)*Log[2])/Pi) + 
       G[0, 0, -2/(1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         (I*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, 
         2/(-1 + I*Sqrt[3]), 1]*((5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*Log[2])/
          Pi) + G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*((-2*I)*Sqrt[3]*5^(1/4) + 
         (I*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), -1, 
         -2/(1 + I*Sqrt[3]), 1]*(-1/4*(5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*
        (I*Sqrt[3]*5^(1/4) + (((3*I)/2)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*((-I)*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4))/2 + (5^(1/4)*Log[2])/
          (4*Pi)) + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4))/2 + (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*(-1/24*(5^(1/4)*Pi) - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 - (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((-1/24*I)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((-1/24*I)*5^(1/4)*Pi - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((I/24)*5^(1/4)*Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((I/24)*5^(1/4)*Pi + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        (-1/6*(5^(1/4)*(I + 3*Sqrt[3])*Pi) - I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 + 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        (-1/4*(5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        (-1/6*(5^(1/4)*(-I + 3*Sqrt[3])*Pi) - I*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 + (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         I*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/2 - 
         (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4)*Log[2] - 
         (((3*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(I/2)*(I + Sqrt[3]), 1]*
        ((((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + G[(2*I)/(-I + Sqrt[3]), 1]*
        ((((-13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/Pi - 
         (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + G[(-2*I)/(I + Sqrt[3]), 1]*
        ((-13*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(8*Pi) - 
         (13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(8*Pi) + 
         (((13*I)/8)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((5^(1/4)*Pi)/24 - 
         (Sqrt[3]*5^(1/4)*Log[2])/2 + (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GI[-1, -1/2 + (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4)*Log[2] + 
         (3*5^(1/4)*Log[2]^2)/(4*Pi)) + GI[-1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) - (5^(1/4)*Log[2]^3)/(4*Pi)) + 
       G[2/(-1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 - (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 - (I/2)*Sqrt[3], -1])/2 - 
         (5*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 - (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 - (I/2)*Sqrt[3], -1] + (((5*I)/4)*5^(1/4)*
           GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi - ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + GR[-1/2 + (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 1]*(-1/2*(Sqrt[3]*5^(1/4)*
           GI[-1, -1/2 + (I/2)*Sqrt[3]]) - (Sqrt[3]*5^(1/4)*
           GI[-1/2 + (I/2)*Sqrt[3], -1])/2 + 
         (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) + (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 + (I/2)*Sqrt[3]] + (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 + (I/2)*Sqrt[3], -1] - 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi - (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi + (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/4)*5^(1/4)*Log[2]^3)/
          Pi) + G[0, (2*I)/(-I + Sqrt[3]), 1]*(((13*I)/2)*Sqrt[3]*5^(1/4) + 
         ((I/8)*5^(1/4)*(-115 + 26*Log[2]))/Pi) + 
       G[-1, (2*I)/(-I + Sqrt[3]), 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) + 
         (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 1])/Pi + 
         ((I/8)*5^(1/4)*(115 + 26*Log[2]))/Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3]]*
        (Sqrt[3]*5^(1/4)*Log[2] - (5^(1/4)*(6*Log[2]^2 - 13*Log[4]))/
          (8*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3]]*
        ((-I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*(6*Log[2]^2 - 
            13*Log[4]))/Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*
        ((-I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-13 + Log[4]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*(Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(-13 + Log[4]))/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/8)*5^(1/4)*Log[2]^2*
           (-13 + Log[4]))/Pi) + GI[-1/2 - (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)*Log[2]^2) + (5^(1/4)*Log[2]^2*(-13 + Log[4]))/
          (8*Pi)) + (((13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/
        Pi + G[(-2*I)/(I + Sqrt[3]), -1, 1]*
        ((13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(8*Pi) - 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[4])/Pi) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
        (((13*I)/2)*Sqrt[3]*5^(1/4) - ((I/8)*5^(1/4)*(-115 + 13*Log[4]))/
          Pi) + G[-1, (-2*I)/(I + Sqrt[3]), 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) + 
         (13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(8*Pi) - 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/8)*5^(1/4)*(115 + 13*Log[4]))/Pi) + 
       GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4) - 
         ((I/4)*5^(1/4)*(-13 + Log[64]))/Pi) + 
       GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(-(Sqrt[3]*5^(1/4)) + 
         (5^(1/4)*(-13 + Log[64]))/(4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        ((-2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(-13 + Log[64] + 
            4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*(2*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(-13 + Log[64] + 4*Log[3 - I*Sqrt[3]] - 
            4*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (5^(1/4)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(Log[2] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       ((I/24)*5^(1/4)*Log[2]*(2*Log[(3 - I*Sqrt[3])/2]^3 + 
          (345 + 8*Log[2]^2 - 39*Log[4])*Log[3 - I*Sqrt[3]] + 
          39*Log[3 - I*Sqrt[3]]^2 - 2*Log[(3 + I*Sqrt[3])/2]^3 - 
          345*Log[3 + I*Sqrt[3]] - 8*Log[2]^2*Log[3 + I*Sqrt[3]] + 
          39*Log[4]*Log[3 + I*Sqrt[3]] - 39*Log[3 + I*Sqrt[3]]^2))/Pi + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(-I + 6*Sqrt[3])*Pi)/12 - 
         ((I/4)*5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/Pi - I*Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(1 + (6*I)*Sqrt[3])*Pi)/12 + 
         (5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/(4*Pi) + Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((I/2)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(Log[2] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (-1/2*(Sqrt[3]*5^(1/4)) + (5^(1/4)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        (2*Sqrt[3]*5^(1/4) - (5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        ((-2*I)*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[8] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       G[(I/2)*(I + Sqrt[3]), -1, 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) + 
         ((I/8)*5^(1/4)*(-115 + 13*Log[4] - 26*Log[3 - I*Sqrt[3]] + 
            26*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
        ((5^(1/4)*(-1 + (6*I)*Sqrt[3])*Pi)/12 - 
         (Sqrt[3]*5^(1/4)*(-13 + Log[256] - 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/2 - (5^(1/4)*(115 + 6*Log[2]^2 - 
            13*Log[4] - 2*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
            2*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/(8*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*((5^(1/4)*(I + 6*Sqrt[3])*Pi)/12 + 
         (I/2)*Sqrt[3]*5^(1/4)*(-13 + Log[256] - 2*Log[3 - I*Sqrt[3]] - 
           2*Log[3 + I*Sqrt[3]]) + ((I/8)*5^(1/4)*(115 + 6*Log[2]^2 - 
            13*Log[4] - 2*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
            2*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (I*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
            2*Log[6 - (2*I)*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
            2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (-(Sqrt[3]*5^(1/4)) - (5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/(2*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/Pi) + 
       (5^(1/4)*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
              Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
             (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
             Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
             Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
            Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
              Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
                (I + Sqrt[3])]))))/24 + (I/2)*Sqrt[3]*5^(1/4)*
        (8*Log[2]^2 + 2*Log[2]^3 + Log[2]*(13*Log[3 - I*Sqrt[3]] + 
           Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
            (13 + Log[3 + I*Sqrt[3]]) - Log[4]*(17 + Log[3 - I*Sqrt[3]] + 
             Log[3 + I*Sqrt[3]])) + 8*Zeta[3]))/EllipticK[(5 + Sqrt[5])/10] + 
     (EllipticK[(5 - Sqrt[5])/10]*((13*5^(1/4)*Pi^2)/(6*Sqrt[3]) + 
        (I/54)*5^(1/4)*(2*I + 3*Sqrt[3])*Pi^3 + 
        (13*5^(1/4)*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/8 + 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/8 - 
        (13*5^(1/4)*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/8 - 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/8 + 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/2 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/2 - 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 12*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 12*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
           -2/(1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
           2/(-1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
           1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
           -2/(-1 + I*Sqrt[3]), 1, 1])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (I/4)*5^(1/4)*(I + 8*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], -1] + 
        (I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]] + (5^(1/4)*(1 + (8*I)*Sqrt[3])*
          GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (I/4)*5^(1/4)*(I + 4*Sqrt[3])*
         GI[-1/2 - (I/2)*Sqrt[3], -1, -1] + (I/8)*5^(1/4)*(I + 4*Sqrt[3])*
         GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]] + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 - (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*
         GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1] + 
        (5^(1/4)*(-1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 8*Sqrt[3])*
          GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1]*Log[2])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + (I/4)*5^(1/4)*
         (I + 4*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2] + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 - (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*
         GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2] + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 - 
        (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        G[0, 2/(-1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/4) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
            GI[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/
           4) + G[0, -2/(1 + I*Sqrt[3]), 1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/4) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
         ((I/2)*Sqrt[3]*5^(1/4)*Pi + (I/8)*5^(1/4)*(I + 4*Sqrt[3])*
           GI[-1/2 + (I/2)*Sqrt[3]] + (5^(1/4)*(I + 4*Sqrt[3])*
            GR[-1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/
           4) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 8*Sqrt[3])*Log[2])/4) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
         ((I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]] + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 8*Sqrt[3])*Log[2])/4) + G[2/(-1 + I*Sqrt[3]), 1]*
         ((-1/8*I)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]] - 
          (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1] - 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2]^2)/8) + G[-2/(1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2]^2)/8) - (I/2)*Sqrt[3]*5^(1/4)*Pi*
         Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]) + 
        GR[-1/2 - (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi - 
          (5^(1/4)*(-13*I - 52*Sqrt[3] + (2*I)*Log[2] + 2*Sqrt[3]*
              Log[65536] + (2*I)*Log[3 - I*Sqrt[3]] - 8*Sqrt[3]*
              Log[3 - I*Sqrt[3]] - (2*I)*Log[3 + I*Sqrt[3]] - 
             8*Sqrt[3]*Log[3 + I*Sqrt[3]]))/8) + GI[-1/2 + (I/2)*Sqrt[3], -1]*
         (-1/2*(Sqrt[3]*5^(1/4)*Pi) + (5^(1/4)*((-16*I)*Sqrt[3]*Log[2] + 
             Log[(3 - I*Sqrt[3])/2] + (4*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]] + (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + 
        GI[-1/2 - (I/2)*Sqrt[3], -1]*(-1/2*(Sqrt[3]*5^(1/4)*Pi) + 
          (5^(1/4)*(-13 + (52*I)*Sqrt[3] - (32*I)*Sqrt[3]*Log[2] + Log[4] + 
             2*Log[3 - I*Sqrt[3]] + (8*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] - 
             2*Log[3 + I*Sqrt[3]] + (8*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/8) + 
        GR[-1/2 + (I/2)*Sqrt[3], -1]*((I/2)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(I*Log[2] - 2*Sqrt[3]*Log[256] - I*Log[3 - I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] + I*Log[3 + I*Sqrt[3]] + 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) - 
        (5^(1/4)*(48*Sqrt[3]*Log[2]^3 - (13*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]]) - 6*Log[2]*
            (Sqrt[3]*Log[20282409603651670423947251286016] + 
             ((-I)*Log[4] + Sqrt[3]*(-52 + Log[256]))*Log[3 - I*Sqrt[3]] + 
             (I - 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
              (-52*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - I*
                Log[3 + I*Sqrt[3]] - 4*Sqrt[3]*Log[3 + I*Sqrt[3]])) + 
           192*Sqrt[3]*Zeta[3]))/48))/EllipticK[(5 + Sqrt[5])/10]^2) + 
   EisensteinH[2, 1, 0, 0, TauA]*
    (((13*5^(1/4)*(2 + (3*I)*Sqrt[3])*Pi^2)/54 - (5^(1/4)*Pi^3)/(6*Sqrt[3]) + 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, -1, (I/2)*(I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((13*I)/8)*5^(1/4)*G[-1, (2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[-1, (-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1])/Pi - (((13*I)/4)*5^(1/4)*
         G[-1, (-2*I)/(I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((13*I)/4)*5^(1/4)*G[-1, (-2*I)/(I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[0, -1, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[0, -1, (-2*I)/(I + Sqrt[3]), 1])/Pi - 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1] - 
       ((5*I)/2)*Sqrt[3]*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1] - 
       (((65*I)/8)*5^(1/4)*G[0, 0, (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/2)*5^(1/4)*G[0, 0, (2*I)/(-I + Sqrt[3]), 1])/Pi + 
       (((13*I)/2)*5^(1/4)*G[0, 0, (-2*I)/(I + Sqrt[3]), 1])/Pi + 
       (((65*I)/8)*5^(1/4)*G[0, 0, (2*I)/(I + Sqrt[3]), 1])/Pi + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1] + 
       (((13*I)/8)*5^(1/4)*G[0, 1, (-2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[0, 1, (2*I)/(I + Sqrt[3]), 1])/Pi - 
       I*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
         1] - I*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
         1] - (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[0, (-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 
          1])/Pi - (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), -1, 1])/
        Pi + (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[0, (2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi + (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), -1, 1])/
        Pi + (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[0, (-2*I)/(I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 
          1])/Pi + (((13*I)/4)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), 1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[0, (2*I)/(I + Sqrt[3]), (-2*I)/(-I + Sqrt[3]), 
          1])/Pi + (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 
         2/(1 + I*Sqrt[3]), 1] + (I/2)*Sqrt[3]*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
         2/(1 + I*Sqrt[3]), 1, 1] + (I/2)*Sqrt[3]*5^(1/4)*
        G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1] + 
       (I/2)*Sqrt[3]*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
         1] - (((13*I)/8)*5^(1/4)*G[(-2*I)/(-I + Sqrt[3]), 1, 
          (2*I)/(I + Sqrt[3]), 1])/Pi - (((13*I)/8)*5^(1/4)*
         G[(-2*I)/(-I + Sqrt[3]), (2*I)/(I + Sqrt[3]), 1, 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, (-2*I)/(I + Sqrt[3]), 
          1])/Pi - (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), 
          (-2*I)/(I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), -1, 
          (I/2)*(I + Sqrt[3]), 1])/Pi + (((13*I)/8)*5^(1/4)*
         G[(-1/2*I)*(-I + Sqrt[3]), (I/2)*(I + Sqrt[3]), -1, 1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), -1, (2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), 
          (2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 1, (-2*I)/(-I + Sqrt[3]), 
          1])/Pi + (((13*I)/8)*5^(1/4)*G[(2*I)/(I + Sqrt[3]), 
          (-2*I)/(-I + Sqrt[3]), 1, 1])/Pi + 
       (((13*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, -1, 1])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 
          (-1/2*I)*(-I + Sqrt[3]), 1])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), (-1/2*I)*(-I + Sqrt[3]), 
          -1, 1])/Pi - (((13*I)/4)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[-1, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[-1, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[-1, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[-1, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/2)*5^(1/4)*G[-1, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + (((3*I)/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - (I*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/2)*5^(1/4)*
         G[-1, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi + (I*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          1])/Pi + ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((5*I)/2)*5^(1/4)*
         G[0, -1, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (((5*I)/2)*5^(1/4)*G[0, -1, -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, -1, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, -1, 0, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          1])/Pi - (I*5^(1/4)*G[0, 0, -1, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -1, -2/(1 + I*Sqrt[3]), 1])/Pi - 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((2*I)*5^(1/4)*G[0, 0, 0, 2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((2*I)*5^(1/4)*G[0, 0, 0, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 0, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 0, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 0, 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/4)*5^(1/4)*G[0, 0, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          -1, 1])/Pi + (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1])/Pi - 
       (I*5^(1/4)*G[0, 0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       (((3*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[0, 0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          1])/Pi - ((I/4)*5^(1/4)*G[0, 1, 1, -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, 1, 2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 1, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 1, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          1])/Pi + ((I/2)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/
        Pi - ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, -2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 
          -1, -1, 1])/Pi + (((3*I)/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - (((3*I)/2)*5^(1/4)*
         G[0, 2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, -1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi - (((3*I)/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/
        Pi - ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/2)*5^(1/4)*G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi - ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/2)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[0, 2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 1, 2/(1 + I*Sqrt[3]), 1])/
        Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          1, 2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi + ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi + 
       (((3*I)/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 1])/
        Pi - (((5*I)/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - (((3*I)/4)*5^(1/4)*
         G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/2)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi + ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi - 
       (((3*I)/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, -1, 1])/Pi + 
       (((5*I)/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 2/(-1 + I*Sqrt[3]), 
          1])/Pi - ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi + (((3*I)/4)*5^(1/4)*
         G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 
          -2/(1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), -1, 1])/Pi + 
       ((I/2)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, -1, 1])/
        Pi - ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 
          2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), -1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 1, -2/(-1 + I*Sqrt[3]), 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 1, 
          1])/Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1, 1])/
        Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 
          2/(1 + I*Sqrt[3]), 1])/Pi - ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
          2/(1 + I*Sqrt[3]), 1, 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 
          -2/(-1 + I*Sqrt[3]), 1])/Pi - 
       ((I/4)*5^(1/4)*G[2/(1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 
          -2/(-1 + I*Sqrt[3]), 1, 1])/Pi + 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) + (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (3*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) + (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
       (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(2*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
       (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1])/(4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        (2*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (2*Pi) + (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/(2*Pi) - 
       (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        (2*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       (((3*I)/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (((3*I)/2)*5^(1/4)*
         GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
        Pi - ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
       (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, -1/2 - (I/2)*Sqrt[3]])/
        Pi - (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, -1])/Pi + 
       (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1, -1])/
        Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
       ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
       (((13*I)/4)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1]*Log[2])/Pi - 
       (((13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), (-2*I)/(I + Sqrt[3]), 1]*
         Log[2])/Pi + (((13*I)/8)*5^(1/4)*G[(-1/2*I)*(-I + Sqrt[3]), 
          (I/2)*(I + Sqrt[3]), 1]*Log[2])/Pi + 
       (((13*I)/8)*5^(1/4)*G[(-2*I)/(I + Sqrt[3]), (2*I)/(-I + Sqrt[3]), 1]*
         Log[2])/Pi - (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 
          (-1/2*I)*(-I + Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
          -2/(1 + I*Sqrt[3]), 1]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 
          2/(-1 + I*Sqrt[3]), 1]*Log[2])/Pi + 
       (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/
        (2*Pi) - (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2])/(2*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) + 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/(4*Pi) - 
       ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
         Log[2])/Pi + ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2])/Pi + 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2])/Pi - 
       (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/
        (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/(4*Pi) + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/Pi - 
       ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
         Log[2]^2)/Pi + GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)) - (5^(1/4)*Log[2])/(4*Pi)) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*
        ((I/2)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*Log[2])/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        ((I/2)*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-2/(1 + I*Sqrt[3]), -1, 2/(-1 + I*Sqrt[3]), 1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*Log[2])/
          Pi) + G[-1, 0, -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, 2/(-1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1, -1]*(I*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, 2/(-1 + I*Sqrt[3]), -1, 1]*
        ((3*I)*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[-1, 2/(-1 + I*Sqrt[3]), -1, 1]*((-2*I)*Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (((3*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), -1, 1]*((5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 
         -2/(1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -1, 2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         (5*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         (((5*I)/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, 0, 2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -2/(1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*((-I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[0, -1, -2/(1 + I*Sqrt[3]), 1]*
        (I*Sqrt[3]*5^(1/4) + ((I/2)*5^(1/4)*Log[2])/Pi) + 
       G[0, -2/(1 + I*Sqrt[3]), -1, 1]*((3*I)*Sqrt[3]*5^(1/4) + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), -1, 1]*(-1/4*(5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
           Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 
         2/(-1 + I*Sqrt[3]), 1]*(I*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + ((I/2)*5^(1/4)*Log[2])/
          Pi) + G[-1, -2/(1 + I*Sqrt[3]), -1, 1]*((-2*I)*Sqrt[3]*5^(1/4) - 
         (3*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (((3*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + G[-1, -1, -2/(1 + I*Sqrt[3]), 1]*
        ((-I)*Sqrt[3]*5^(1/4) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*Log[2])/Pi) + GR[-1/2 + (I/2)*Sqrt[3], -1, 
         -1/2 - (I/2)*Sqrt[3]]*((I/2)*Sqrt[3]*5^(1/4) - 
         (((3*I)/4)*5^(1/4)*Log[2])/Pi) + GR[-1/2 - (I/2)*Sqrt[3], -1, 
         -1/2 + (I/2)*Sqrt[3]]*((I/2)*Sqrt[3]*5^(1/4) + 
         (((3*I)/4)*5^(1/4)*Log[2])/Pi) + G[0, 0, 2/(-1 + I*Sqrt[3]), 1]*
        ((2*I)*Sqrt[3]*5^(1/4) - (I*5^(1/4)*Log[2])/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - (I*5^(1/4)*Log[2])/
          Pi) + G[0, 0, -2/(1 + I*Sqrt[3]), 1]*((2*I)*Sqrt[3]*5^(1/4) + 
         (I*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, 
         2/(-1 + I*Sqrt[3]), 1]*(-1/4*(5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + (I*5^(1/4)*Log[2])/
          Pi) + GR[-1, -1, -1/2 + (I/2)*Sqrt[3]]*((-I)*Sqrt[3]*5^(1/4) - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + G[2/(-1 + I*Sqrt[3]), -1, -1, 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(2*Pi) - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + G[-2/(1 + I*Sqrt[3]), -1, -1, 1]*
        (I*Sqrt[3]*5^(1/4) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]])/Pi + 
         (((3*I)/2)*5^(1/4)*Log[2])/Pi) + GI[-1, -1/2 + (I/2)*Sqrt[3], 
         -1/2 - (I/2)*Sqrt[3]]*(-1/2*(Sqrt[3]*5^(1/4)) + 
         (5^(1/4)*Log[2])/(4*Pi)) + GI[-1/2 + (I/2)*Sqrt[3], -1, -1]*
        (-(Sqrt[3]*5^(1/4)) + (5^(1/4)*Log[2])/(2*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]]*
        (-1/2*(Sqrt[3]*5^(1/4)) + (3*5^(1/4)*Log[2])/(4*Pi)) + 
       GI[-1, -1, -1/2 + (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4) + 
         (3*5^(1/4)*Log[2])/(2*Pi)) + GI[-1, -1/2 + (I/2)*Sqrt[3]]*
        (-(Sqrt[3]*5^(1/4)*Log[2]) - (3*5^(1/4)*Log[2]^2)/(4*Pi)) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*(-1/24*(5^(1/4)*Pi) + 
         (Sqrt[3]*5^(1/4)*Log[2])/2 - (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((-1/24*I)*5^(1/4)*Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*((-1/24*I)*5^(1/4)*Pi - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*((I/24)*5^(1/4)*Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*((I/24)*5^(1/4)*Pi + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         (I/2)*Sqrt[3]*5^(1/4)*Log[2] + ((I/8)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, 2/(-1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(-I + 3*Sqrt[3])*Pi)/6 + I*Sqrt[3]*5^(1/4)*Log[2] - 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
        (-1/12*(5^(1/4)*(I + 6*Sqrt[3])*Pi) - 
         (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]])/2 + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) + 
         (5*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] - 
         ((I/2)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (I*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         I*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -1, 1]*
        (-1/2*(Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3]]) + 
         (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3]] - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi + 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] - ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[-2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*
        (-1/4*(5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[0, -2/(1 + I*Sqrt[3]), 1]*
        ((5^(1/4)*(I + 3*Sqrt[3])*Pi)/6 + I*Sqrt[3]*5^(1/4)*Log[2] + 
         ((I/4)*5^(1/4)*Log[2]^2)/Pi) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
        (-1/12*(5^(1/4)*(-I + 6*Sqrt[3])*Pi) - 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/2 - 
         (5*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/Pi + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/2)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (I*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         I*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       G[2/(-1 + I*Sqrt[3]), -1, 1]*
        (-1/2*(Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]]) - 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (I/2)*Sqrt[3]*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]] + 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (2*I)*Sqrt[3]*5^(1/4)*Log[2] + ((I/4)*5^(1/4)*Log[2]^2)/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3]]*(I*Sqrt[3]*5^(1/4)*Log[2] + 
         (((3*I)/4)*5^(1/4)*Log[2]^2)/Pi) + G[(-2*I)/(I + Sqrt[3]), 1]*
        ((13*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/(8*Pi) + 
         (13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/(8*Pi) - 
         (((13*I)/8)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + G[(I/2)*(I + Sqrt[3]), 1]*
        ((((-13*I)/8)*5^(1/4)*G[(2*I)/(-I + Sqrt[3]), -1, 1])/Pi + 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + G[(2*I)/(-I + Sqrt[3]), 1]*
        ((((13*I)/8)*5^(1/4)*G[-1, (I/2)*(I + Sqrt[3]), 1])/Pi + 
         (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), -1, 1])/Pi + 
         (((13*I)/8)*5^(1/4)*Log[2]^2)/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]]*((5^(1/4)*Pi)/24 + 
         (Sqrt[3]*5^(1/4)*Log[2])/2 + (5^(1/4)*Log[2]^2)/(8*Pi)) + 
       GR[-1/2 + (I/2)*Sqrt[3]]*((-1/2*I)*Sqrt[3]*5^(1/4)*Log[2]^2 - 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + G[-2/(1 + I*Sqrt[3]), 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3]])/2 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1])/2 - 
         (5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(2*Pi) - 
         (5*5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) - (3*5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(2*Pi) + 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) + (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 + (I/2)*Sqrt[3]] - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 + (I/2)*Sqrt[3], -1] + 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((5*I)/4)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3]])/
          Pi + (((3*I)/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1, -1/2 + (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/2)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 + (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi - ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi - ((I/4)*5^(1/4)*
           GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi - (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 - ((I/4)*5^(1/4)*Log[2]^3)/
          Pi) + G[2/(-1 + I*Sqrt[3]), 1]*
        ((Sqrt[3]*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3]])/2 + 
         (Sqrt[3]*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1])/2 + 
         (5*5^(1/4)*GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/(4*Pi) + 
         (5^(1/4)*GI[-1, -1, -1/2 + (I/2)*Sqrt[3]])/(2*Pi) + 
         (3*5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) + (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/(4*Pi) - 
         (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/
          (4*Pi) - (5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/(2*Pi) - 
         (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          (4*Pi) - (5^(1/4)*GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/(4*Pi) - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1, -1/2 - (I/2)*Sqrt[3]] - (I/2)*Sqrt[3]*5^(1/4)*
          GR[-1/2 - (I/2)*Sqrt[3], -1] - (((5*I)/4)*5^(1/4)*
           GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/2)*5^(1/4)*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/Pi - 
         (((3*I)/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1, -1/2 - (I/2)*Sqrt[3], 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1, -1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]])/Pi - 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1, 
            -1/2 + (I/2)*Sqrt[3]])/Pi + ((I/4)*5^(1/4)*
           GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1])/
          Pi + ((I/2)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/Pi + 
         ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1, -1/2 - (I/2)*Sqrt[3]])/
          Pi + ((I/4)*5^(1/4)*GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], 
            -1])/Pi - (I/2)*Sqrt[3]*5^(1/4)*Log[2]^2 + 
         ((I/4)*5^(1/4)*Log[2]^3)/Pi) + GI[-1/2 + (I/2)*Sqrt[3]]*
        ((Sqrt[3]*5^(1/4)*Log[2]^2)/2 + (5^(1/4)*Log[2]^3)/(4*Pi)) + 
       G[0, (2*I)/(-I + Sqrt[3]), 1]*(((-13*I)/2)*Sqrt[3]*5^(1/4) - 
         ((I/8)*5^(1/4)*(-115 + 26*Log[2]))/Pi) + 
       G[-1, (2*I)/(-I + Sqrt[3]), 1]*(((13*I)/2)*Sqrt[3]*5^(1/4) - 
         (((13*I)/8)*5^(1/4)*G[(I/2)*(I + Sqrt[3]), 1])/Pi - 
         ((I/8)*5^(1/4)*(115 + 26*Log[2]))/Pi) + GR[-1, -1/2 - (I/2)*Sqrt[3]]*
        (I*Sqrt[3]*5^(1/4)*Log[2] - ((I/8)*5^(1/4)*(6*Log[2]^2 - 13*Log[4]))/
          Pi) + GI[-1, -1/2 - (I/2)*Sqrt[3]]*(-(Sqrt[3]*5^(1/4)*Log[2]) + 
         (5^(1/4)*(6*Log[2]^2 - 13*Log[4]))/(8*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1, -1]*(-(Sqrt[3]*5^(1/4)) - 
         (5^(1/4)*(-13 + Log[4]))/(4*Pi)) + GR[-1/2 - (I/2)*Sqrt[3], -1, -1]*
        (I*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(-13 + Log[4]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3]]*((Sqrt[3]*5^(1/4)*Log[2]^2)/2 - 
         (5^(1/4)*Log[2]^2*(-13 + Log[4]))/(8*Pi)) + GR[-1/2 - (I/2)*Sqrt[3]]*
        ((-1/2*I)*Sqrt[3]*5^(1/4)*Log[2]^2 + ((I/8)*5^(1/4)*Log[2]^2*
           (-13 + Log[4]))/Pi) - (((13*I)/8)*5^(1/4)*
         G[-1, (I/2)*(I + Sqrt[3]), 1]*Log[4])/Pi + 
       G[(-2*I)/(I + Sqrt[3]), -1, 1]*((-13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/
          (8*Pi) + (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         (((13*I)/8)*5^(1/4)*Log[4])/Pi) + G[0, (-2*I)/(I + Sqrt[3]), 1]*
        (((-13*I)/2)*Sqrt[3]*5^(1/4) + ((I/8)*5^(1/4)*(-115 + 13*Log[4]))/
          Pi) + G[-1, (-2*I)/(I + Sqrt[3]), 1]*(((13*I)/2)*Sqrt[3]*5^(1/4) - 
         (13*5^(1/4)*GI[-1/2 - (I/2)*Sqrt[3]])/(8*Pi) + 
         (((13*I)/8)*5^(1/4)*GR[-1/2 - (I/2)*Sqrt[3]])/Pi + 
         ((I/8)*5^(1/4)*(115 + 13*Log[4]))/Pi) + 
       GI[-1, -1, -1/2 - (I/2)*Sqrt[3]]*(Sqrt[3]*5^(1/4) - 
         (5^(1/4)*(-13 + Log[64]))/(4*Pi)) + GR[-1, -1, -1/2 - (I/2)*Sqrt[3]]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(-13 + Log[64]))/Pi) + 
       GI[-1, -1/2 - (I/2)*Sqrt[3], -1]*(-2*Sqrt[3]*5^(1/4) - 
         (5^(1/4)*(-13 + Log[64] + 4*Log[3 - I*Sqrt[3]] - 
            4*Log[3 + I*Sqrt[3]]))/(4*Pi)) + GR[-1, -1/2 - (I/2)*Sqrt[3], -1]*
        ((2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(-13 + Log[64] + 
            4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) - ((I/4)*5^(1/4)*(Log[2] + 
            2*Log[3 - I*Sqrt[3]] - 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((Sqrt[3]*5^(1/4))/2 + (5^(1/4)*(Log[2] + 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) - 
       ((I/24)*5^(1/4)*Log[2]*(2*Log[(3 - I*Sqrt[3])/2]^3 + 
          (345 + 8*Log[2]^2 - 39*Log[4])*Log[3 - I*Sqrt[3]] + 
          39*Log[3 - I*Sqrt[3]]^2 - 2*Log[(3 + I*Sqrt[3])/2]^3 - 
          345*Log[3 + I*Sqrt[3]] - 8*Log[2]^2*Log[3 + I*Sqrt[3]] + 
          39*Log[4]*Log[3 + I*Sqrt[3]] - 39*Log[3 + I*Sqrt[3]]^2))/Pi + 
       GI[-1/2 + (I/2)*Sqrt[3], -1]*((5^(1/4)*(-1 - (6*I)*Sqrt[3])*Pi)/12 - 
         (5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/(4*Pi) - Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1]*(-1/12*(5^(1/4)*(-I + 6*Sqrt[3])*Pi) + 
         ((I/4)*5^(1/4)*(3*Log[2]^2 + Log[(3 - I*Sqrt[3])/2]^2 - 
            Log[(3 + I*Sqrt[3])/2]^2))/Pi + I*Sqrt[3]*5^(1/4)*
          (Log[(3 - I*Sqrt[3])/16] + Log[3 + I*Sqrt[3]])) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((Sqrt[3]*5^(1/4))/2 - (5^(1/4)*(Log[2] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((-1/2*I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(Log[2] - 
            2*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1, -1/2 + (I/2)*Sqrt[3], -1]*((2*I)*Sqrt[3]*5^(1/4) - 
         ((I/2)*5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/Pi) + GI[-1, -1/2 + (I/2)*Sqrt[3], -1]*
        (-2*Sqrt[3]*5^(1/4) + (5^(1/4)*(Log[8] - 2*Log[3 - I*Sqrt[3]] + 
            2*Log[3 + I*Sqrt[3]]))/(2*Pi)) + G[(I/2)*(I + Sqrt[3]), -1, 1]*
        (((13*I)/2)*Sqrt[3]*5^(1/4) - ((I/8)*5^(1/4)*(-115 + 13*Log[4] - 
            26*Log[3 - I*Sqrt[3]] + 26*Log[3 + I*Sqrt[3]]))/Pi) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1]*(-1/12*(5^(1/4)*(I + 6*Sqrt[3])*Pi) - 
         (I/2)*Sqrt[3]*5^(1/4)*(-13 + Log[256] - 2*Log[3 - I*Sqrt[3]] - 
           2*Log[3 + I*Sqrt[3]]) - ((I/8)*5^(1/4)*(115 + 6*Log[2]^2 - 
            13*Log[4] - 2*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
            2*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/Pi) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1]*((5^(1/4)*(1 - (6*I)*Sqrt[3])*Pi)/12 + 
         (Sqrt[3]*5^(1/4)*(-13 + Log[256] - 2*Log[3 - I*Sqrt[3]] - 
            2*Log[3 + I*Sqrt[3]]))/2 + (5^(1/4)*(115 + 6*Log[2]^2 - 
            13*Log[4] - 2*Log[(3 - I*Sqrt[3])/2]^2 - 26*Log[3 - I*Sqrt[3]] + 
            2*Log[(3 + I*Sqrt[3])/2]^2 + 26*Log[3 + I*Sqrt[3]]))/(8*Pi)) + 
       GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        (Sqrt[3]*5^(1/4) - (5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
            2*Log[6 - (2*I)*Sqrt[3]]))/(4*Pi)) + 
       GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1]*
        ((-I)*Sqrt[3]*5^(1/4) + ((I/4)*5^(1/4)*(13 + 2*Log[3 + I*Sqrt[3]] - 
            2*Log[6 - (2*I)*Sqrt[3]]))/Pi) + 
       GR[-1/2 + (I/2)*Sqrt[3], -1/2 + (I/2)*Sqrt[3], -1]*
        ((-I)*Sqrt[3]*5^(1/4) - ((I/2)*5^(1/4)*(Log[3 - I*Sqrt[3]] - 
            Log[6 + (2*I)*Sqrt[3]]))/Pi) + GI[-1/2 + (I/2)*Sqrt[3], 
         -1/2 + (I/2)*Sqrt[3], -1]*(Sqrt[3]*5^(1/4) + 
         (5^(1/4)*(Log[3 - I*Sqrt[3]] - Log[6 + (2*I)*Sqrt[3]]))/(2*Pi)) - 
       (5^(1/4)*Pi*(Log[4]*(Sqrt[3]*Log[4096] - I*(Log[3 - I*Sqrt[3]] - 
              Log[3 + I*Sqrt[3]])) + I*(I*Sqrt[3]*Log[4096]*
             (Log[3 - I*Sqrt[3]] + Log[3 + I*Sqrt[3]]) + Log[3 - I*Sqrt[3]]*
             Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] - Log[3 + I*Sqrt[3]]*
             Log[(3*I + Sqrt[3])/(I + Sqrt[3])] + 
            Log[2]*(4*Log[3 - I*Sqrt[3]] - 4*Log[3 + I*Sqrt[3]] - 
              Log[(-3*I + Sqrt[3])/(-I + Sqrt[3])] + Log[(3*I + Sqrt[3])/
                (I + Sqrt[3])]))))/24 - (I/2)*Sqrt[3]*5^(1/4)*
        (8*Log[2]^2 + 2*Log[2]^3 + Log[2]*(13*Log[3 - I*Sqrt[3]] + 
           Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
            (13 + Log[3 + I*Sqrt[3]]) - Log[4]*(17 + Log[3 - I*Sqrt[3]] + 
             Log[3 + I*Sqrt[3]])) + 8*Zeta[3]))/EllipticK[(5 + Sqrt[5])/10] + 
     (EllipticK[(5 - Sqrt[5])/10]*((-13*5^(1/4)*Pi^2)/(6*Sqrt[3]) + 
        (5^(1/4)*(2 - (3*I)*Sqrt[3])*Pi^3)/54 - 
        (13*5^(1/4)*(I + 4*Sqrt[3])*G[-1, (2*I)/(-I + Sqrt[3]), 1])/8 - 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[-1, (-2*I)/(I + Sqrt[3]), 1])/8 + 
        (13*5^(1/4)*(I + 4*Sqrt[3])*G[0, (2*I)/(-I + Sqrt[3]), 1])/8 + 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[0, (-2*I)/(I + Sqrt[3]), 1])/8 - 
        (13*5^(1/4)*(-I + 4*Sqrt[3])*G[(I/2)*(I + Sqrt[3]), -1, 1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -1, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, -1, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 0, 2/(-1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[-1, 0, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 8*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 8*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), -1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-1, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          G[-1, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -1, 2/(-1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -1, -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5*5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(-1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(-1 + I*Sqrt[3]), 1])/2 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 0, -2/(1 + I*Sqrt[3]), 1])/2 + 
        (5*5^(1/4)*(-I + 4*Sqrt[3])*G[0, 0, 2/(1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 1, -2/(-1 + I*Sqrt[3]), 1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 1, 2/(1 + I*Sqrt[3]), 1])/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 1, 1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(-1 + I*Sqrt[3]), 
           2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I - 12*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(-1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, 2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 - 
        (5^(1/4)*(I + 12*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, -2/(1 + I*Sqrt[3]), 
           2/(-1 + I*Sqrt[3]), 1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[0, -2/(1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), 1, 1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[0, 2/(1 + I*Sqrt[3]), -2/(-1 + I*Sqrt[3]), 
           1])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(-1 + I*Sqrt[3]), 1, 
           2/(1 + I*Sqrt[3]), 1])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(-1 + I*Sqrt[3]), 2/(1 + I*Sqrt[3]), 1, 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, -1, 1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(-1 + I*Sqrt[3]), -1, 
           -2/(1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), -1, 1])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, -1, 1])/4 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), -1, 
           2/(-1 + I*Sqrt[3]), 1])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), -1, 1])/8 - 
        (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 1, -2/(-1 + I*Sqrt[3]), 
           1])/8 - (5^(1/4)*(I + 4*Sqrt[3])*G[2/(1 + I*Sqrt[3]), 
           -2/(-1 + I*Sqrt[3]), 1, 1])/8 + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + (I/4)*5^(1/4)*(I + 4*Sqrt[3])*
         GI[-1, -1, -1/2 + (I/2)*Sqrt[3]] + (5^(1/4)*(1 - (8*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 - (8*I)*Sqrt[3])*
          GI[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 - (I/8)*5^(1/4)*
         (-I + 4*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3], 
          -1/2 - (I/2)*Sqrt[3]] + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-1 + (4*I)*Sqrt[3])*
          GI[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3], -1] + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 - (I/8)*5^(1/4)*
         (-I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1, 
          -1/2 - (I/2)*Sqrt[3]] + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1, -1, -1/2 - (I/2)*Sqrt[3]])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1, -1/2 + (I/2)*Sqrt[3]])/4 - 
        (5^(1/4)*(I + 8*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(-I + 8*Sqrt[3])*
          GR[-1, -1/2 + (I/2)*Sqrt[3], -1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3], 
           -1/2 - (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1, -1])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1, 
           -1/2 + (I/2)*Sqrt[3]])/8 + (5^(1/4)*(I + 4*Sqrt[3])*
          GR[-1/2 - (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1, -1])/4 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1, 
           -1/2 - (I/2)*Sqrt[3]])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3], -1])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3], -1])/4 + (5^(1/4)*(I + 4*Sqrt[3])*
          G[2/(-1 + I*Sqrt[3]), -2/(1 + I*Sqrt[3]), 1]*Log[2])/8 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*G[-2/(1 + I*Sqrt[3]), 2/(-1 + I*Sqrt[3]), 
           1]*Log[2])/8 + (5^(1/4)*(-1 - (4*I)*Sqrt[3])*
          GI[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
        (I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], 
          -1/2 + (I/2)*Sqrt[3]]*Log[2] + (5^(1/4)*(1 + (4*I)*Sqrt[3])*
          GI[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 - 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]]*Log[2])/4 - 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]]*Log[2])/4 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], 
           -1/2 + (I/2)*Sqrt[3]]*Log[2])/8 + (5^(1/4)*(-I + 4*Sqrt[3])*
          GR[-1/2 + (I/2)*Sqrt[3], -1/2 - (I/2)*Sqrt[3]]*Log[2])/8 + 
        (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2 + 
        (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]]*Log[2]^2)/8 + 
        G[0, 2/(-1 + I*Sqrt[3]), 1]*((I/2)*Sqrt[3]*5^(1/4)*Pi - 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/4) + G[-1, 2/(-1 + I*Sqrt[3]), 1]*
         ((-1/2*I)*Sqrt[3]*5^(1/4)*Pi - (I/8)*5^(1/4)*(-I + 4*Sqrt[3])*
           GI[-1/2 - (I/2)*Sqrt[3]] - (5^(1/4)*(-I + 4*Sqrt[3])*
            GR[-1/2 - (I/2)*Sqrt[3]])/8 - (5^(1/4)*(-I + 4*Sqrt[3])*Log[2])/
           4) + G[0, -2/(1 + I*Sqrt[3]), 1]*((I/2)*Sqrt[3]*5^(1/4)*Pi - 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/4) + G[-1, -2/(1 + I*Sqrt[3]), 1]*
         ((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*(1 - (4*I)*Sqrt[3])*
            GI[-1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*
            GR[-1/2 + (I/2)*Sqrt[3]])/8 - (5^(1/4)*(I + 4*Sqrt[3])*Log[2])/
           4) + G[2/(-1 + I*Sqrt[3]), -1, 1]*
         ((-1/8*I)*5^(1/4)*(-I + 4*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3]] - 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(-I + 8*Sqrt[3])*Log[2])/4) + G[-2/(1 + I*Sqrt[3]), -1, 1]*
         ((5^(1/4)*(1 - (4*I)*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3]])/8 - 
          (5^(1/4)*(I + 8*Sqrt[3])*Log[2])/4) + G[2/(-1 + I*Sqrt[3]), 1]*
         ((5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(1 + (4*I)*Sqrt[3])*GI[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1, -1/2 - (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*GR[-1/2 - (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(-I + 4*Sqrt[3])*Log[2]^2)/8) + G[-2/(1 + I*Sqrt[3]), 1]*
         ((I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1, -1/2 + (I/2)*Sqrt[3]] + 
          (I/8)*5^(1/4)*(I + 4*Sqrt[3])*GI[-1/2 + (I/2)*Sqrt[3], -1] + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1, -1/2 + (I/2)*Sqrt[3]])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*GR[-1/2 + (I/2)*Sqrt[3], -1])/8 + 
          (5^(1/4)*(I + 4*Sqrt[3])*Log[2]^2)/8) + (I/2)*Sqrt[3]*5^(1/4)*Pi*
         Log[2]*(Log[(3 - I*Sqrt[3])/4] + Log[3 + I*Sqrt[3]]) + 
        GR[-1/2 - (I/2)*Sqrt[3], -1]*((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + 
          (5^(1/4)*(-13*I - 52*Sqrt[3] + (2*I)*Log[2] + 2*Sqrt[3]*
              Log[65536] + (2*I)*Log[3 - I*Sqrt[3]] - 8*Sqrt[3]*
              Log[3 - I*Sqrt[3]] - (2*I)*Log[3 + I*Sqrt[3]] - 
             8*Sqrt[3]*Log[3 + I*Sqrt[3]]))/8) + GR[-1/2 + (I/2)*Sqrt[3], -1]*
         ((-1/2*I)*Sqrt[3]*5^(1/4)*Pi + (5^(1/4)*((-I)*Log[2] + 
             2*Sqrt[3]*Log[256] + I*Log[3 - I*Sqrt[3]] - 
             4*Sqrt[3]*Log[3 - I*Sqrt[3]] - I*Log[3 + I*Sqrt[3]] - 
             4*Sqrt[3]*Log[3 + I*Sqrt[3]]))/4) + GI[-1/2 - (I/2)*Sqrt[3], -1]*
         ((Sqrt[3]*5^(1/4)*Pi)/2 + (5^(1/4)*(13 - (52*I)*Sqrt[3] + 
             (32*I)*Sqrt[3]*Log[2] - Log[4] - 2*Log[3 - I*Sqrt[3]] - 
             (8*I)*Sqrt[3]*Log[3 - I*Sqrt[3]] + 2*Log[3 + I*Sqrt[3]] - 
             (8*I)*Sqrt[3]*Log[3 + I*Sqrt[3]]))/8) + 
        GI[-1/2 + (I/2)*Sqrt[3], -1]*((Sqrt[3]*5^(1/4)*Pi)/2 + 
          (5^(1/4)*((16*I)*Sqrt[3]*Log[2] + (-1 - (4*I)*Sqrt[3])*
              Log[3 - I*Sqrt[3]] - (4*I)*Sqrt[3]*Log[3 + I*Sqrt[3]] + 
             Log[6 + (2*I)*Sqrt[3]]))/4) + 
        (5^(1/4)*(48*Sqrt[3]*Log[2]^3 - (13*I)*Log[64]*(Log[3 - I*Sqrt[3]] - 
             Log[3 + I*Sqrt[3]]) - 6*Log[2]*
            (Sqrt[3]*Log[20282409603651670423947251286016] + 
             ((-I)*Log[4] + Sqrt[3]*(-52 + Log[256]))*Log[3 - I*Sqrt[3]] + 
             (I - 4*Sqrt[3])*Log[3 - I*Sqrt[3]]^2 + Log[3 + I*Sqrt[3]]*
              (-52*Sqrt[3] + I*Log[4] + Sqrt[3]*Log[256] - I*
                Log[3 + I*Sqrt[3]] - 4*Sqrt[3]*Log[3 + I*Sqrt[3]])) + 
           192*Sqrt[3]*Zeta[3]))/48))/EllipticK[(5 + Sqrt[5])/10]^2)}
