(* Created with the Wolfram Language : www.wolfram.com *)
{F[57, 0] -> ((-5*I)/24)*Pi^3 - (Pi^2*G[0, 2])/4 - I*Pi*G[0, 0, 2] + 
   sGt[{1, -1/3}, 1/12 + TauA/4, TauA]*sGt[{1, (-1 - 3*TauA)/12}, 
     {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA] + 
   sGt[{1, -5/12 - TauA/4}, 1/4 - TauA/4, TauA]*
    (-4*sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA] - 
     sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA] + 
     (3*sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA])/2 + 
     sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]/2 - 
     2*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] + 
     sGt[{1, (1 + TauA)/2}, {1, 1/6}, (1 + 3*TauA)/12, TauA]/2 + 
     (7*sGt[{1, (-1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2) - 
   3*sGt[{1, -1/3}, {1, -1/3}, {1, -1/3}, 1/3, TauA] - 
   4*sGt[{1, -1/3}, {1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA] - 
   sGt[{1, -1/3}, {1, -1/3}, {1, 1/3}, 1/3, TauA] + 
   4*sGt[{1, -1/3}, {1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   5*sGt[{1, -1/3}, {1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   5*sGt[{1, -1/3}, {1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA] + 
   sGt[{1, -1/3}, {1, 0}, {1, -1/3}, 1/3, TauA] + 
   sGt[{1, -1/3}, {1, 0}, {1, 1/3}, 1/3, TauA] - 
   sGt[{1, -1/3}, {1, 0}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   sGt[{1, -1/3}, {1, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   2*sGt[{1, -1/3}, {1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA] - 
   (3*sGt[{1, -1/3}, {1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA])/2 + 
   (3*sGt[{1, -1/3}, {1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA])/2 + 
   sGt[{1, -1/3}, {1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]/2 - 
   2*sGt[{1, -1/3}, {1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
   sGt[{1, -1/3}, {1, 1/3}, {1, -1/3}, 1/3, TauA]/2 - 
   sGt[{1, -1/3}, {1, 1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA]/2 + 
   sGt[{1, -1/3}, {1, 1/3}, {1, 1/3}, 1/3, TauA]/2 + 
   sGt[{1, -1/3}, {1, 1/3}, {1, (1 + TauA)/2}, 1/3, TauA]/2 - 
   sGt[{1, -1/3}, {1, 1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA]/2 - 
   sGt[{1, -1/3}, {1, (1 + TauA)/2}, {1, -1/3}, 1/3, TauA]/2 + 
   sGt[{1, -1/3}, {1, (1 + TauA)/2}, {1, 1/6}, (1 + 3*TauA)/12, TauA]/2 + 
   sGt[{1, -1/3}, {1, (1 + TauA)/2}, {1, 1/3}, 1/3, TauA]/2 - 
   (3*sGt[{1, -1/3}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, 1/3, TauA])/2 - 
   2*sGt[{1, -1/3}, {1, (1 + TauA)/2}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   (7*sGt[{1, -1/3}, {1, (1 + TauA)/2}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 + 
   (5*sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 - 
   sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]/2 + 
   sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]/2 + 
   sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]/2 - 
   3*sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA] + 
   4*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA] + 
   (7*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/
    2 - 2*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA] - 
   6*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   (5*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 1/3, TauA])/
    2 - sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, 
    TauA] + 3*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA] - 6*sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA] + 
   2*sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA] + 
   3*sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA] - 
   (5*sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 1/3, TauA])/
    2 - sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, 
    TauA] + 6*sGt[{1, 0}, {1, -1/3}, {1, -1/3}, 1/3, TauA] + 
   (7*sGt[{1, 0}, {1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 - 
   4*sGt[{1, 0}, {1, -1/3}, {1, 1/3}, 1/3, TauA] - 
   3*sGt[{1, 0}, {1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   4*sGt[{1, 0}, {1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   3*sGt[{1, 0}, {1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA] + 
   (5*sGt[{1, 0}, {1, 0}, {1, -1/3}, 1/3, TauA])/2 - 
   (5*sGt[{1, 0}, {1, 0}, {1, 1/3}, 1/3, TauA])/2 + 
   (5*sGt[{1, 0}, {1, 0}, {1, (1 + TauA)/2}, 1/3, TauA])/2 - 
   sGt[{1, 0}, {1, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   (3*sGt[{1, 0}, {1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 + 
   sGt[{1, 0}, {1, 1/6}, {1, -1/3}, (1 + 3*TauA)/12, TauA] - 
   sGt[{1, 0}, {1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA] + 
   sGt[{1, 0}, {1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA] + 
   2*sGt[{1, 0}, {1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
   3*sGt[{1, 0}, {1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
   4*sGt[{1, 0}, {1, 1/3}, {1, -1/3}, 1/3, TauA] - 
   (3*sGt[{1, 0}, {1, 1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 + 
   4*sGt[{1, 0}, {1, 1/3}, {1, 1/3}, 1/3, TauA] - 
   sGt[{1, 0}, {1, 1/3}, {1, (1 + TauA)/2}, 1/3, TauA] - 
   5*sGt[{1, 0}, {1, 1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   6*sGt[{1, 0}, {1, 1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA] + 
   sGt[{1, 0}, {1, (1 + TauA)/2}, {1, -1/3}, 1/3, TauA] + 
   (3*sGt[{1, 0}, {1, (1 + TauA)/2}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 - 
   sGt[{1, 0}, {1, (1 + TauA)/2}, {1, 1/3}, 1/3, TauA] - 
   2*sGt[{1, 0}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, 1/3, TauA] - 
   sGt[{1, 0}, {1, (1 + TauA)/2}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   3*sGt[{1, 0}, {1, (1 + TauA)/2}, {1, (1 + 3*TauA)/6}, 1/3, TauA] - 
   (5*sGt[{1, 0}, {1, (-2 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 + 
   sGt[{1, 0}, {1, (-2 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]/2 - 
   sGt[{1, 0}, {1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]/2 + 
   2*sGt[{1, 0}, {1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   sGt[{1, 0}, {1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]/2 + 
   (3*sGt[{1, 0}, {1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 - 
   (3*sGt[{1, 0}, {1, (-1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA])/2 + 
   (3*sGt[{1, 0}, {1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA])/2 - 
   3*sGt[{1, 0}, {1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   (3*sGt[{1, 0}, {1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 - 
   2*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA] - 
   (7*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 + 
   4*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA] + 
   3*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   2*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   7*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA] - 
   sGt[{1, 1/6}, {1, 1/3}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 + 
   sGt[{1, 1/6}, {1, 1/3}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]/2 + 
   sGt[{1, 1/6}, {1, 1/3}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]/2 - 
   sGt[{1, 1/6}, {1, 1/3}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] + 
   sGt[{1, 1/6}, {1, (1 + TauA)/2}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 - 
   sGt[{1, 1/6}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]/
    2 - sGt[{1, 1/6}, {1, (1 + TauA)/2}, {1, (-1 + 3*TauA)/6}, 
     (1 + 3*TauA)/12, TauA]/2 + sGt[{1, 1/6}, {1, (1 + TauA)/2}, 
    {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
   sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA] + 
   sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, 
    TauA] - sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
    (1 + 3*TauA)/12, TauA] - (3*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, {1, 1/3}, 
      (1 + 3*TauA)/12, TauA])/2 + 
   (3*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, 
      TauA])/2 + (5*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
      (1 + 3*TauA)/12, TauA])/2 - 4*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, 
     {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
   4*sGt[{1, (-5 - 3*TauA)/12}, {1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
     (1 - TauA)/4, TauA] - 
   (3*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
      (1 - TauA)/4, TauA])/2 + sGt[{1, (-5 - 3*TauA)/12}, 
     {1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, TauA]/2 - 
   2*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, 
     (1 - TauA)/4, TauA] + 
   (3*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 - sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
     {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]/2 + 
   (7*sGt[{1, (-5 - 3*TauA)/12}, {1, (-1 + TauA)/4}, {1, (1 - 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 + sGt[{1, (-5 - 3*TauA)/12}, 
     {1, (5 + 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]/2 + 
   (7*sGt[{1, (-1 - 3*TauA)/12}, {1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 + sGt[{1, (-1 - 3*TauA)/12}, 
    {1, (1 - 3*TauA)/12}, {1, (-5 - 3*TauA)/12}, (1 - TauA)/4, TauA] - 
   sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
    (1 - TauA)/4, TauA] + 2*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
     {1, (-1 + TauA)/4}, (1 - TauA)/4, TauA] - 
   3*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, 
     (1 - TauA)/4, TauA] + sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
    {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA] - 
   (3*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - TauA)/4}, {1, (1 - 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 - 
   (7*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 + 
   (3*sGt[{1, (-1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 - sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
     {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]/2 + 
   sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, {1, (-1 + TauA)/4}, 
     (1 - TauA)/4, TauA]/2 - sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
    {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA] + 
   sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, {1, (5 + 3*TauA)/12}, 
     (1 - TauA)/4, TauA]/2 - sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, 
    {1, (1 - TauA)/4}, (1 - TauA)/4, TauA] - sGt[{1, (1 - 3*TauA)/12}, 
    {1, (-1 + TauA)/4}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA] + 
   sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, {1, (5 + 3*TauA)/12}, 
    (1 - TauA)/4, TauA] - (3*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, 
      {1, (1 - TauA)/4}, (1 - TauA)/4, TauA])/2 + 
   (5*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, {1, (-1 + TauA)/4}, 
      (1 - TauA)/4, TauA])/2 - 4*sGt[{1, (1 - 3*TauA)/12}, 
     {1, (1 + 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA] + 
   (3*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
      (1 - TauA)/4, TauA])/2 + sGt[{1, (1 - 3*TauA)/12}, 
     {1, (5 + 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]/2 - 
   sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, {1, (-1 + TauA)/4}, 
     (1 - TauA)/4, TauA]/2 + sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
    {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA] - 
   sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
     (1 - TauA)/4, TauA]/2 + sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, {1, -1/3}, 
    1/3, TauA] + sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, {1, 1/3}, 1/3, TauA] - 
   sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA] - 
   (5*sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA])/
    2 + (3*sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, 
      TauA])/2 - (7*sGt[{1, (-2 + 3*TauA)/6}, {1, 0}, {1, -1/3}, 1/3, TauA])/
    2 + (3*sGt[{1, (-2 + 3*TauA)/6}, {1, 0}, {1, 1/3}, 1/3, TauA])/2 - 
   (3*sGt[{1, (-2 + 3*TauA)/6}, {1, 0}, {1, (1 + TauA)/2}, 1/3, TauA])/2 + 
   (7*sGt[{1, (-2 + 3*TauA)/6}, {1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, 1/3}, {1, -1/3}, 1/3, TauA]/2 - 
   sGt[{1, (-2 + 3*TauA)/6}, {1, 1/3}, {1, 1/3}, 1/3, TauA]/2 + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, 1/3}, {1, (1 + TauA)/2}, 1/3, TauA]/2 + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, 1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   (3*sGt[{1, (-2 + 3*TauA)/6}, {1, 1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/
    2 - sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, -1/3}, 1/3, TauA]/
    2 + sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, 1/3}, 1/3, TauA]/2 - 
   sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, 1/3, TauA]/
    2 - sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, (-1 + 3*TauA)/6}, 
    1/3, TauA] + (3*sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, 
      {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 + sGt[{1, (-2 + 3*TauA)/6}, 
    {1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA] - 
   sGt[{1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]/2 + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, 
     TauA]/2 + 4*sGt[{1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
     {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   (5*sGt[{1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
      1/3, TauA])/2 + (3*sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
      {1, -1/3}, 1/3, TauA])/2 - 2*sGt[{1, (-2 + 3*TauA)/6}, 
     {1, (1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA] + 
   2*sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, 
     TauA] + sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
    {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   5*sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA] + 2*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, {1, 1/3}, 1/3, TauA] - 
   3*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   (5*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, {1, (-2 + 3*TauA)/6}, 1/3, TauA])/
    2 - 2*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, 
     TauA] + sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, 
    TauA] + (5*sGt[{1, (-1 + 3*TauA)/6}, {1, 0}, {1, -1/3}, 1/3, TauA])/2 - 
   (5*sGt[{1, (-1 + 3*TauA)/6}, {1, 0}, {1, 1/3}, 1/3, TauA])/2 + 
   (5*sGt[{1, (-1 + 3*TauA)/6}, {1, 0}, {1, (1 + TauA)/2}, 1/3, TauA])/2 - 
   sGt[{1, (-1 + 3*TauA)/6}, {1, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   (3*sGt[{1, (-1 + 3*TauA)/6}, {1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 + 
   2*sGt[{1, (-1 + 3*TauA)/6}, {1, 1/3}, {1, -1/3}, 1/3, TauA] - 
   2*sGt[{1, (-1 + 3*TauA)/6}, {1, 1/3}, {1, 1/3}, 1/3, TauA] - 
   sGt[{1, (-1 + 3*TauA)/6}, {1, 1/3}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, 1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, -1/3}, 1/3, TauA] - 
   sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, 1/3}, 1/3, TauA] + 
   4*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, 1/3, 
     TauA] + 5*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 
     {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   9*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA] + sGt[{1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, {1, 1/3}, 1/3, 
     TauA]/2 - sGt[{1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 
     {1, (1 + TauA)/2}, 1/3, TauA]/2 + 
   2*sGt[{1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
     1/3, TauA] + sGt[{1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 
     {1, (1 + 3*TauA)/6}, 1/3, TauA]/2 - 
   (9*sGt[{1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/
    2 + (9*sGt[{1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, 1/3}, 1/3, 
      TauA])/2 + (15*sGt[{1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
      {1, (1 + TauA)/2}, 1/3, TauA])/2 - 
   3*sGt[{1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
     1/3, TauA] - (9*sGt[{1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
      {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 + 
   2*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA] - 
   2*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA] - 
   9*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, 
     TauA] - 4*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
     {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   11*sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
     1/3, TauA] - 5*sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, {1, -1/3}, 1/3, 
     TauA] + sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, {1, 1/3}, 1/3, TauA] + 
   4*sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA] - 
   (5*sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, {1, (-2 + 3*TauA)/6}, 1/3, TauA])/
    2 + sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, 
    TauA] - 3*sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA] + sGt[{1, (1 + 3*TauA)/6}, {1, 0}, {1, -1/3}, 1/3, TauA] + 
   sGt[{1, (1 + 3*TauA)/6}, {1, 0}, {1, 1/3}, 1/3, TauA] - 
   sGt[{1, (1 + 3*TauA)/6}, {1, 0}, {1, (1 + TauA)/2}, 1/3, TauA] + 
   sGt[{1, (1 + 3*TauA)/6}, {1, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   2*sGt[{1, (1 + 3*TauA)/6}, {1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA] + 
   (3*sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, {1, -1/3}, 1/3, TauA])/2 - 
   (3*sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, {1, 1/3}, 1/3, TauA])/2 + 
   sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, {1, (1 + TauA)/2}, 1/3, TauA]/2 + 
   2*sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
   (5*sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 - 
   sGt[{1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, -1/3}, 1/3, TauA]/2 + 
   sGt[{1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, 1/3}, 1/3, TauA]/2 + 
   sGt[{1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, 1/3, TauA]/
    2 - sGt[{1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA]/2 - sGt[{1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, {1, 1/3}, 1/3, 
     TauA]/2 + sGt[{1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 
     {1, (1 + TauA)/2}, 1/3, TauA]/2 - 
   2*sGt[{1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
     1/3, TauA] - sGt[{1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 
     {1, (1 + 3*TauA)/6}, 1/3, TauA]/2 - 
   4*sGt[{1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, 
     TauA] - sGt[{1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 
    {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   3*sGt[{1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA] - 4*sGt[{1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, 
     TauA] - sGt[{1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 
    1/3, TauA] - sGt[{1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
    {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
   6*sGt[{1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, 
     TauA] + sGt[{0, 0}, {1, (1 + TauA)/2}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (5 + 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {0, 0}, {1, 1/3}, 1/3, TauA]*
    ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, 1/3}, {1, 1/3}, 1/3, TauA]*
    ((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])) + 
   sGt[{0, 0}, {1, (1 + TauA)/2}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    (-2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     6*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])) + sGt[{1, 0}, {1, -1/3}, 1/3, TauA]*
    ((((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, 0}, {1, 1/3}, 1/3, TauA]*
    ((((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{0, 0}, {1, 1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
        qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 - TauA)/4}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
    (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
        qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
        qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, 1/3}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + TauA)/2}, {1, 1/3}, 1/3, TauA]*
    (((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, 1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     (9*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + TauA)/2}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])) + 
   sGt[{0, 0}, {1, (1 + TauA)/2}, {1, -1/3}, 1/3, TauA]*
    (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])) + 
   sGt[{0, 0}, {1, 1/3}, {1, -1/3}, 1/3, TauA]*
    ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, 1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    (((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + TauA)/2}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    (((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((6*I)*Pi - (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (5*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{0, 0}, {1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]*
    ((4*I)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (9*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{0, 0}, {1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((4*I)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (9*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{1, -1/3}, {1, 0}, {0, 0}, 1/3, TauA]*
    ((6*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     ((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, 0}, {0, 0}, 1/3, TauA]*
    ((6*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     ((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA]*
    ((-2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 + (5*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3) + sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]*
    (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*5^(1/4)) + 2*(((4*I)/3)*Pi - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 2*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3) + sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, TauA]*
    (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*5^(1/4)) + 2*(((4*I)/3)*Pi - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 2*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3) + sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
     2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (5*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
            qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/3) + sGt[{0, 0}, 1/12 + TauA/4, TauA]*
    sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
    ((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{1, 0}, {1, 1/6}, {0, 0}, (1 + 3*TauA)/12, TauA]*
    ((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, {0, 0}, (1 - TauA)/4, 
     TauA]*((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{0, 0}, {1, 1/6}, {1, -1/3}, (1 + 3*TauA)/12, TauA]*
    ((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (9*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{0, 0}, {1, (1 - 3*TauA)/12}, {1, (-5 - 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (9*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{0, 0}, {1, (-2 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA]*
    ((2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{1, 0}, {0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((-4*I)*Pi + ((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
   sGt[{0, 0}, {1, -1/3}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, -1/3}, {0, 0}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {1, (-2 + 3*TauA)/6}, {0, 0}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {0, 0}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-2 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, 0}, {0, 0}, 1/3, TauA]*
    ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {0, 0}, {1, -1/3}, 1/3, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
          qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     2*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     2*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     2*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((-6*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((-4*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, 0}, {1, (1 + TauA)/2}, {0, 0}, 1/3, TauA]*
    ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 - 
     2*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     3*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
         qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     7*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     ((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, 0}, {0, 0}, {1, 1/3}, 1/3, TauA]*
    ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     4*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, 0}, {1, 1/3}, {0, 0}, 1/3, TauA]*
    (-(5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]) + 
     5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     4*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, 1/6 + TauA/2}, 1/12 + TauA/4, TauA]*
    (-2*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
       TauA] - 3*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
       (1 - TauA)/4, TauA] - sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
      (1 - TauA)/4, TauA] - sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, 
      (1 - TauA)/4, TauA] - 4*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, 
       (1 - TauA)/4, TauA] + sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
      (1 - TauA)/4, TauA] + sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
       TauA]*((4*I)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        5^(1/4) - (9*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
              qr4}])/c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, 
            {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4]))/2 - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])) + 
   sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    (((-7*I)/3)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 - 
     (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         (3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/6 + 
     (((4*I)/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (3*(((4*I)/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    (((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 - 
     (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         (3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + (3*(((-4*I)/3)*Pi + (2*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (3*(((4*I)/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((-I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
    (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 - 
     (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    (((-2*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (3*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         (3*c4[qr1, qr2, qr3, qr4]) - 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]*
    (((-2*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]*
    (((-2*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (((-2*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     2*(((4*I)/3)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (3*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         (3*c4[qr1, qr2, qr3, qr4]) - 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + 2*(((-2*I)/3)*Pi + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (((-4*I)/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*5^(1/4)) + 2*(((2*I)/3)*Pi - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 4*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     2*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3) + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + 2*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)) + 
   sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    (((2*I)/3)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         (3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    (((2*I)/3)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         (3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]*
    (((2*I)/3)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]*
    (((2*I)/3)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]*
    (((2*I)/3)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - 3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA]*
    (((-2*I)/3)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    (((2*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
            qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + (7*(((-4*I)/3)*Pi + (2*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (5*(((4*I)/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA]*
    (((2*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     3*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (5*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
            qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 + (7*(((-4*I)/3)*Pi + (2*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
         3))/2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (5*(((4*I)/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, -1/3}, {1, 1/3}, 1/3, TauA]*
    (3*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (3*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]*
    (((4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
     4*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 2*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     2*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3) - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + 2*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)) + 
   sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    (((-8*I)/3)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     (((-4*I)/3)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3)/2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    (((-2*I)/3)*Pi - (5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/3 + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     4*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 2*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3) + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     2*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3) + 
     (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA]*
    (-1/3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      (3*c4[qr1, qr2, qr3, qr4]) + (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       GStar[qr1, qr2, qr3, qr4])/3 + 
     3*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 2*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (3*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, -1/3}, {1, -1/3}, 1/3, TauA]*((-2*I)*Pi + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     4*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     2*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3) + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     2*(((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3) + 
     (((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (((-4*I)/3)*Pi + (5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/3 + 
     3*(((4*I)/3)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
     (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/
      3 + (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]*
    (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
     3*(((4*I)/3)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + 5*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3) + (5*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (5*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*(((-4*I)/3)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
   sGt[{0, 0}, {1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]*
    ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {0, 0}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-2 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-2 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((6*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
         qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     (3*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
           qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
         qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     (3*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
           qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     2*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     (3*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {1, (1 + 3*TauA)/6}, {0, 0}, 1/3, TauA]*
    ((2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     ((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-1 - 3*TauA)/12}, {0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
     TauA]*((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {1, 0}, {0, 0}, 1/3, TauA]*((-2*I)*Pi + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {1, 0}, {0, 0}, 1/3, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, (-2 + 3*TauA)/6}, {0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, -1/3}, {1, -1/3}, 1/3, TauA]*
    ((6*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     6*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (7*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
          qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     4*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {1, -1/3}, {0, 0}, 1/3, TauA]*
    ((2*I)*Pi + 5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
     (5*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     ((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     4*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     3*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((2*I)*Pi - 5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
     (5*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (7*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, 0}, {0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((6*I)*Pi + 5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     8*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (7*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     2*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]*
    (-(EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4)) + 
     5*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (7*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     4*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, -1/3}, {0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     6*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (7*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (9*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     6*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (7*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (9*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, -1/3}, {0, 0}, {1, 1/3}, 1/3, TauA]*
    ((-2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     4*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (11*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (9*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {0, 0}, {1, 1/3}, 1/3, TauA]*
    ((-2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     4*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (11*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (9*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, -1/3}, {0, 0}, {1, -1/3}, 1/3, TauA]*
    ((4*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (11*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     4*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {0, 0}, {1, -1/3}, 1/3, TauA]*
    ((4*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (11*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     4*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, 0}, {0, 0}, {1, -1/3}, 1/3, TauA]*
    ((4*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     4*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (7*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, -1/3}, {0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-4*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (11*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-4*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     (11*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (9*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     (5*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (15*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     ((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*(-(EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((-2*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (5*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     4*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (13*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
          qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*(-(EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, 1/3}, 1/3, TauA]*
    ((-2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     8*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*((-4*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, -1/3}, {1, 1/3}, 1/3, TauA]*
    ((3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (9*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     6*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     2*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     ((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*((-4*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]*
    ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, TauA]*
    ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-1 + TauA)/4}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
    ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     2*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {1, (-1 + 3*TauA)/6}, {0, 0}, 1/3, TauA]*
    ((6*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     ((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4])/2 - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     2*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     2*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, -1/3}, {0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (1 + 3*TauA)/6}, {0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((2*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 - 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((-2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {0, 0}, {1, 1/3}, 1/3, TauA]*
    ((2*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (9*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (7*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     4*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     4*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (5*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA]*
    ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
     4*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) - 
     3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     (5*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
     2*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-10*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      GStar[qr1, qr2, qr3, qr4] + 
     (7*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + 
     3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     4*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (5*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{1, (-1 + 3*TauA)/6}, {0, 0}, {1, -1/3}, 1/3, TauA]*
    ((-4*I)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
     2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (7*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4]))/2 + 
     3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    ((8*Pi^2)/3 + (((4*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/(3*c4[qr1, qr2, qr3, qr4]^2) + ((8*I)/3)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 - (((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3)/5^(1/4) + 
     (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/5^(1/4) + 
     ((4*Pi^2)/3 + (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + ((4*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3)/2 + 
     ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3 - 
     (2*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) + 
     (5*((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     ((4*Pi^2)/3 + ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + ((2*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/
      2) + sGt[{0, 0}, {1, 1/3}, 1/3, TauA]*((4*Pi^2)/3 + 
     (((4*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/(3*c4[qr1, qr2, qr3, qr4]^2) + ((8*I)/3)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 + (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3)/5^(1/4) - 
     (3*(((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) + 
     (5*((4*Pi^2)/3 + (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] + ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) - 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3))/2 + 
     ((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
           qr4}])/c4[qr1, qr2, qr3, qr4] - ((4*I)/3)*Pi*
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (3*((-4*Pi^2)/3 - ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/
      2) + sGt[{0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    ((4*Pi^2)/3 + (((4*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/(3*c4[qr1, qr2, qr3, qr4]^2) + ((8*I)/3)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 + (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3)/5^(1/4) - 
     (3*(((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) + 
     (5*((4*Pi^2)/3 + (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] + ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) - 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3))/2 + 
     ((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
           qr4}])/c4[qr1, qr2, qr3, qr4] - ((4*I)/3)*Pi*
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (3*((-4*Pi^2)/3 - ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/
      2) + sGt[{0, 0}, {0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
    (-8*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
     4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 - ((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) + 
     ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (-4*Pi^2 - ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/2 - 
     (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*Z4[1, 1, {qr1, qr2, qr3, qr4}]^
       2 - (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) + 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{1, 0}, {0, 0}, {0, 0}, 1/3, TauA]*
    (-8*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
     4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 - ((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) + 
     ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (-4*Pi^2 - ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/2 - 
     (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*Z4[1, 1, {qr1, qr2, qr3, qr4}]^
       2 - (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) + 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
   sGt[{0, 0}, {0, 0}, {1, 1/3}, 1/3, TauA]*
    (-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
     4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 + ((4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) + 
     ((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) - 
     (3*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (3*((-4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) + 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*(-8*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - (8*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
        (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
        4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*(8*Pi^2 + (8*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2))/2 + 
     (3*(-4*Pi^2 - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2))/2 + 
     (8*Pi^2 + ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (-4*Pi^2 - ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*(8*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*(-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
    (-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
     4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 + ((4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) + 
     ((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) - 
     (3*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (3*((-4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) + 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*(-8*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - (8*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
        (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
        4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*(8*Pi^2 + (8*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2))/2 + 
     (3*(-4*Pi^2 - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2))/2 + 
     (8*Pi^2 + ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (-4*Pi^2 - ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (3*(8*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*(-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2) + 
   sGt[{0, 0}, {0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    (-4*Pi^2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/Sqrt[5] + 
     5^(3/4)*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4]) - 
     3*(-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/c4[qr1, qr2, qr3, qr4] + 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
         2) - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*Z4[1, 0, {qr1, qr2, qr3, qr4}]^
       2 - ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2) + 
   sGt[{0, 0}, {0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
    (-4*Pi^2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/Sqrt[5] + 
     5^(3/4)*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4]) - 
     3*(-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/c4[qr1, qr2, qr3, qr4] + 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
         2) - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*Z4[1, 0, {qr1, qr2, qr3, qr4}]^
       2 - ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2) + 
   sGt[{0, 0}, {0, 0}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    (-4*Pi^2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/Sqrt[5] + 
     5^(3/4)*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4]) - 
     3*(-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/c4[qr1, qr2, qr3, qr4] + 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
         2) - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*Z4[1, 0, {qr1, qr2, qr3, qr4}]^
       2 - ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
     (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2) + 
   sGt[{0, 0}, {0, 0}, {1, -1/3}, 1/3, TauA]*
    (8*Pi^2 + ((8*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      Sqrt[5] - (2*qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      c4[qr1, qr2, qr3, qr4]^2 + (16*I)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (8*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] - 
     8*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 + 5^(3/4)*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4]) + 
     ((-4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) - 
     3*(4*Pi^2 + ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4]^2 + (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/c4[qr1, qr2, qr3, qr4] - 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
         2) - (3*((4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (3*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) + 
     (5*(8*Pi^2 + ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (8*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*(-4*Pi^2 - ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     2*(4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]^2) + 
     (3*(-8*Pi^2 - (8*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2))/2 + 
     (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (-8*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (5*(4*Pi^2 + (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + (2*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*(-8*Pi^2 - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2) + 
   sGt[{0, 0}, {0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    (8*Pi^2 + ((8*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      Sqrt[5] - (2*qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      c4[qr1, qr2, qr3, qr4]^2 + (16*I)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (8*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] - 
     8*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 + 5^(3/4)*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4]) + 
     ((-4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        GStar[qr1, qr2, qr3, qr4])/5^(1/4) - 
     3*(4*Pi^2 + ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        c4[qr1, qr2, qr3, qr4]^2 + (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/c4[qr1, qr2, qr3, qr4] - 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
         2) - (3*((4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
     (3*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/5^(1/4) + 
     (5*(8*Pi^2 + ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + (8*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     (5*(-4*Pi^2 - ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] - (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
        (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
     2*(4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]^2) + 
     (3*(-8*Pi^2 - (8*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2))/2 + 
     (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (-8*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - (8*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
     (5*(4*Pi^2 + (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + (2*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (3*(-8*Pi^2 - (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - (4*I)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
     (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2) + 
   sGt[{1, -1/3 + TauA/2}, 1/3, TauA]*((-2*Pi^2)/9 - 
     EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2/(9*Sqrt[5]) + 
     5^(3/4)*((-1/9*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (18*c4[qr1, qr2, qr3, qr4]) + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4])/
        9) - 3*((-2*Pi^2)/9 - (((2*I)/9)*Pi*qr1*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (18*c4[qr1, qr2, qr3, qr4]^2) - ((4*I)/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/(9*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
          2)/9) - ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/18 - 
     ((I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/18)/5^(1/4) + 
     ((2*Pi^2)/9 + ((I/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + ((2*I)/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/(18*c4[qr1, qr2, qr3, qr4]) - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/9)/2 + 
     (2*((I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/18))/5^(1/4) - 
     (5*((2*Pi^2)/9 + ((I/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + ((2*I)/9)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/(18*c4[qr1, qr2, qr3, qr4]) - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/9))/2 + 
     ((-2*Pi^2)/9 - (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (I/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/18)/
      2 + ((2*Pi^2)/9 + ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/18)/2) + 
   sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
    ((2*Pi^2)/3 - (((8*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (3*Sqrt[5]) + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      c4[qr1, qr2, qr3, qr4]^2 - 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (3*5^(1/4)*c4[qr1, qr2, qr3, qr4]) - ((16*I)/3)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4])/
      (3*5^(1/4)) + (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
     4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
       2 + (3*(((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
         (3*c4[qr1, qr2, qr3, qr4]) - 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4])/3))/5^(1/4) + ((4*I)/3)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(6*c4[qr1, qr2, qr3, qr4]) + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3 - 
     (2*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) + 
     (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) - 
     (5*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(6*c4[qr1, qr2, qr3, qr4]) - 
     (5*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/3 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/6 + 
     sGt[{1, -1/3}, 1/12 + TauA/4, TauA]*((-4*I)*Pi + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (9*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/5^(1/4) + 
     ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3)/2) + 
   sGt[{0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
    ((4*Pi^2)/3 + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/(3*Sqrt[5]) + 
     5^(3/4)*(((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3) - 3*((4*Pi^2)/3 + (((4*I)/3)*Pi*qr1*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]^2) + ((8*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
          2)/3) + ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3 - 
     (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/5^(1/4) + 
     ((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
           qr4}])/c4[qr1, qr2, qr3, qr4] - ((4*I)/3)*Pi*
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (2*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) - 
     (5*((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     ((4*Pi^2)/3 + ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + ((2*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/
      2 + ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3)/2) + 
   sGt[{0, 0}, {1, (-2 + 3*TauA)/6}, 1/3, TauA]*
    ((4*Pi^2)/3 + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/(3*Sqrt[5]) + 
     5^(3/4)*(((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3) - 3*((4*Pi^2)/3 + (((4*I)/3)*Pi*qr1*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]^2) + ((8*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
          2)/3) + ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3 - 
     (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/5^(1/4) + 
     ((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
           qr4}])/c4[qr1, qr2, qr3, qr4] - ((4*I)/3)*Pi*
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (2*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) - 
     (5*((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2 + 
     ((4*Pi^2)/3 + ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + ((2*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/
      2 + ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3)/2) + 
   sGt[{0, 0}, {1, -1/3}, 1/3, TauA]*((-8*Pi^2)/3 - 
     (((4*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (3*Sqrt[5]) + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (3*c4[qr1, qr2, qr3, qr4]^2) - ((8*I)/3)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) + 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 + (((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3)/5^(1/4) + 
     (4*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
         (3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4])/3))/5^(1/4) - 
     2*((-4*Pi^2)/3 - (((4*I)/3)*Pi*qr1*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]^2) - ((8*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/(3*c4[qr1, qr2, qr3, qr4]) + 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
          2)/3) - ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     (3*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) + 
     (5*((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*((4*Pi^2)/3 + ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3))/2 + 
     ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3)/2 - 
     ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3 + 
     ((4*Pi^2)/3 + (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + ((4*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (3*((4*Pi^2)/3 + ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/
      2 + ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3)/2) + 
   sGt[{0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
    ((-8*Pi^2)/3 - (((4*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (3*Sqrt[5]) + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (3*c4[qr1, qr2, qr3, qr4]^2) - ((8*I)/3)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) + 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 + (((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/3)/5^(1/4) + 
     (4*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
         (3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4])/3))/5^(1/4) - 
     2*((-4*Pi^2)/3 - (((4*I)/3)*Pi*qr1*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (3*c4[qr1, qr2, qr3, qr4]^2) - ((8*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/(3*c4[qr1, qr2, qr3, qr4]) + 
       (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
          2)/3) - ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     (3*(((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/5^(1/4) + 
     (5*((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
        ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         GStar[qr1, qr2, qr3, qr4] - ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
        (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/3))/2 + 
     (3*((4*Pi^2)/3 + ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3))/2 + 
     ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3)/2 - 
     ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3 + 
     ((4*Pi^2)/3 + (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + ((4*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
     (3*((4*Pi^2)/3 + ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}] + ((2*I)/3)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/
      2 + ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3)/2) + 
   sGt[{1, 1/3}, 1/12 + TauA/4, TauA]*
    ((-3*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
        TauA])/2 - sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, 
      (1 - TauA)/4, TauA] - sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
       (1 - TauA)/4, TauA]/2 - sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, 
      (1 - TauA)/4, TauA] - (3*sGt[{1, (1 - 3*TauA)/12}, 
        {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA])/2 + 
     sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]/2 + 
     sGt[{1, 1/12 - TauA/4}, 1/4 - TauA/4, TauA]*
      (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3 + sGt[{1, -1/6 + TauA/2}, 1/3, TauA]/2 + 
       sGt[{1, -1/6 + TauA/2}, 1/12 + TauA/4, TauA]/2 - 
       sGt[{1, 1/6 + TauA/2}, 1/3, TauA]/2 - sGt[{1, 1/6 + TauA/2}, 
        1/12 + TauA/4, TauA] + sGt[{1, (1 + TauA)/2}, 1/12 + TauA/4, TauA]/
        2 + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/6 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/6) + 
     sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{1, 1/6 + TauA/2}, 1/3, TauA]*
    (Pi^2/3 + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2/(9*Sqrt[5]) + 
     (4*((I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
         (18*c4[qr1, qr2, qr3, qr4]) - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4])/
         9))/5^(1/4) - 2*((2*Pi^2)/9 + 
       (((2*I)/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - 
       (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (18*c4[qr1, qr2, qr3, qr4]^2) + ((4*I)/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
          qr4])/(9*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
          2)/9) - (5*sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 + 
     (5*sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, 1/3, TauA])/2 + 
     2*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] - 
     sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 + 
     sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]/2 - 
     2*sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
     sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]/2 - 
     (5*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 - sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]/
      2 - 2*sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, 
       TauA] - sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, 
       TauA]/2 + sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
       (1 - TauA)/4, TauA]/2 + (5*sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, 1/3, 
        TauA])/2 + 2*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA] + 
     ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/36 + 
     (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/18 + 
     ((2*Pi^2)/9 + ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/18)/2 + 
     sGt[{0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{1, (1 + TauA)/2}, 1/12 + TauA/4, TauA]*
    ((3*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 + sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, 
      TauA] + sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, 
       TauA]/2 + sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, 
      TauA] + (3*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, 
        (1 - TauA)/4, TauA])/2 - sGt[{1, (1 - 3*TauA)/12}, 
       {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]/2 + 
     sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{1, -1/6 + TauA/2}, 1/3, TauA]*((-5*Pi^2)/9 - 
     (((2*I)/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
        2)/(18*c4[qr1, qr2, qr3, qr4]^2) - ((4*I)/9)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(9*c4[qr1, qr2, qr3, qr4]) + 
     (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/9 - ((-1/9*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (18*c4[qr1, qr2, qr3, qr4]) + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4])/
        9)/5^(1/4) + (5*sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 - 
     (5*sGt[{1, -1/3}, {1, (-2 + 3*TauA)/6}, 1/3, TauA])/2 + 
     sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 - 
     sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]/2 + 
     2*sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] + 
     sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]/2 + 
     (5*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 + sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]/
      2 + 2*sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, 
       TauA] + sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, 
       TauA]/2 - sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, 
       (1 - TauA)/4, TauA]/2 - (5*sGt[{1, (-2 + 3*TauA)/6}, {1, -1/3}, 1/3, 
        TauA])/2 - (I/18)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     ((I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/18)/5^(1/4) + 
     ((-2*Pi^2)/9 - ((I/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - ((2*I)/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/(18*c4[qr1, qr2, qr3, qr4]) + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/9)/2 + 
     ((2*Pi^2)/9 + ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/18)/2 - 
     ((5*I)/18)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/36 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/18 - 
     (2*((I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/18))/5^(1/4) + 
     (5*((2*Pi^2)/9 + ((I/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
         c4[qr1, qr2, qr3, qr4] + ((2*I)/9)*Pi*EllipticPeriod[1, 
          {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
        (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
        (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/(18*c4[qr1, qr2, qr3, qr4]) - 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/9))/2 + 
     sGt[{0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{1, -1/12 - TauA/4}, 1/4 - TauA/4, TauA]*
    ((7*sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 + 
     sGt[{1, 1/6}, {1, -1/3}, (1 + 3*TauA)/12, TauA] + 
     sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 + 
     sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA] + 
     2*sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
     3*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] + 
     (3*sGt[{1, (1 + TauA)/2}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 - 
     (7*sGt[{1, (1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 + 
     sGt[{1, 1/6}, {0, 0}, (1 + 3*TauA)/12, TauA]*
      ((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     sGt[{0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{1, 1/12 - TauA/4}, 1/4 - TauA/4, TauA]*
    ((((-4*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (((4*I)/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2/
      (9*Sqrt[5]) - (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (6*c4[qr1, qr2, qr3, qr4]^2) + 
     (5^(3/4)*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (18*c4[qr1, qr2, qr3, qr4]) + ((8*I)/9)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/9 - 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
     (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 + sGt[{1, -1/3}, {1, -1/3}, 1/3, TauA] + 
     sGt[{1, -1/3}, {1, 1/3}, 1/3, TauA]/2 - 
     sGt[{1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA]/2 - 
     6*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
     (5*sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 - 
     (7*sGt[{1, 0}, {1, -1/3}, 1/3, TauA])/2 + 
     (3*sGt[{1, 0}, {1, 1/3}, 1/3, TauA])/2 - 
     (3*sGt[{1, 0}, {1, (1 + TauA)/2}, 1/3, TauA])/2 + 
     (7*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 - 
     sGt[{1, 1/3}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 - 
     sGt[{1, (1 + TauA)/2}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]/2 - 
     sGt[{1, (1 + TauA)/2}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]/2 + 
     sGt[{1, (1 + TauA)/2}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] - 
     (5*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 - 
     (3*sGt[{1, (-1 + 3*TauA)/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA])/2 + 
     sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA] - 
     sGt[{1, (-1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] + 
     (5*sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 - 
     sGt[{1, (1 + 3*TauA)/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 + 
     (3*sGt[{1, (1 + 3*TauA)/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA])/
      2 + (5*sGt[{1, (1 + 3*TauA)/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, 
        TauA])/2 - 4*sGt[{1, (1 + 3*TauA)/6}, {1, (1 + 3*TauA)/6}, 
       (1 + 3*TauA)/12, TauA] - ((2*I)/9)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(18*5^(1/4)) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(36*c4[qr1, qr2, qr3, qr4]) - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/18 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/18 + 
     sGt[{0, 0}, {1, 1/3}, 1/3, TauA]*
      ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
      (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
          qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) - 
     ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(9*5^(1/4)) + 
     (5*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(36*c4[qr1, qr2, qr3, qr4]) + 
     (5*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/18 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/36 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/36 + 
     sGt[{1, -1/6 + TauA/2}, 1/3, TauA]*(((-4*I)/3)*Pi - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (2*c4[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + 2*sGt[{1, -1/6 + TauA/2}, 1/12 + TauA/4, 
         TauA] + sGt[{1, 1/6 + TauA/2}, 1/12 + TauA/4, TauA]/2 - 
       sGt[{1, (1 + TauA)/2}, 1/12 + TauA/4, TauA]/2 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/6) + 
     sGt[{1, 1/6 + TauA/2}, 1/3, TauA]*(((4*I)/3)*Pi + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (2*c4[qr1, qr2, qr3, qr4]) - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - sGt[{1, 1/6 + TauA/2}, 1/12 + TauA/4, 
         TauA]/2 + sGt[{1, (1 + TauA)/2}, 1/12 + TauA/4, TauA]/2 - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/6) + 
     sGt[{1, -1/6 + TauA/2}, 1/12 + TauA/4, TauA]*(((-4*I)/3)*Pi + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
       2*(((4*I)/3)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          (3*c4[qr1, qr2, qr3, qr4]) - 
         (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, 
            qr4])/3) + 2*(((-2*I)/3)*Pi + 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          (3*c4[qr1, qr2, qr3, qr4]) + 
         (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, 
            qr4])/3) - 2*sGt[{1, 1/6 + TauA/2}, 1/3, TauA] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3) + sGt[{1, 0}, {0, 0}, 1/3, TauA]*
      ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, -1/3}, 1/3, TauA]*((-2*I)*Pi + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
            qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
       2*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
            qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, (1 + TauA)/2}, 1/12 + TauA/4, TauA]*(((-2*I)/3)*Pi - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3 + (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
       (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
     sGt[{1, 1/6 + TauA/2}, 1/12 + TauA/4, TauA]*(((-2*I)/3)*Pi + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3 + 2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          (3*c4[qr1, qr2, qr3, qr4]) - 
         (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, 
            qr4])/3) + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
       (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3 + 
       (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2) + 
     sGt[{0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
      ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) + 
       2*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
       (3*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
      ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{1, -1/6 + TauA/2}, 1/12 + TauA/4, TauA]*
    (sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]/2 + 
     2*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA] + 
     sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]/2 + 
     (5*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 - sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]/
      2 + sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2)) + 
   sGt[{0, 0}, 1/4 - TauA/4, TauA]*
    (sGt[{1, (1 + TauA)/2}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]*
      ((4*I)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (9*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       3*((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     sGt[{1, 1/6}, {1, -1/3}, (1 + 3*TauA)/12, TauA]*
      ((-4*I)*Pi + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (9*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       3*((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}]) + 
     sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]*
      ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, (1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (3*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
       2*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]*
      ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
           qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
       (3*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
             qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA]*
      ((2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, (-1 + 3*TauA)/6}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       2*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      (-4*Pi^2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/Sqrt[5] + 
       5^(3/4)*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/c4[qr1, qr2, qr3, 
           qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          GStar[qr1, qr2, qr3, qr4]) - 
       3*(-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 
         (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
          c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
         (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
         4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          GStar[qr1, qr2, qr3, qr4]^2) - (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]^2 - 
       ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
         (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
         2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
       (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
          EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
       (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
            {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
          (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
          (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
            Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
          2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
            qr4]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
       (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2)) + 
   sGt[{1, -1/3}, 1/3, TauA]*(Pi^2/9 + (I/9)*5^(3/4)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
     (((4*I)/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2/
      (9*Sqrt[5]) + (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (9*c4[qr1, qr2, qr3, qr4]^2) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (6*5^(1/4)*c4[qr1, qr2, qr3, qr4]) - ((8*I)/9)*Pi*
      EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4])/
      (3*5^(1/4)) + (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(9*c4[qr1, qr2, qr3, qr4]) + 
     (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/9 + ((I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
        (18*c4[qr1, qr2, qr3, qr4]) - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4])/
        9)/5^(1/4) + (sGt[{1, 1/3}, 1/12 + TauA/4, TauA]*
       sGt[{1, 1/12 - TauA/4}, 1/4 - TauA/4, TauA])/2 + 
     (3*sGt[{1, -1/3}, {1, 1/6}, (1 + 3*TauA)/12, TauA])/2 + 
     sGt[{1, 1/6}, {1, 1/3}, (1 + 3*TauA)/12, TauA]/2 - 
     sGt[{1, 1/6}, {1, (1 + TauA)/2}, (1 + 3*TauA)/12, TauA]/2 + 
     sGt[{1, 1/6}, {1, (-1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA] + 
     (3*sGt[{1, 1/6}, {1, (1 + 3*TauA)/6}, (1 + 3*TauA)/12, TauA])/2 + 
     (3*sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 + sGt[{1, (1 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]/
      2 + sGt[{1, (1 - 3*TauA)/12}, {1, (-1 + TauA)/4}, (1 - TauA)/4, TauA] + 
     (3*sGt[{1, (1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 - sGt[{1, (1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]/
      2 + (I/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(9*5^(1/4)) + 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(36*c4[qr1, qr2, qr3, qr4]) + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/18 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/18 + 
     (2*((-1/9*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
        (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/18))/5^(1/4) + 
     ((2*Pi^2)/9 + ((I/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + ((2*I)/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/(18*c4[qr1, qr2, qr3, qr4]) - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/9)/2 + 
     ((-2*Pi^2)/9 - ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/18)/2 + 
     ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(36*c4[qr1, qr2, qr3, qr4]) - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/18 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/12 + 
     sGt[{1, 1/12 - TauA/4}, 1/4 - TauA/4, TauA]*
      ((I/3)*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           (3*c4[qr1, qr2, qr3, qr4]) + 
          (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, 
             qr4])/3))/2 + sGt[{1, -1/6 + TauA/2}, 1/12 + TauA/4, TauA] + 
       (3*sGt[{1, 1/6 + TauA/2}, 1/12 + TauA/4, TauA])/2 - 
       sGt[{1, (1 + TauA)/2}, 1/12 + TauA/4, TauA]/2 - 
       (5*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/6 - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3) + 
     ((-2*Pi^2)/9 - ((I/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] - ((2*I)/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(18*c4[qr1, qr2, qr3, qr4]) + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/9)/2 + 
     ((-2*Pi^2)/9 - (I/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - (I/9)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/18)/
      2 + ((2*Pi^2)/9 + ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/18)/2 + 
     sGt[{0, 0}, {1, 1/6}, (1 + 3*TauA)/12, TauA]*
      ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((-2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, 1/6}, 1/12 + TauA/4, TauA]*
      (I*Pi - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (2*c4[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + (3*sGt[{1, -5/12 - TauA/4}, 1/4 - TauA/4, 
          TauA])/2 - (5*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/6 - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/6 + 
       (((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 1, {qr1, qr2, qr3, qr4}])/3)/2 + 
       sGt[{0, 0}, 1/4 - TauA/4, TauA]*((-2*I)*Pi - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
         (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
             c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, 
               qr4}]*GStar[qr1, qr2, qr3, qr4]))/2 + 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, 
           {qr1, qr2, qr3, qr4}] + ((2*I)*Pi - EllipticPeriod[1, 
             {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2))) + 
   sGt[{1, 1/6}, 1/12 + TauA/4, TauA]*
    ((((-4*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
     (((4*I)/9)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
      c4[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2/
      (9*Sqrt[5]) - (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (6*c4[qr1, qr2, qr3, qr4]^2) + 
     (5^(3/4)*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
      (18*c4[qr1, qr2, qr3, qr4]) + ((8*I)/9)*Pi*EllipticPeriod[1, 
       {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
     (5^(3/4)*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/9 - 
     (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
     (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]^
        2)/3 + sGt[{1, 1/3}, 1/12 + TauA/4, TauA]*
      (-1/2*sGt[{1, -5/12 - TauA/4}, 1/4 - TauA/4, TauA] - 
       (3*sGt[{1, -1/12 - TauA/4}, 1/4 - TauA/4, TauA])/2) + 
     sGt[{1, -1/3}, {1, -1/3}, 1/3, TauA] + 
     sGt[{1, -1/3}, {1, 1/3}, 1/3, TauA]/2 - 
     sGt[{1, -1/3}, {1, (1 + TauA)/2}, 1/3, TauA]/2 - 
     6*sGt[{1, -1/3}, {1, (-1 + 3*TauA)/6}, 1/3, TauA] + 
     (5*sGt[{1, -1/3}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 - 
     (7*sGt[{1, 0}, {1, -1/3}, 1/3, TauA])/2 + 
     (3*sGt[{1, 0}, {1, 1/3}, 1/3, TauA])/2 - 
     (3*sGt[{1, 0}, {1, (1 + TauA)/2}, 1/3, TauA])/2 + 
     (7*sGt[{1, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA])/2 - 
     4*sGt[{1, (-5 - 3*TauA)/12}, {1, (-5 - 3*TauA)/12}, (1 - TauA)/4, 
       TauA] - sGt[{1, (-5 - 3*TauA)/12}, {1, (1 - TauA)/4}, (1 - TauA)/4, 
       TauA]/2 + (7*sGt[{1, (-5 - 3*TauA)/12}, {1, (-1 + TauA)/4}, 
        (1 - TauA)/4, TauA])/2 + sGt[{1, (-5 - 3*TauA)/12}, 
       {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]/2 + 
     (7*sGt[{1, (-1 - 3*TauA)/12}, {1, (-5 - 3*TauA)/12}, (1 - TauA)/4, 
        TauA])/2 - (3*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 - TauA)/4}, 
        (1 - TauA)/4, TauA])/2 - 
     (7*sGt[{1, (-1 - 3*TauA)/12}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA])/
      2 + (3*sGt[{1, (-1 - 3*TauA)/12}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, 
        TauA])/2 - (5*sGt[{1, (-1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 + 
     (5*sGt[{1, (1 + 3*TauA)/6}, {1, -1/3}, 1/3, TauA])/2 - 
     ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(18*5^(1/4)) - 
     (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/(36*c4[qr1, qr2, qr3, qr4]) - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 0, {qr1, qr2, qr3, qr4}])/18 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/18 + 
     sGt[{0, 0}, {1, 1/3}, 1/3, TauA]*
      ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (5 + 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] + 
       2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 - TauA)/4}, (1 - TauA)/4, TauA]*
      (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
          qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 + TauA)/2}, 1/3, TauA]*
      (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
          qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) - 
     ((2*I)/9)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
      Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(9*5^(1/4)) + 
     (5*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/(36*c4[qr1, qr2, qr3, qr4]) + 
     (5*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
       Z4[1, 1, {qr1, qr2, qr3, qr4}])/18 + 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/36 - 
     (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
       Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/36 + 
     sGt[{1, -1/6 + TauA/2}, 1/3, TauA]*(((-4*I)/3)*Pi - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) + 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (2*c4[qr1, qr2, qr3, qr4]) + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        GStar[qr1, qr2, qr3, qr4] + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/6) + 
     sGt[{1, 1/6 + TauA/2}, 1/3, TauA]*((I/3)*Pi + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/(3*5^(1/4)) - 
       (3*(((-2*I)/3)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           (3*c4[qr1, qr2, qr3, qr4]) + 
          (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, 
             qr4])/3))/2 - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/6) + sGt[{1, 0}, {0, 0}, 1/3, TauA]*
      ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, -1/3}, 1/3, TauA]*((-2*I)*Pi + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
            qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
       2*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
            qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 + 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 - 
       (3*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 + 
       2*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, -5/12 - TauA/4}, 1/4 - TauA/4, TauA]*
      (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        (3*c4[qr1, qr2, qr3, qr4]) - 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4])/
        3 + 2*(((2*I)/3)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          (3*c4[qr1, qr2, qr3, qr4]) - 
         (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, 
            qr4])/3) + (5*sGt[{1, -1/6 + TauA/2}, 1/3, TauA])/2 - 
       (5*sGt[{1, 1/6 + TauA/2}, 1/3, TauA])/2 + 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
         Z4[1, 0, {qr1, qr2, qr3, qr4}])/3 + 
       (((-2*I)/3)*Pi + (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 - 
       (3*(((2*I)/3)*Pi - (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
            Z4[1, 1, {qr1, qr2, qr3, qr4}])/3))/2) + 
     sGt[{0, 0}, {1, (-5 - 3*TauA)/12}, (1 - TauA)/4, TauA]*
      ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
           qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
       (3*(-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
             qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (1 + 3*TauA)/6}, 1/3, TauA]*
      ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) + 
       2*((4*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]) - 
       (3*((-4*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4] + EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}]))/2 - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{1, (-1 - 3*TauA)/12}, {0, 0}, (1 - TauA)/4, TauA]*
      ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (-1 + 3*TauA)/6}, 1/3, TauA]*
      ((2*I)*Pi - (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) - 
       (3*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((4*I)*Pi - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       ((-2*I)*Pi + EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {1, (-1 + TauA)/4}, (1 - TauA)/4, TauA]*
      ((-2*I)*Pi + (3*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       2*((4*I)*Pi - (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] - 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       2*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4]) - 
       (3*((-4*I)*Pi + (2*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + 4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           GStar[qr1, qr2, qr3, qr4]))/2 + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       ((2*I)*Pi - EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       ((-4*I)*Pi + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
     sGt[{0, 0}, {0, 0}, (1 - TauA)/4, TauA]*
      (-4*Pi^2 - (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/Sqrt[5] + 
       5^(3/4)*((-2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/c4[qr1, qr2, qr3, 
           qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          GStar[qr1, qr2, qr3, qr4]) - 
       3*(-4*Pi^2 - ((4*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + 
         (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
          c4[qr1, qr2, qr3, qr4]^2 - (8*I)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
         (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           GStar[qr1, qr2, qr3, qr4])/c4[qr1, qr2, qr3, qr4] + 
         4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          GStar[qr1, qr2, qr3, qr4]^2) - (4*I)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
       EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
        Z4[1, 0, {qr1, qr2, qr3, qr4}]^2 - 
       ((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       (4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
          c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
         (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
         2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
           qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/2 + 
       (2*((2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
          EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 1, {qr1, qr2, qr3, qr4}]))/5^(1/4) - 
       (5*(4*Pi^2 + ((2*I)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
           c4[qr1, qr2, qr3, qr4] + (4*I)*Pi*EllipticPeriod[1, 
            {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
          (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
           Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
          (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
            Z4[1, 1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
          2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
            qr4]*Z4[1, 1, {qr1, qr2, qr3, qr4}]))/2 + 
       (-4*Pi^2 - (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}] - (2*I)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2 + 
       (4*Pi^2 + (4*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}] - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
          Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/2) + 
     sGt[{0, 0}, 1/4 - TauA/4, TauA]*((16*Pi^2)/3 - 
       (((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/5^(1/4) + 
       (((5*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
        c4[qr1, qr2, qr3, qr4] + (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^
          2)/(3*Sqrt[5]) + ((10*I)/3)*Pi*EllipticPeriod[1, 
         {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] + 
       5^(3/4)*(((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] - 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
          (3*c4[qr1, qr2, qr3, qr4]) - 
         (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
            qr4])/3) - 3*((4*Pi^2)/3 + (((4*I)/3)*Pi*qr1*EllipticPeriod[1, 
            {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, qr4] - 
         (qr1^2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2)/
          (3*c4[qr1, qr2, qr3, qr4]^2) + ((8*I)/3)*Pi*EllipticPeriod[1, 
           {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
         (4*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           GStar[qr1, qr2, qr3, qr4])/(3*c4[qr1, qr2, qr3, qr4]) - 
         (4*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           GStar[qr1, qr2, qr3, qr4]^2)/3) + ((5*I)/3)*Pi*
        EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 0, {qr1, qr2, qr3, qr4}] - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]^2)/3 - 
       (((-2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}] + 
         (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/5^(1/4) + 
       ((-4*Pi^2)/3 - (((2*I)/3)*Pi*qr1*EllipticPeriod[1, {qr1, qr2, qr3, 
             qr4}])/c4[qr1, qr2, qr3, qr4] - ((4*I)/3)*Pi*
          EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*GStar[qr1, qr2, qr3, qr4] - 
         ((2*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 0, {qr1, qr2, qr3, qr4}] + 
         (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 0, {qr1, qr2, qr3, qr4}])/(3*c4[qr1, qr2, qr3, qr4]) + 
         (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, 
            qr4]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/3)/2 + 
       sGt[{1, 1/3}, 1/12 + TauA/4, TauA]*
        (-((qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/c4[qr1, qr2, qr3, 
            qr4]) - 2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          GStar[qr1, qr2, qr3, qr4] - 
         (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
             c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, 
               qr4}]*GStar[qr1, qr2, qr3, qr4]))/2 + 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, 
           {qr1, qr2, qr3, qr4}] + ((-2*I)*Pi + EllipticPeriod[1, 
             {qr1, qr2, qr3, qr4}]*Z4[1, 0, {qr1, qr2, qr3, qr4}])/2) + 
       (2*I)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
        Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
       (2*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(3*5^(1/4)) - 
       (5*qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/(6*c4[qr1, qr2, qr3, qr4]) - 
       (5*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*GStar[qr1, qr2, qr3, qr4]*
         Z4[1, 1, {qr1, qr2, qr3, qr4}])/3 - 
       (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
         Z4[1, 0, {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/6 + 
       ((-4*Pi^2)/3 - ((4*I)/3)*Pi*EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*
          Z4[1, 1, {qr1, qr2, qr3, qr4}] + 
         (EllipticPeriod[1, {qr1, qr2, qr3, qr4}]^2*
           Z4[1, 1, {qr1, qr2, qr3, qr4}]^2)/3)/2 + 
       sGt[{1, 1/6 + TauA/2}, 1/3, TauA]*((-2*I)*Pi - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
         (3*((2*I)*Pi - (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
             c4[qr1, qr2, qr3, qr4] - 2*EllipticPeriod[1, {qr1, qr2, qr3, 
               qr4}]*GStar[qr1, qr2, qr3, qr4]))/2 + 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, 
           {qr1, qr2, qr3, qr4}] + ((2*I)*Pi - EllipticPeriod[1, 
             {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2) + 
       sGt[{1, -1/6 + TauA/2}, 1/3, TauA]*((2*I)*Pi + 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]/5^(1/4) - 
         (3*((-2*I)*Pi + (qr1*EllipticPeriod[1, {qr1, qr2, qr3, qr4}])/
             c4[qr1, qr2, qr3, qr4] + 2*EllipticPeriod[1, {qr1, qr2, qr3, 
               qr4}]*GStar[qr1, qr2, qr3, qr4]))/2 - 
         EllipticPeriod[1, {qr1, qr2, qr3, qr4}]*Z4[1, 0, 
           {qr1, qr2, qr3, qr4}] + ((-2*I)*Pi + EllipticPeriod[1, 
             {qr1, qr2, qr3, qr4}]*Z4[1, 1, {qr1, qr2, qr3, qr4}])/2))) - 
   (9*Zeta[3])/4}
