This folder contains a number of files FI_k.m, which give replacements lists for the coefficient k of the epsilon expansion of integral I in terms of elliptic functions.
## Functions description  
* G[..]: PolyLogTools functions for the Multiple Polylogarithmic functions.
* sGt[{n_1,z_1},...,{n_k,z_k},z,tau]: these functions represent the eMPLs as defined in Eq. (3.2) of the paper. In our case we have two different tau, "TauA" and "TauB", which indicates respectively the elliptic curves "a" and "b". Their definitions, and numerical values, are given in Eq. (3.19).
* IEI[{n_1,N_1,r_1,s_,1},...,{n_k,N_k,r_k,s_k},tau]: these functions represent the Iterated integrals of Eisenstein series defined in Eq. (3.4) of the paper. For a detailed understanding of these functions, and the relative notation, we guide the reader to refs. [6,9], and [61-64] of our paper. As for the eMPLs functions we have two different tau, "TauA" and "TauB".
* EisensteinH[n, N, r, s, TauA]; these functions represent the Eisenstein series kernels (see, for example, Eq. (8.5) of ref. [62]).
* Z4[n_,x,{qr1,qr2,qr3,qr4}]: these functions represent the Z4 object related to an elliptic curve as defined, for example, in ref. [6] (Eq. (7.25)-(7.26)) of our paper.
* GStar[qr1,qr2,qr3,qr4]: this function represents the GStar object related to an elliptic curve as defined, for example, in ref. [36] (Eq. (2.28)) of our paper.
* EllipticPeriod[1, {qr1, qr2, qr3, qr4}]: this function represents the first elliptic period for the curve "a". See, as reference Eq. (7.3) of ref. [6] in our paper. 
* EllipticPeriod[1, {a1, a2, a3, a4}]: this function represents the first elliptic period for the curve "b".
* c4[qr1, qr2, qr3, qr4]: this function represents the quantity defined in Eq. (7.4) of ref. [6] of our paper.
* {qr1,qr2,qr3,qr4}: they are the branch-points for the elliptic curve "a". Their values are: {qr1 = 1/2*(1-sqrt[1+2*I]),qr2 = 1/2*(1-sqrt[1-2*I]),qr3 = 1/2*(1+sqrt[1+2*I]),qr4 = 1/2*(1+sqrt[1-2*I])}.
* {a1,a2,a3,a4}: they are the branch-points for the elliptic curve "b". Their values are: {a1 = 1 - sqrt[5],a2 = 0,a3 = 2,a4 = 1 + sqrt[5]}.
* Zi: this quantity, with i=1,...,6,b, are defined in the Appendix B of the paper, see Eqs. (B.4) and (B.8). 
