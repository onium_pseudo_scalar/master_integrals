(* Created with the Wolfram Language : www.wolfram.com *)
{F[24, 0] -> -((EllipticPeriod[1, {a1, a2, a3, a4}]*
      (5*Pi^4*IEI[{0, 0, 0, 0}, {3, 4, 1, 2}, TauB] - 
       160*Catalan*Pi*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, TauB] - 
       320*Catalan*Pi*IEI[{2, 4, 1, 0}, {2, 4, 1, 1}, TauB] - 
       160*Catalan*Pi*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, TauB] - 
       320*Catalan*Pi*IEI[{2, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
       80*Catalan*Pi*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, TauB] - 
       160*Catalan*Pi*IEI[{2, 4, 1, 2}, {2, 4, 1, 1}, TauB] - 
       80*Catalan*Pi*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, TauB] - 
       160*Catalan*Pi*IEI[{2, 4, 1, 2}, {2, 4, 3, 1}, TauB] - 
       12*Pi^2*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 0}, TauB] - 
       24*Pi^2*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 1}, TauB] - 
       12*Pi^2*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 2}, TauB] - 
       24*Pi^2*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
       160*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {2, 4, 1, 0}, {3, 4, 1, 0}, 
         TauB] + 640*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {2, 4, 1, 0}, 
         {3, 4, 1, 2}, TauB] - 320*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, 
         {2, 4, 1, 1}, {3, 4, 1, 0}, TauB] - 
       160*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {2, 4, 1, 2}, {3, 4, 1, 0}, 
         TauB] + 320*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {2, 4, 1, 2}, 
         {3, 4, 1, 2}, TauB] - 320*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, 
         {2, 4, 3, 1}, {3, 4, 1, 0}, TauB] + 
       96*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {3, 4, 1, 0}, {2, 4, 1, 0}, TauB] + 
       192*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {3, 4, 1, 0}, {2, 4, 1, 1}, 
         TauB] + 96*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, {3, 4, 1, 0}, 
         {2, 4, 1, 2}, TauB] + 192*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, 
         {3, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
       80*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {2, 4, 1, 0}, {3, 4, 1, 0}, TauB] + 
       320*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {2, 4, 1, 0}, {3, 4, 1, 2}, 
         TauB] - 160*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {2, 4, 1, 1}, 
         {3, 4, 1, 0}, TauB] - 80*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, 
         {2, 4, 1, 2}, {3, 4, 1, 0}, TauB] + 
       160*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {2, 4, 1, 2}, {3, 4, 1, 2}, 
         TauB] - 160*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {2, 4, 3, 1}, 
         {3, 4, 1, 0}, TauB] + 48*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, 
         {3, 4, 1, 0}, {2, 4, 1, 0}, TauB] + 
       96*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {3, 4, 1, 0}, {2, 4, 1, 1}, TauB] + 
       48*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {3, 4, 1, 0}, {2, 4, 1, 2}, TauB] + 
       96*IEI[{0, 0, 0, 0}, {2, 4, 1, 2}, {3, 4, 1, 0}, {2, 4, 3, 1}, TauB] + 
       96*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 0}, {2, 4, 1, 0}, TauB] + 
       192*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 0}, {2, 4, 1, 1}, 
         TauB] + 96*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 0}, 
         {2, 4, 1, 2}, TauB] + 192*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, 
         {2, 4, 1, 0}, {2, 4, 3, 1}, TauB] + 
       48*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 2}, {2, 4, 1, 0}, TauB] + 
       96*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 2}, {2, 4, 1, 1}, TauB] + 
       48*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 2}, {2, 4, 1, 2}, TauB] + 
       96*IEI[{0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 2}, {2, 4, 3, 1}, TauB] - 
       160*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {2, 4, 1, 0}, {3, 4, 1, 0}, 
         TauB] + 640*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {2, 4, 1, 0}, 
         {3, 4, 1, 2}, TauB] - 320*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, 
         {2, 4, 1, 1}, {3, 4, 1, 0}, TauB] - 
       160*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {2, 4, 1, 2}, {3, 4, 1, 0}, 
         TauB] + 320*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {2, 4, 1, 2}, 
         {3, 4, 1, 2}, TauB] - 320*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, 
         {2, 4, 3, 1}, {3, 4, 1, 0}, TauB] + 
       96*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 0}, TauB] + 
       192*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 1}, 
         TauB] + 96*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 0}, 
         {2, 4, 1, 2}, TauB] + 192*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, 
         {3, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
       160*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 0}, 
         TauB] + 640*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, {0, 0, 0, 0}, 
         {3, 4, 1, 2}, TauB] - 320*IEI[{2, 4, 1, 0}, {2, 4, 1, 1}, 
         {0, 0, 0, 0}, {3, 4, 1, 0}, TauB] - 
       160*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, {0, 0, 0, 0}, {3, 4, 1, 0}, 
         TauB] + 320*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, {0, 0, 0, 0}, 
         {3, 4, 1, 2}, TauB] - 320*IEI[{2, 4, 1, 0}, {2, 4, 3, 1}, 
         {0, 0, 0, 0}, {3, 4, 1, 0}, TauB] - 
       80*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {2, 4, 1, 0}, {3, 4, 1, 0}, TauB] + 
       320*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {2, 4, 1, 0}, {3, 4, 1, 2}, 
         TauB] - 160*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {2, 4, 1, 1}, 
         {3, 4, 1, 0}, TauB] - 80*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, 
         {2, 4, 1, 2}, {3, 4, 1, 0}, TauB] + 
       160*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {2, 4, 1, 2}, {3, 4, 1, 2}, 
         TauB] - 160*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {2, 4, 3, 1}, 
         {3, 4, 1, 0}, TauB] + 48*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, 
         {3, 4, 1, 0}, {2, 4, 1, 0}, TauB] + 
       96*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 1}, TauB] + 
       48*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 1, 2}, TauB] + 
       96*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {3, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
       80*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 0}, TauB] + 
       320*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 2}, 
         TauB] - 160*IEI[{2, 4, 1, 2}, {2, 4, 1, 1}, {0, 0, 0, 0}, 
         {3, 4, 1, 0}, TauB] - 80*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, 
         {0, 0, 0, 0}, {3, 4, 1, 0}, TauB] + 
       160*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, {0, 0, 0, 0}, {3, 4, 1, 2}, 
         TauB] - 160*IEI[{2, 4, 1, 2}, {2, 4, 3, 1}, {0, 0, 0, 0}, 
         {3, 4, 1, 0}, TauB] - (12*I)*Pi^3*IEI[{0, 0, 0, 0}, {3, 4, 1, 2}, 
         TauB]*Log[2] + (96*I)*Pi*IEI[{0, 0, 0, 0}, {2, 4, 1, 0}, 
         {3, 4, 1, 2}, TauB]*Log[2] + (48*I)*Pi*IEI[{0, 0, 0, 0}, 
         {2, 4, 1, 2}, {3, 4, 1, 2}, TauB]*Log[2] + 
       (96*I)*Pi*IEI[{2, 4, 1, 0}, {0, 0, 0, 0}, {3, 4, 1, 2}, TauB]*Log[2] + 
       (48*I)*Pi*IEI[{2, 4, 1, 2}, {0, 0, 0, 0}, {3, 4, 1, 2}, TauB]*Log[2] + 
       12*Pi^2*IEI[{0, 0, 0, 0}, {3, 4, 1, 2}, TauB]*Log[2]^2))/
     ((1 + Sqrt[5])*Pi^4)) + (12*EllipticPeriod[1, {a1, a2, a3, a4}]*
     IEI[{0, 0, 0, 0}, {3, 4, 1, 2}, TauB]*sGt[{1, -1/4}, Zb, TauB]^2)/
    ((1 + Sqrt[5])*Pi^2) + (12*EllipticPeriod[1, {a1, a2, a3, a4}]*
     IEI[{0, 0, 0, 0}, {3, 4, 1, 2}, TauB]*sGt[{1, 1/4}, Zb, TauB]^2)/
    ((1 + Sqrt[5])*Pi^2) - (12*EllipticPeriod[1, {a1, a2, a3, a4}]*
     (Pi^2*IEI[{2, 4, 1, 0}, TauB] + 2*Pi^2*IEI[{2, 4, 1, 1}, TauB] + 
      Pi^2*IEI[{2, 4, 1, 2}, TauB] + 2*Pi^2*IEI[{2, 4, 3, 1}, TauB] - 
      8*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, TauB] - 
      16*IEI[{2, 4, 1, 0}, {2, 4, 1, 1}, TauB] - 
      8*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, TauB] - 
      16*IEI[{2, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
      4*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, TauB] - 
      8*IEI[{2, 4, 1, 2}, {2, 4, 1, 1}, TauB] - 
      4*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, TauB] - 
      8*IEI[{2, 4, 1, 2}, {2, 4, 3, 1}, TauB])*sGt[{0, 0}, {1, -1/4}, Zb, 
      TauB])/((1 + Sqrt[5])*Pi^2) + (12*EllipticPeriod[1, {a1, a2, a3, a4}]*
     (Pi^2*IEI[{2, 4, 1, 0}, TauB] + 2*Pi^2*IEI[{2, 4, 1, 1}, TauB] + 
      Pi^2*IEI[{2, 4, 1, 2}, TauB] + 2*Pi^2*IEI[{2, 4, 3, 1}, TauB] - 
      8*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, TauB] - 
      16*IEI[{2, 4, 1, 0}, {2, 4, 1, 1}, TauB] - 
      8*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, TauB] - 
      16*IEI[{2, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
      4*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, TauB] - 
      8*IEI[{2, 4, 1, 2}, {2, 4, 1, 1}, TauB] - 
      4*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, TauB] - 
      8*IEI[{2, 4, 1, 2}, {2, 4, 3, 1}, TauB])*sGt[{0, 0}, {1, 1/4}, Zb, 
      TauB])/((1 + Sqrt[5])*Pi^2) - 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, -1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, -1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, 1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, 1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, TauB/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, TauB/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, (1 + TauB)/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
     sGt[{0, 0}, {1, (1 + TauB)/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   sGt[{1, 1/4}, Zb, TauB]*((-12*Zb*EllipticPeriod[1, {a1, a2, a3, a4}]*
       (Pi^2*IEI[{2, 4, 1, 0}, TauB] + 2*Pi^2*IEI[{2, 4, 1, 1}, TauB] + 
        Pi^2*IEI[{2, 4, 1, 2}, TauB] + 2*Pi^2*IEI[{2, 4, 3, 1}, TauB] - 
        8*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, TauB] - 
        16*IEI[{2, 4, 1, 0}, {2, 4, 1, 1}, TauB] - 
        8*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, TauB] - 
        16*IEI[{2, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
        4*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, TauB] - 
        8*IEI[{2, 4, 1, 2}, {2, 4, 1, 1}, TauB] - 
        4*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, TauB] - 
        8*IEI[{2, 4, 1, 2}, {2, 4, 3, 1}, TauB]))/((1 + Sqrt[5])*Pi^2) - 
     ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
       sGt[{0, 0}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
       sGt[{0, 0}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
        {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
        {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5])) + 
   sGt[{1, -1/4}, Zb, TauB]*((12*Zb*EllipticPeriod[1, {a1, a2, a3, a4}]*
       (Pi^2*IEI[{2, 4, 1, 0}, TauB] + 2*Pi^2*IEI[{2, 4, 1, 1}, TauB] + 
        Pi^2*IEI[{2, 4, 1, 2}, TauB] + 2*Pi^2*IEI[{2, 4, 3, 1}, TauB] - 
        8*IEI[{2, 4, 1, 0}, {2, 4, 1, 0}, TauB] - 
        16*IEI[{2, 4, 1, 0}, {2, 4, 1, 1}, TauB] - 
        8*IEI[{2, 4, 1, 0}, {2, 4, 1, 2}, TauB] - 
        16*IEI[{2, 4, 1, 0}, {2, 4, 3, 1}, TauB] - 
        4*IEI[{2, 4, 1, 2}, {2, 4, 1, 0}, TauB] - 
        8*IEI[{2, 4, 1, 2}, {2, 4, 1, 1}, TauB] - 
        4*IEI[{2, 4, 1, 2}, {2, 4, 1, 2}, TauB] - 
        8*IEI[{2, 4, 1, 2}, {2, 4, 3, 1}, TauB]))/((1 + Sqrt[5])*Pi^2) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*IEI[{0, 0, 0, 0}, {3, 4, 1, 2}, 
        TauB]*sGt[{1, 1/4}, Zb, TauB])/((1 + Sqrt[5])*Pi^2) + 
     ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
       sGt[{0, 0}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     ((12*I)*Pi*(-1 + 4*Zb)*EllipticPeriod[1, {a1, a2, a3, a4}]*
       sGt[{0, 0}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
        {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
        {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
        {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
     (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
        {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5])) + 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, -1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, -1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, 1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, 1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, TauB/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, TauB/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, (1 + TauB)/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((96*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {0, 0}, 
      {1, (1 + TauB)/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((48*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {0, 0}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((48*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {0, 0}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, TauB/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, TauB/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, TauB/2}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, TauB/2}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, (1 + TauB)/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, (1 + TauB)/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, (1 + TauB)/2}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, -1/4}, 
      {1, (1 + TauB)/2}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   ((48*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, 
      {0, 0}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   ((48*I)*Pi*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, 
      {0, 0}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, {1, TauB/2}, 
      {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, {1, TauB/2}, 
      {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, {1, TauB/2}, 
      {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, {1, TauB/2}, 
      {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, 
      {1, (1 + TauB)/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, 
      {1, (1 + TauB)/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, 
      {1, (1 + TauB)/2}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, 1/4}, 
      {1, (1 + TauB)/2}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (48*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, -1/4}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, -1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, -1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (48*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, {1, 1/4}, 
      {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, {1, 1/4}, 
      {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, {1, 1/4}, 
      {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, TauB/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, TauB/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, (1 + TauB)/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, TauB/2}, 
      {1, (1 + TauB)/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (48*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, -1/4}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, -1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, -1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (48*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, 1/4}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, 1/4}, {1, TauB/2}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, 1/4}, {1, (1 + TauB)/2}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, TauB/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, TauB/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5]) - 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, (1 + TauB)/2}, {1, -1/4}, Zb, TauB])/(1 + Sqrt[5]) + 
   (24*EllipticPeriod[1, {a1, a2, a3, a4}]*sGt[{0, 0}, {1, (1 + TauB)/2}, 
      {1, (1 + TauB)/2}, {1, 1/4}, Zb, TauB])/(1 + Sqrt[5])}
