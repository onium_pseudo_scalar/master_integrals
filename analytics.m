(* Created with the Wolfram Language : www.wolfram.com *)
{m[1] -> 1/(12*ep^4) + (((3*I)/32)*Pi)/ep^3 - (3*Pi^2)/(32*ep^2) - 
   (((5*I)/64)*Pi^3)/ep + (71*Pi^4)/1440 - Log[2]/(3*ep^3) - 
   (((3*I)/8)*Pi*Log[2])/ep^2 + (3*Pi^2*Log[2])/(8*ep) + 
   ((5*I)/16)*Pi^3*Log[2] + (2*Log[2]^2)/(3*ep^2) + 
   (((3*I)/4)*Pi*Log[2]^2)/ep - (3*Pi^2*Log[2]^2)/4 - (8*Log[2]^3)/(9*ep) - 
   I*Pi*Log[2]^3 + (8*Log[2]^4)/9 - (17*Zeta[3])/(36*ep) - 
   ((3*I)/4)*Pi*Zeta[3] + (17*Log[2]*Zeta[3])/9, 
 m[2] -> -14/3 + 5/(96*ep^4) + 7/(96*ep^3) - 7/(24*ep^2) + 7/(6*ep) + 
   (2*I)*Pi + (((3*I)/64)*Pi)/ep^3 + ((I/8)*Pi)/ep^2 - ((I/2)*Pi)/ep - 
   Pi^2/48 - (11*Pi^2)/(192*ep^2) + Pi^2/(192*ep) - (I/16)*Pi^3 - 
   (((9*I)/128)*Pi^3)/ep + (11*Pi^4)/960 - (81*GR[0, 0, -(-1)^(1/3), 1])/32 - 
   (27*GR[0, 0, -(-1)^(2/3), -1])/8 - (14*Log[2])/3 - (5*Log[2])/(24*ep^3) - 
   (7*Log[2])/(24*ep^2) + (7*Log[2])/(6*ep) + (2*I)*Pi*Log[2] - 
   (((3*I)/16)*Pi*Log[2])/ep^2 - ((I/2)*Pi*Log[2])/ep + (29*Pi^2*Log[2])/48 + 
   (11*Pi^2*Log[2])/(48*ep) + ((7*I)/32)*Pi^3*Log[2] - (7*Log[2]^2)/3 + 
   (5*Log[2]^2)/(12*ep^2) + (7*Log[2]^2)/(12*ep) + I*Pi*Log[2]^2 + 
   (((3*I)/8)*Pi*Log[2]^2)/ep - (11*Pi^2*Log[2]^2)/24 - (7*Log[2]^3)/9 - 
   (5*Log[2]^3)/(9*ep) - (I/2)*Pi*Log[2]^3 + (5*Log[2]^4)/9 + 
   (395*Zeta[3])/144 - (211*Zeta[3])/(288*ep) - ((61*I)/32)*Pi*Zeta[3] + 
   (37*Log[2]*Zeta[3])/18, m[3] -> F[3, 0], 
 m[4] -> -1/32*1/ep^4 + Pi^2/(32*ep^2) + (71*Pi^4)/3840 + Log[2]/(8*ep^3) - 
   (Pi^2*Log[2])/(16*ep) - Log[2]^2/(4*ep^2) + (Pi^2*Log[2]^2)/16 + 
   Log[2]^3/(3*ep) - Log[2]^4/3 + (59*Zeta[3])/(96*ep) - 
   (19*Log[2]*Zeta[3])/12, m[5] -> F[5, 0], 
 m[6] -> (-161*Pi^4)/720 + (27*GR[0, 0, -(-1)^(1/3), 1])/4 + 
   9*GR[0, 0, -(-1)^(2/3), -1] - (7*Zeta[3])/(4*ep) + 7*Log[2]*Zeta[3], 
 m[7] -> (247*Pi^4)/69120 + F[15, 0]/12 - F[31, 0]/4 - 
   (9*GR[0, 0, -(-1)^(1/3), 1])/64 - (3*GR[0, 0, -(-1)^(2/3), -1])/16 - 
   (Pi^2*Log[2]^2)/24 + (7*Log[2]*Zeta[3])/24, 
 m[8] -> -1/96*Pi^4 - (5*Pi^3*G[0, 2])/64 + 2*Catalan*G[0, 0, 2] - 
   (9*Pi*G[0, 0, 0, 2])/8 - (3*I)*G[0, 2]*G[0, 0, 1, 1/2 - I/2] + 
   (3*I)*G[0, 2]*G[0, 0, 1, 1/2 + I/2] + 
   (I/4)*HPLI[{0, minus, plus, minus}] + (7*PolyGamma[3, 1/4])/3072 - 
   PolyGamma[3, 3/4]/1024 + (7*Pi*Zeta[3])/32, 
 m[9] -> (-209*ep*Pi^4)/2880 - (81*ep*GR[0, 0, -(-1)^(1/3), 1])/8 - 
   (27*ep*GR[0, 0, -(-1)^(2/3), -1])/2 - (2*Pi^2*Log[2])/3 - 
   4*ep*Pi^2*Log[2] + (11*ep*Pi^2*Log[2]^2)/6 - 3*Zeta[3] - 18*ep*Zeta[3] - 
   3*ep*Log[2]*Zeta[3], m[10] -> -1/8*Pi^2/ep^2 - (593*Pi^4)/2880 - 
   (27*GR[0, 0, -(-1)^(1/3), 1])/8 - (9*GR[0, 0, -(-1)^(2/3), -1])/2 - 
   (7*Zeta[3])/(2*ep), m[11] -> Pi^2/2 + 3*ep*Pi^2 + (797*ep*Pi^4)/24000 + 
   2*ep*F[12, 1] - (28*ep*F[15, 0])/25 + 2*F[18, 0] + 12*ep*F[18, 0] - 
   (284*ep*F[31, 0])/25 - 5*F[57, 0] - (112*ep*F[57, 0])/5 - 
   (19*ep*F[57, 1])/5 - (6561*ep*GR[0, 0, -(-1)^(1/3), 1])/200 - 
   (2187*ep*GR[0, 0, -(-1)^(2/3), -1])/50 + (Pi^2*Log[2])/2 + 
   3*ep*Pi^2*Log[2] + (4*ep*Pi^2*Log[2]^2)/25 - (25*Zeta[3])/2 - 
   75*ep*Zeta[3] - (77*ep*Log[2]*Zeta[3])/10, 
 m[12] -> Pi^2/4 + ep*F[12, 1] + F[18, 0] - (3*F[57, 0])/5 + 
   (Pi^2*Log[2])/20 - (29*Zeta[3])/40, 
 m[13] -> (-157*Pi^4)/21600 + (2*F[15, 0])/15 + (2*F[31, 0])/5 + 
   (9*GR[0, 0, -(-1)^(1/3), 1])/20 + (3*GR[0, 0, -(-1)^(2/3), -1])/5 - 
   (Pi^2*Log[2]^2)/15 + (7*Log[2]*Zeta[3])/6, 
 m[14] -> F[14, 0] + ep*F[14, 1], m[15] -> F[15, 0], 
 m[16] -> -1/48*Pi^4 - (Pi^2*Log[2]^2)/2 + (7*Log[2]*Zeta[3])/4, 
 m[17] -> (53*Pi^4)/2880 + (27*GR[0, 0, -(-1)^(1/3), 1])/8 + 
   (9*GR[0, 0, -(-1)^(2/3), -1])/2 + (7*Zeta[3])/(8*ep), 
 m[18] -> -1/8*Pi^2/ep + F[18, 0] + ep*F[18, 1], 
 m[19] -> -1/4*Pi^2 - Pi^2/(8*ep) - (ep*Pi^2)/2 - (219*ep*Pi^4)/1280 + 
   (81*ep*GR[0, 0, -(-1)^(1/3), 1])/32 + (27*ep*GR[0, 0, -(-1)^(2/3), -1])/
    8 + (Pi^2*Log[2])/4 + (ep*Pi^2*Log[2])/2 - (ep*Pi^2*Log[2]^2)/8 - 
   (7*Zeta[3])/2 - 7*ep*Zeta[3] + (49*ep*Log[2]*Zeta[3])/8, 
 m[20] -> 1/(48*ep^4) - Pi^2/(48*ep^2) - (833*Pi^4)/5760 - Log[2]/(12*ep^3) + 
   (Pi^2*Log[2])/(12*ep) + Log[2]^2/(6*ep^2) - (Pi^2*Log[2]^2)/6 - 
   (2*Log[2]^3)/(9*ep) + (2*Log[2]^4)/9 - (269*Zeta[3])/(144*ep) + 
   (143*Log[2]*Zeta[3])/36, m[21] -> (23*Pi^4)/28800 - (2*F[15, 0])/5 - 
   (6*F[31, 0])/5 - (189*GR[0, 0, -(-1)^(1/3), 1])/40 - 
   (63*GR[0, 0, -(-1)^(2/3), -1])/10 + (Pi^2*Log[2]^2)/5 - 
   (7*Log[2]*Zeta[3])/2, m[22] -> F[22, 0], 
 m[23] -> (51479*Pi^4)/1036800 - F[15, 0]/5 - 3*F[22, 0] - (13*F[31, 0])/5 - 
   (3*Pi*GI[0, 1, -(-1)^(1/3)])/8 - (Pi*GI[0, -(-1)^(2/3), -1])/2 + 
   (99*GR[0, 0, -(-1)^(1/3), 1])/320 + (33*GR[0, 0, -(-1)^(2/3), -1])/80 + 
   3*GR[-(-1)^(2/3), 1, 1, -1] - (5*Pi*Cl[2, Pi/3]*Log[2])/12 + 
   (3*GR[-(-1)^(2/3), 1, -1]*Log[2])/2 + (19*Pi^2*Log[2]^2)/40 - 
   (Pi*Cl[2, Pi/3]*Log[3])/8 + (3*Pi^2*PolyLog[2, -1/2])/4 - 
   (187*Log[2]*Zeta[3])/24, m[24] -> F[24, 0], m[25] -> F[25, 0], 
 m[26] -> (-161*Pi^4)/720 + (27*GR[0, 0, -(-1)^(1/3), 1])/4 + 
   9*GR[0, 0, -(-1)^(2/3), -1] - (7*Zeta[3])/(4*ep) + 7*Log[2]*Zeta[3], 
 m[27] -> 1/(16*ep^4) + ((I/8)*Pi)/ep^3 - (3*Pi^2)/(16*ep^2) - 
   (((5*I)/24)*Pi^3)/ep + (87*Pi^4)/640 - Log[2]/(4*ep^3) - 
   ((I/2)*Pi*Log[2])/ep^2 + (3*Pi^2*Log[2])/(4*ep) + ((5*I)/6)*Pi^3*Log[2] + 
   Log[2]^2/(2*ep^2) + (I*Pi*Log[2]^2)/ep - (3*Pi^2*Log[2]^2)/2 - 
   (2*Log[2]^3)/(3*ep) - ((4*I)/3)*Pi*Log[2]^3 + (2*Log[2]^4)/3 - 
   (83*Zeta[3])/(48*ep) - ((83*I)/24)*Pi*Zeta[3] + (83*Log[2]*Zeta[3])/12, 
 m[28] -> (-1/6*I)*Pi^3 - (I/3)*ep*Pi^3 + (29*ep*Pi^4)/180 - 
   27*ep*GR[0, 0, -(-1)^(1/3), 1] - 36*ep*GR[0, 0, -(-1)^(2/3), -1] - 
   (2*Pi^2*Log[2])/3 - (4*ep*Pi^2*Log[2])/3 + ((2*I)/3)*ep*Pi^3*Log[2] + 
   (4*ep*Pi^2*Log[2]^2)/3 + (3*Zeta[3])/2 + 3*ep*Zeta[3] - 
   (7*I)*ep*Pi*Zeta[3] - 14*ep*Log[2]*Zeta[3], 
 m[29] -> (-71*Pi^4)/1440 + (27*GR[0, 0, -(-1)^(1/3), 1])/8 + 
   (9*GR[0, 0, -(-1)^(2/3), -1])/2, 
 m[30] -> -5/(96*ep^3) + (5*Pi^2)/(192*ep) + (71*ep*Pi^4)/3840 - 
   (27*ep*GR[0, 0, -(-1)^(1/3), 1])/16 - (9*ep*GR[0, 0, -(-1)^(2/3), -1])/4 + 
   (5*Log[2])/(24*ep^2) - (5*Pi^2*Log[2])/48 - (5*Log[2]^2)/(12*ep) + 
   (5*ep*Pi^2*Log[2]^2)/24 + (5*Log[2]^3)/9 - (5*ep*Log[2]^4)/9 + 
   (55*Zeta[3])/72 - (55*ep*Log[2]*Zeta[3])/18, m[31] -> F[31, 0], 
 m[32] -> (-36259*Pi^4)/622080 - F[15, 0] - F[31, 0] + 
   (Pi*GI[0, 1, -(-1)^(1/3)])/8 + (Pi*GI[0, -(-1)^(2/3), -1])/6 - 
   (309*GR[0, 0, -(-1)^(1/3), 1])/64 - (103*GR[0, 0, -(-1)^(2/3), -1])/16 - 
   GR[-(-1)^(2/3), 1, 1, -1] + (5*Pi*Cl[2, Pi/3]*Log[2])/36 - 
   (GR[-(-1)^(2/3), 1, -1]*Log[2])/2 + (3*Pi^2*Log[2]^2)/8 + 
   (Pi*Cl[2, Pi/3]*Log[3])/24 - (Pi^2*PolyLog[2, -1/2])/4 - 
   (317*Log[2]*Zeta[3])/72, m[33] -> -89/16 + (57*ep)/8 + (5*Pi^2)/24 + 
   (1543657*ep^2*Pi^4)/2592000 - (5*ep*Pi*Cl[2, Pi/3])/12 + 
   (4*ep^2*F[15, 0])/25 - (288*ep^2*F[31, 0])/25 - (23*ep*F[57, 0])/5 + 
   (46*ep^2*F[57, 0])/5 - (23*ep^2*F[57, 1])/5 + F[64, 0]/2 - 
   (7*ep*F[64, 0])/4 + (3*ep^2*F[64, 0])/2 + (ep*F[64, 1])/2 - 
   (7*ep^2*F[64, 1])/4 + (ep^2*F[64, 2])/2 - F[65, 0]/2 + ep*F[65, 0] - 
   (ep*F[65, 1])/2 + ep^2*F[65, 1] - (ep^2*F[65, 2])/2 - 
   (3*ep^2*Pi*GI[0, 1, -(-1)^(1/3)])/4 - ep^2*Pi*GI[0, -(-1)^(2/3), -1] - 
   (3*ep*GR[-(-1)^(2/3), 1, -1])/2 - (26883*ep^2*GR[0, 0, -(-1)^(1/3), 1])/
    800 - (12561*ep^2*GR[0, 0, -(-1)^(2/3), -1])/200 + 
   6*ep^2*GR[-(-1)^(2/3), 1, 1, -1] - (13*ep*Pi^2*Log[2])/40 + 
   6*ep^2*GR[-(-1)^(2/3), 1, -1]*Log[2] + (87*ep^2*Pi^2*Log[2]^2)/100 - 
   (3*ep*Log[2]^3)/8 + (3*ep^2*Log[2]^4)/4 + (3*ep*Pi^2*Log[3])/4 - 
   (ep^2*Pi*Cl[2, Pi/3]*Log[3])/4 - ep^2*Pi^2*Log[2]*Log[3] + 
   (3*ep*Log[2]^2*Log[3])/4 - (3*ep^2*Log[2]^3*Log[3])/2 + 
   ep^2*Pi^2*PolyLog[2, -1/2] - (3*ep*Log[2]*PolyLog[2, -1/2])/4 + 
   (3*ep^2*Log[2]^2*PolyLog[2, -1/2])/2 - (847*ep*Zeta[3])/120 - 
   (313*ep^2*Log[2]*Zeta[3])/20, 
 m[34] -> (-19087*Pi^4)/622080 + (Pi*GI[0, 1, -(-1)^(1/3)])/8 + 
   (Pi*GI[0, -(-1)^(2/3), -1])/6 + (15*GR[0, 0, -(-1)^(1/3), 1])/64 + 
   (5*GR[0, 0, -(-1)^(2/3), -1])/16 - GR[-(-1)^(2/3), 1, 1, -1] + 
   (5*Pi*Cl[2, Pi/3]*Log[2])/36 - (GR[-(-1)^(2/3), 1, -1]*Log[2])/2 - 
   (Pi^2*Log[2]^2)/8 + (Pi*Cl[2, Pi/3]*Log[3])/24 - 
   (Pi^2*PolyLog[2, -1/2])/4 + (61*Log[2]*Zeta[3])/72, 
 m[35] -> (317*ep*Pi^4)/3888 - (5*Pi*Cl[2, Pi/3])/36 - 
   (ep*Pi*GI[0, 1, -(-1)^(1/3)])/4 - (ep*Pi*GI[0, -(-1)^(2/3), -1])/3 - 
   GR[-(-1)^(2/3), 1, -1]/2 - 3*ep*GR[0, 0, -(-1)^(1/3), 1] - 
   10*ep*GR[0, 0, -(-1)^(2/3), -1] + 2*ep*GR[-(-1)^(2/3), 1, 1, -1] - 
   (7*Pi^2*Log[2])/24 + 2*ep*GR[-(-1)^(2/3), 1, -1]*Log[2] + 
   (ep*Pi^2*Log[2]^2)/2 - Log[2]^3/8 + (ep*Log[2]^4)/4 + (Pi^2*Log[3])/4 - 
   (ep*Pi*Cl[2, Pi/3]*Log[3])/12 - (ep*Pi^2*Log[2]*Log[3])/3 + 
   (Log[2]^2*Log[3])/4 - (ep*Log[2]^3*Log[3])/2 + 
   (ep*Pi^2*PolyLog[2, -1/2])/3 - (Log[2]*PolyLog[2, -1/2])/4 + 
   (ep*Log[2]^2*PolyLog[2, -1/2])/2 - (11*Zeta[3])/144 - 
   (163*ep*Log[2]*Zeta[3])/24, m[36] -> -1/96*Pi^4 - (7*Log[2]*Zeta[3])/4, 
 m[37] -> (-37*Pi^4)/2880 + (27*GR[0, 0, -(-1)^(1/3), 1])/8 + 
   (9*GR[0, 0, -(-1)^(2/3), -1])/2 - (Pi^2*Log[2]^2)/2 + 
   (7*Log[2]*Zeta[3])/2, m[38] -> (-19*Pi^4)/1440 + (Pi^2*G[0, 2]^2)/6 + 
   G[0, 2]^4/12 + (I/24)*Pi^3*G[0, 3 - 2*Sqrt[2]] - 
   (Pi^2*G[0, 2]*G[0, 3 - 2*Sqrt[2]])/12 - (Pi^2*G[0, 3 - 2*Sqrt[2]]^2)/16 + 
   (I/24)*Pi*G[0, 3 - 2*Sqrt[2]]^3 - (G[0, 2]*G[0, 3 - 2*Sqrt[2]]^3)/12 - 
   G[0, 3 - 2*Sqrt[2]]^4/32 + (I/24)*Pi^3*G[0, 3 + 2*Sqrt[2]] - 
   (Pi^2*G[0, 2]*G[0, 3 + 2*Sqrt[2]])/12 - (Pi^2*G[0, 3 + 2*Sqrt[2]]^2)/16 + 
   (I/24)*Pi*G[0, 3 + 2*Sqrt[2]]^3 - (G[0, 2]*G[0, 3 + 2*Sqrt[2]]^3)/12 - 
   G[0, 3 + 2*Sqrt[2]]^4/32 - (I/4)*Pi*G[0, 0, 1/2 - 1/Sqrt[2], 1] + 
   (G[0, 2]*G[0, 0, 1/2 - 1/Sqrt[2], 1])/2 - 
   (I/4)*Pi*G[0, 0, 1/2 + 1/Sqrt[2], 1] + 
   (G[0, 2]*G[0, 0, 1/2 + 1/Sqrt[2], 1])/2 - 
   (I/4)*Pi*G[0, 1/2 - 1/Sqrt[2], 1, 1] + 
   (G[0, 2]*G[0, 1/2 - 1/Sqrt[2], 1, 1])/2 - 
   (I/4)*Pi*G[0, 1/2 + 1/Sqrt[2], 1, 1] + 
   (G[0, 2]*G[0, 1/2 + 1/Sqrt[2], 1, 1])/2 + 
   (I/4)*Pi*G[1/2 - 1/Sqrt[2], 0, 1, 1] - 
   (G[0, 2]*G[1/2 - 1/Sqrt[2], 0, 1, 1])/2 + 
   (I/4)*Pi*G[1/2 + 1/Sqrt[2], 0, 1, 1] - 
   (G[0, 2]*G[1/2 + 1/Sqrt[2], 0, 1, 1])/2 + 
   (3*G[0, 0, 0, 1/2 - 1/Sqrt[2], 1])/4 + (3*G[0, 0, 0, 1/2 + 1/Sqrt[2], 1])/
    4 + (3*G[0, 0, 1/2 - 1/Sqrt[2], 1, 1])/4 + 
   (3*G[0, 0, 1/2 + 1/Sqrt[2], 1, 1])/4 + G[0, 1/2 - 1/Sqrt[2], 0, 1, 1]/4 + 
   G[0, 1/2 - 1/Sqrt[2], 1, 1, 1] + G[0, 1/2 + 1/Sqrt[2], 0, 1, 1]/4 + 
   G[0, 1/2 + 1/Sqrt[2], 1, 1, 1] - G[1/2 - 1/Sqrt[2], 0, 1, 1, 1]/2 - 
   G[1/2 - 1/Sqrt[2], 1, 0, 1, 1]/4 - G[1/2 + 1/Sqrt[2], 0, 1, 1, 1]/2 - 
   G[1/2 + 1/Sqrt[2], 1, 0, 1, 1]/4 + 2*PolyLog[4, 1/2], 
 m[39] -> -1/64*Pi^4 - (Pi^2*Log[2]^2)/4, 
 m[40] -> (-1/8*I)*Pi^3 - (I/4)*ep*Pi^3 + (1307*ep*Pi^4)/5760 - 
   (297*ep*GR[0, 0, -(-1)^(1/3), 1])/16 - (99*ep*GR[0, 0, -(-1)^(2/3), -1])/
    4 - (Pi^2*Log[2])/2 - ep*Pi^2*Log[2] + (I/4)*ep*Pi^3*Log[2] + 
   (21*Zeta[3])/8 + (21*ep*Zeta[3])/4 - ((7*I)/4)*ep*Pi*Zeta[3] - 
   (63*ep*Log[2]*Zeta[3])/4, m[41] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + 
   (65*ep)/2 + (211*ep^2)/2 + Pi^2/12 + (5*ep*Pi^2)/12 + (19*ep^2*Pi^2)/12 - 
   (41*ep^2*Pi^4)/720 - 10*Log[2] - (2*Log[2])/ep - 38*ep*Log[2] - 
   130*ep^2*Log[2] - (ep*Pi^2*Log[2])/3 - (5*ep^2*Pi^2*Log[2])/3 + 
   4*Log[2]^2 + 20*ep*Log[2]^2 + 76*ep^2*Log[2]^2 + 
   (2*ep^2*Pi^2*Log[2]^2)/3 - (16*ep*Log[2]^3)/3 - (80*ep^2*Log[2]^3)/3 + 
   (16*ep^2*Log[2]^4)/3 - (13*ep*Zeta[3])/3 - (65*ep^2*Zeta[3])/3 + 
   (52*ep^2*Log[2]*Zeta[3])/3, m[42] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + 
   (65*ep)/2 - (13921*ep*Pi^4)/155520 + (5*Pi*Cl[2, Pi/3])/36 - 
   (5*ep*Pi*Cl[2, Pi/3])/36 + (ep*Pi*GI[0, 1, -(-1)^(1/3)])/2 + 
   (2*ep*Pi*GI[0, -(-1)^(2/3), -1])/3 + GR[-(-1)^(2/3), 1, -1]/2 - 
   (ep*GR[-(-1)^(2/3), 1, -1])/2 - (147*ep*GR[0, 0, -(-1)^(1/3), 1])/16 - 
   (25*ep*GR[0, 0, -(-1)^(2/3), -1])/4 - 4*ep*GR[-(-1)^(2/3), 1, 1, -1] - 
   10*Log[2] - (2*Log[2])/ep - 38*ep*Log[2] + (Pi^2*Log[2])/24 - 
   (7*ep*Pi^2*Log[2])/8 + (5*ep*Pi*Cl[2, Pi/3]*Log[2])/18 - 
   3*ep*GR[-(-1)^(2/3), 1, -1]*Log[2] + 3*Log[2]^2 + 15*ep*Log[2]^2 - 
   (ep*Pi^2*Log[2]^2)/2 + Log[2]^3/8 - (83*ep*Log[2]^3)/24 - 
   (ep*Log[2]^4)/4 - (Pi^2*Log[3])/4 + (ep*Pi^2*Log[3])/4 + 
   (ep*Pi*Cl[2, Pi/3]*Log[3])/6 + (ep*Pi^2*Log[2]*Log[3])/3 - 
   (Log[2]^2*Log[3])/4 + (ep*Log[2]^2*Log[3])/4 + (ep*Log[2]^3*Log[3])/2 - 
   (5*ep*Pi^2*PolyLog[2, -1/2])/6 + (Log[2]*PolyLog[2, -1/2])/4 - 
   (ep*Log[2]*PolyLog[2, -1/2])/4 - (ep*Log[2]^2*PolyLog[2, -1/2])/2 + 
   (37*Zeta[3])/72 - (223*ep*Zeta[3])/72 + (85*ep*Log[2]*Zeta[3])/36, 
 m[43] -> Pi^2/(8*ep) + (16061*ep*Pi^4)/62208 - (5*Pi*Cl[2, Pi/3])/36 - 
   (ep*Pi*GI[0, 1, -(-1)^(1/3)])/4 - (ep*Pi*GI[0, -(-1)^(2/3), -1])/3 - 
   GR[-(-1)^(2/3), 1, -1]/2 + (39*ep*GR[0, 0, -(-1)^(1/3), 1])/32 - 
   (35*ep*GR[0, 0, -(-1)^(2/3), -1])/8 + 2*ep*GR[-(-1)^(2/3), 1, 1, -1] - 
   (7*Pi^2*Log[2])/24 + 2*ep*GR[-(-1)^(2/3), 1, -1]*Log[2] + 
   (ep*Pi^2*Log[2]^2)/2 - Log[2]^3/8 + (ep*Log[2]^4)/4 + (Pi^2*Log[3])/4 - 
   (ep*Pi*Cl[2, Pi/3]*Log[3])/12 - (ep*Pi^2*Log[2]*Log[3])/3 + 
   (Log[2]^2*Log[3])/4 - (ep*Log[2]^3*Log[3])/2 + 
   (ep*Pi^2*PolyLog[2, -1/2])/3 - (Log[2]*PolyLog[2, -1/2])/4 + 
   (ep*Log[2]^2*PolyLog[2, -1/2])/2 + (19*Zeta[3])/9 - 
   (71*ep*Log[2]*Zeta[3])/12, m[44] -> 2 + 1/(2*ep^2) + ep^(-1) + 4*ep + 
   8*ep^2 + Pi^2/3 + (2*ep*Pi^2)/3 + (4*ep^2*Pi^2)/3 + 
   (209923*ep^2*Pi^4)/466560 - (ep^2*Pi*GI[0, 1, -(-1)^(1/3)])/6 - 
   (2*ep^2*Pi*GI[0, -(-1)^(2/3), -1])/9 - (41*ep^2*GR[0, 0, -(-1)^(1/3), 1])/
    16 - (41*ep^2*GR[0, 0, -(-1)^(2/3), -1])/12 + 
   (4*ep^2*GR[-(-1)^(2/3), 1, 1, -1])/3 - (ep*Pi^2*Log[2])/18 - 
   (ep^2*Pi^2*Log[2])/9 - (5*ep^2*Pi*Cl[2, Pi/3]*Log[2])/27 + 
   (2*ep^2*GR[-(-1)^(2/3), 1, -1]*Log[2])/3 - Log[2]^2/3 - 
   (2*ep*Log[2]^2)/3 - (4*ep^2*Log[2]^2)/3 - (ep^2*Pi^2*Log[2]^2)/18 + 
   (2*ep*Log[2]^3)/3 + (4*ep^2*Log[2]^3)/3 - (7*ep^2*Log[2]^4)/9 - 
   (ep^2*Pi*Cl[2, Pi/3]*Log[3])/18 + (ep^2*Pi^2*PolyLog[2, -1/2])/3 + 
   (55*ep*Zeta[3])/12 + (55*ep^2*Zeta[3])/6 - (349*ep^2*Log[2]*Zeta[3])/54, 
 m[45] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + (65*ep)/2 - Pi^2/6 - (5*ep*Pi^2)/6 + 
   (7*ep*Pi^4)/2880 - (27*ep*GR[0, 0, -(-1)^(1/3), 1])/8 - 
   (9*ep*GR[0, 0, -(-1)^(2/3), -1])/2 + (ep*Pi^2*Log[2])/2 - (7*Zeta[3])/4 - 
   (22*ep*Zeta[3])/3 - (7*ep*Log[2]*Zeta[3])/2, 
 m[46] -> Pi^2/(8*ep) + (457*ep*Pi^4)/5760 - (27*ep*GR[0, 0, -(-1)^(1/3), 1])/
    16 - (9*ep*GR[0, 0, -(-1)^(2/3), -1])/4 - (Pi^2*Log[2])/4 + 
   (ep*Pi^2*Log[2]^2)/4 + (21*Zeta[3])/8 - (7*ep*Log[2]*Zeta[3])/4, 
 m[47] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + (65*ep)/2 - Pi^2/12 - 
   (5*ep*Pi^2)/12 + (37*ep*Pi^4)/960 - (81*ep*GR[0, 0, -(-1)^(1/3), 1])/8 - 
   (27*ep*GR[0, 0, -(-1)^(2/3), -1])/2 - 10*Log[2] - (2*Log[2])/ep - 
   38*ep*Log[2] + 2*Log[2]^2 + 10*ep*Log[2]^2 - (4*ep*Log[2]^3)/3 - 
   (7*Zeta[3])/4 - (49*ep*Zeta[3])/12 - (7*ep*Log[2]*Zeta[3])/2, 
 m[48] -> -13/8 + 1/(2*ep^2) + 5/(2*ep) - (71*ep)/8 - Pi^2/12 - 
   (5*ep*Pi^2)/12 + (149*ep*Pi^4)/36000 + (16*ep*F[15, 0])/25 - 
   (152*ep*F[31, 0])/25 - (12*F[57, 0])/5 - (12*ep*F[57, 0])/5 - 
   (12*ep*F[57, 1])/5 + F[64, 0] + (3*ep*F[64, 0])/2 + ep*F[64, 1] - 
   F[65, 0] - 3*ep*F[65, 0] - ep*F[65, 1] - 
   (2079*ep*GR[0, 0, -(-1)^(1/3), 1])/100 - 
   (693*ep*GR[0, 0, -(-1)^(2/3), -1])/25 + (Pi^2*Log[2])/5 + 
   (3*ep*Pi^2*Log[2])/5 - (13*ep*Pi^2*Log[2]^2)/25 - (32*Zeta[3])/5 - 
   (283*ep*Zeta[3])/15 + (7*ep*Log[2]*Zeta[3])/5, 
 m[49] -> 73/8 + 1/(2*ep^2) + 1/(2*ep) + (61*ep)/8 + Pi^2/6 + 
   (13*ep*Pi^2)/12 + (191*ep*Pi^4)/8000 - (2*ep*F[15, 0])/25 - 
   (56*ep*F[31, 0])/25 - (6*F[57, 0])/5 + (6*ep*F[57, 0])/5 - 
   (6*ep*F[57, 1])/5 - F[64, 0] + (ep*F[64, 0])/2 - ep*F[64, 1] + 
   2*F[65, 0] + 2*ep*F[65, 0] + 2*ep*F[65, 1] - 
   (2349*ep*GR[0, 0, -(-1)^(1/3), 1])/200 - 
   (783*ep*GR[0, 0, -(-1)^(2/3), -1])/50 + (Pi^2*Log[2])/10 + 
   (ep*Pi^2*Log[2])/10 - (3*ep*Pi^2*Log[2]^2)/50 - (16*Zeta[3])/5 - 
   (58*ep*Zeta[3])/15 + (7*ep*Log[2]*Zeta[3])/10, 
 m[50] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + (65*ep)/2 + (5*I)*Pi + (I*Pi)/ep + 
   (19*I)*ep*Pi - (7*Pi^2)/12 - (Sqrt[2]*Pi^2)/3 - (35*ep*Pi^2)/12 - 
   (5*Sqrt[2]*ep*Pi^2)/3 - (I/6)*ep*Pi^3 - (I/3)*Sqrt[2]*ep*Pi^3 - 
   (ep*Pi^4)/36 - 10*G[0, 2] - 38*ep*G[0, 2] - (2*I)*Pi*G[0, 2] - 
   (2*I)*Sqrt[2]*Pi*G[0, 2] - (10*I)*ep*Pi*G[0, 2] - 
   (10*I)*Sqrt[2]*ep*Pi*G[0, 2] - (Pi^2*G[0, 2])/3 - (2*ep*Pi^2*G[0, 2])/3 + 
   2*Sqrt[2]*ep*Pi^2*G[0, 2] - (I/3)*ep*Pi^3*G[0, 2] + 2*G[0, 2]^2 + 
   2*Sqrt[2]*G[0, 2]^2 + 10*ep*G[0, 2]^2 + 10*Sqrt[2]*ep*G[0, 2]^2 - 
   I*Pi*G[0, 2]^2 - (3*I)*ep*Pi*G[0, 2]^2 + (8*I)*Sqrt[2]*ep*Pi*G[0, 2]^2 + 
   (4*ep*Pi^2*G[0, 2]^2)/3 + (2*G[0, 2]^3)/3 + 2*ep*G[0, 2]^3 - 
   (8*Sqrt[2]*ep*G[0, 2]^3)/3 + (2*I)*ep*Pi*G[0, 2]^3 - ep*G[0, 2]^4 + 
   2*Sqrt[2]*ep*Pi^2*G[0, 4 - 2*Sqrt[2]] + (8*I)*Sqrt[2]*ep*Pi*G[0, 2]*
    G[0, 4 - 2*Sqrt[2]] - 12*Sqrt[2]*ep*G[0, 2]^2*G[0, 4 - 2*Sqrt[2]] - 
   (4*I)*Sqrt[2]*ep*Pi*G[0, 4 - 2*Sqrt[2]]^2 + 12*Sqrt[2]*ep*G[0, 2]*
    G[0, 4 - 2*Sqrt[2]]^2 - (4*Sqrt[2]*ep*G[0, 4 - 2*Sqrt[2]]^3)/3 + 
   (10*I)*Sqrt[2]*ep*Pi*G[0, 2*(-1 + Sqrt[2])] - 
   (8*I)*Sqrt[2]*ep*Pi*G[0, 4 - 2*Sqrt[2]]*G[0, 2*(-1 + Sqrt[2])] - 
   8*Sqrt[2]*ep*G[0, 4 - 2*Sqrt[2]]^2*G[0, 2*(-1 + Sqrt[2])] + 
   2*Sqrt[2]*G[0, 2*(-1 + Sqrt[2])]^2 + 10*Sqrt[2]*ep*
    G[0, 2*(-1 + Sqrt[2])]^2 - (5*I)*ep*Pi*G[0, 2*(-1 + Sqrt[2])]^2 + 
   (4*I)*Sqrt[2]*ep*Pi*G[0, 2*(-1 + Sqrt[2])]^2 - 
   (2*G[0, 2*(-1 + Sqrt[2])]^3)/3 - (10*ep*G[0, 2*(-1 + Sqrt[2])]^3)/3 - 
   (8*Sqrt[2]*ep*G[0, 2*(-1 + Sqrt[2])]^3)/3 - 
   (4*I)*ep*Pi*G[0, 2*(-1 + Sqrt[2])]^3 - (7*ep*G[0, 2*(-1 + Sqrt[2])]^4)/3 + 
   (2*I)*Sqrt[2]*Pi*G[0, -2 + 2*Sqrt[2]] + (Pi^2*G[0, -2 + 2*Sqrt[2]])/3 + 
   (5*ep*Pi^2*G[0, -2 + 2*Sqrt[2]])/3 - 2*Sqrt[2]*ep*Pi^2*
    G[0, -2 + 2*Sqrt[2]] + (I/3)*ep*Pi^3*G[0, -2 + 2*Sqrt[2]] - 
   4*Sqrt[2]*G[0, 2]*G[0, -2 + 2*Sqrt[2]] - 20*Sqrt[2]*ep*G[0, 2]*
    G[0, -2 + 2*Sqrt[2]] + (2*I)*Pi*G[0, 2]*G[0, -2 + 2*Sqrt[2]] + 
   (10*I)*ep*Pi*G[0, 2]*G[0, -2 + 2*Sqrt[2]] - (12*I)*Sqrt[2]*ep*Pi*G[0, 2]*
    G[0, -2 + 2*Sqrt[2]] - (10*ep*Pi^2*G[0, 2]*G[0, -2 + 2*Sqrt[2]])/3 - 
   2*G[0, 2]^2*G[0, -2 + 2*Sqrt[2]] - 10*ep*G[0, 2]^2*G[0, -2 + 2*Sqrt[2]] + 
   8*Sqrt[2]*ep*G[0, 2]^2*G[0, -2 + 2*Sqrt[2]] - 
   (8*I)*ep*Pi*G[0, 2]^2*G[0, -2 + 2*Sqrt[2]] + 
   (16*ep*G[0, 2]^3*G[0, -2 + 2*Sqrt[2]])/3 + 16*Sqrt[2]*ep*G[0, 2]*
    G[0, 4 - 2*Sqrt[2]]*G[0, -2 + 2*Sqrt[2]] - I*Pi*G[0, -2 + 2*Sqrt[2]]^2 + 
   2*ep*Pi^2*G[0, -2 + 2*Sqrt[2]]^2 + 2*G[0, 2]*G[0, -2 + 2*Sqrt[2]]^2 + 
   10*ep*G[0, 2]*G[0, -2 + 2*Sqrt[2]]^2 + (10*I)*ep*Pi*G[0, 2]*
    G[0, -2 + 2*Sqrt[2]]^2 - 10*ep*G[0, 2]^2*G[0, -2 + 2*Sqrt[2]]^2 - 
   8*Sqrt[2]*ep*G[0, 4 - 2*Sqrt[2]]*G[0, -2 + 2*Sqrt[2]]^2 + 
   8*ep*G[0, 2]*G[0, -2 + 2*Sqrt[2]]^3 - 2*Sqrt[2]*G[0, 1, 3 - 2*Sqrt[2]] - 
   10*Sqrt[2]*ep*G[0, 1, 3 - 2*Sqrt[2]] - (2*ep*Pi^2*G[0, 1, 3 - 2*Sqrt[2]])/
    3 - (4*I)*ep*Pi*G[0, 2]*G[0, 1, 3 - 2*Sqrt[2]] + 
   4*ep*G[0, 2]^2*G[0, 1, 3 - 2*Sqrt[2]] + 
   4*Sqrt[2]*ep*G[0, 2*(-1 + Sqrt[2])]*G[0, 1, 3 - 2*Sqrt[2]] + 
   (4*I)*ep*Pi*G[0, 2*(-1 + Sqrt[2])]*G[0, 1, 3 - 2*Sqrt[2]] + 
   4*ep*G[0, 2*(-1 + Sqrt[2])]^2*G[0, 1, 3 - 2*Sqrt[2]] - 
   8*ep*G[0, 2]*G[0, -2 + 2*Sqrt[2]]*G[0, 1, 3 - 2*Sqrt[2]] - 
   2*ep*G[0, 1, 3 - 2*Sqrt[2]]^2 + 8*Sqrt[2]*ep*G[0, 4 - 2*Sqrt[2]]*
    G[0, 1, 4 - 2*Sqrt[2]] + (4*I)*Sqrt[2]*ep*Pi*G[0, 1, -3 + 2*Sqrt[2]] - 
   (2*ep*Pi^2*G[0, 1, -3 + 2*Sqrt[2]])/3 - 8*Sqrt[2]*ep*G[0, 2]*
    G[0, 1, -3 + 2*Sqrt[2]] - (4*I)*ep*Pi*G[0, 2]*G[0, 1, -3 + 2*Sqrt[2]] + 
   4*ep*G[0, 2]^2*G[0, 1, -3 + 2*Sqrt[2]] + 8*Sqrt[2]*ep*G[0, 4 - 2*Sqrt[2]]*
    G[0, 1, -3 + 2*Sqrt[2]] + (4*I)*ep*Pi*G[0, 2*(-1 + Sqrt[2])]*
    G[0, 1, -3 + 2*Sqrt[2]] + 4*ep*G[0, 2*(-1 + Sqrt[2])]^2*
    G[0, 1, -3 + 2*Sqrt[2]] - 8*ep*G[0, 2]*G[0, -2 + 2*Sqrt[2]]*
    G[0, 1, -3 + 2*Sqrt[2]] + 4*Sqrt[2]*ep*G[0, 2*(-1 + Sqrt[2])]*
    G[0, 1, -2 + 2*Sqrt[2]] + 8*Sqrt[2]*ep*G[0, 0, 1, 1/2 - 1/Sqrt[2]] - 
   8*Sqrt[2]*ep*G[0, 0, 1, 1 - 1/Sqrt[2]] + G[0, 0, 1, 3 - 2*Sqrt[2]] + 
   5*ep*G[0, 0, 1, 3 - 2*Sqrt[2]] + 2*Sqrt[2]*ep*G[0, 0, 1, 3 - 2*Sqrt[2]] - 
   (2*I)*ep*Pi*G[0, 0, 1, 3 - 2*Sqrt[2]] + 
   4*ep*G[0, 2]*G[0, 0, 1, 3 - 2*Sqrt[2]] - 2*ep*G[0, 2*(-1 + Sqrt[2])]*
    G[0, 0, 1, 3 - 2*Sqrt[2]] - 8*Sqrt[2]*ep*G[0, 0, 1, 4 - 2*Sqrt[2]] - 
   8*Sqrt[2]*ep*G[0, 0, 1, 1 - Sqrt[2]] + 
   8*Sqrt[2]*ep*G[0, 0, 1, 2 - Sqrt[2]] + 
   8*Sqrt[2]*ep*G[0, 0, 1, -1 + Sqrt[2]] - 
   4*Sqrt[2]*ep*G[0, 0, 1, -3 + 2*Sqrt[2]] - 
   (4*I)*ep*Pi*G[0, 0, 1, -3 + 2*Sqrt[2]] + 
   8*ep*G[0, 2]*G[0, 0, 1, -3 + 2*Sqrt[2]] - 8*ep*G[0, 2*(-1 + Sqrt[2])]*
    G[0, 0, 1, -3 + 2*Sqrt[2]] - 4*Sqrt[2]*ep*G[0, 0, 1, -2 + 2*Sqrt[2]] + 
   4*ep*G[0, 0, 0, 1, 3 - 2*Sqrt[2]] + 6*ep*G[0, 0, 0, 1, -3 + 2*Sqrt[2]] + 
   2*ep*G[0, 0, 1, 1, 3 - 2*Sqrt[2]] - 4*ep*G[0, 1, 0, -1, -3 + 2*Sqrt[2]] - 
   (2*Log[2])/ep + Zeta[3] + (8*ep*Zeta[3])/3 - 2*Sqrt[2]*ep*Zeta[3] + 
   I*ep*Pi*Zeta[3] - 2*ep*G[0, 2]*Zeta[3] + 4*ep*G[0, 2*(-1 + Sqrt[2])]*
    Zeta[3], m[51] -> (Pi^2*Log[-1 + Sqrt[2]])/6 - 
   (I/2)*Pi*Log[-1 + Sqrt[2]]^2 - Log[-1 + Sqrt[2]]^3/3 - 
   PolyLog[3, 3 - 2*Sqrt[2]]/2 + Zeta[3]/2, 
 m[52] -> -5/2 + 1/(2*ep^2) + 1/(2*ep) - (35*ep)/2 + (7*Pi^2)/12 + 
   (ep*Pi^2)/12 + (97*ep*Pi^4)/960 - (81*ep*GR[0, 0, -(-1)^(1/3), 1])/8 - 
   (27*ep*GR[0, 0, -(-1)^(2/3), -1])/2 - (Pi^2*Log[2])/2 + 
   (ep*Pi^2*Log[2])/2 - (ep*Pi^2*Log[2]^2)/2 + (7*Zeta[3])/4 + 
   (143*ep*Zeta[3])/12 - (21*ep*Log[2]*Zeta[3])/2, 
 m[53] -> (7*ep*Pi^4)/2880 - (27*ep*GR[0, 0, -(-1)^(1/3), 1])/8 - 
   (9*ep*GR[0, 0, -(-1)^(2/3), -1])/2 - (7*Zeta[3])/4 - 
   (7*ep*Log[2]*Zeta[3])/2, m[54] -> Pi^2/16 + (443*ep^2*Pi^4)/11520 + 
   (27*ep^2*GR[0, 0, -(-1)^(1/3), 1])/32 + (9*ep^2*GR[0, 0, -(-1)^(2/3), -1])/
    8 - (ep*Pi^2*Log[2])/8 + (ep^2*Pi^2*Log[2]^2)/8 + (35*ep*Zeta[3])/16 + 
   (7*ep^2*Log[2]*Zeta[3])/8, m[55] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + 
   (65*ep)/2 + (211*ep^2)/2 + (4*I)*Pi + (I*Pi)/ep + (12*I)*ep*Pi + 
   (32*I)*ep^2*Pi - (13*Pi^2)/12 - (47*ep*Pi^2)/12 - (121*ep^2*Pi^2)/12 - 
   (I/3)*ep*Pi^3 - ((4*I)/3)*ep^2*Pi^3 - (7*ep^2*Pi^4)/80 - 8*Log[2] - 
   (2*Log[2])/ep - 24*ep*Log[2] - 64*ep^2*Log[2] - (4*I)*Pi*Log[2] - 
   (16*I)*ep*Pi*Log[2] - (48*I)*ep^2*Pi*Log[2] + (13*ep*Pi^2*Log[2])/3 + 
   (52*ep^2*Pi^2*Log[2])/3 + ((4*I)/3)*ep^2*Pi^3*Log[2] + 4*Log[2]^2 + 
   16*ep*Log[2]^2 + 48*ep^2*Log[2]^2 + (8*I)*ep*Pi*Log[2]^2 + 
   (32*I)*ep^2*Pi*Log[2]^2 - (20*ep^2*Pi^2*Log[2]^2)/3 - (16*ep*Log[2]^3)/3 - 
   (64*ep^2*Log[2]^3)/3 - ((32*I)/3)*ep^2*Pi*Log[2]^3 + 
   (10*ep^2*Log[2]^4)/3 - 48*ep^2*PolyLog[4, 1/2] - (77*ep*Zeta[3])/6 - 
   (143*ep^2*Zeta[3])/3 - ((14*I)/3)*ep^2*Pi*Zeta[3] + 
   (28*ep^2*Log[2]*Zeta[3])/3, m[56] -> 19/2 + 1/(2*ep^2) + 5/(2*ep) + 
   (65*ep)/2 + (211*ep^2)/2 + (5*I)*Pi + (I*Pi)/ep + (19*I)*ep*Pi + 
   (65*I)*ep^2*Pi - (11*Pi^2)/12 - (55*ep*Pi^2)/12 - (209*ep^2*Pi^2)/12 - 
   (I/2)*ep*Pi^3 - ((5*I)/2)*ep^2*Pi^3 + (79*ep^2*Pi^4)/720 - 10*Log[2] - 
   (2*Log[2])/ep - 38*ep*Log[2] - 130*ep^2*Log[2] - (4*I)*Pi*Log[2] - 
   (20*I)*ep*Pi*Log[2] - (76*I)*ep^2*Pi*Log[2] + (11*ep*Pi^2*Log[2])/3 + 
   (55*ep^2*Pi^2*Log[2])/3 + (2*I)*ep^2*Pi^3*Log[2] + 4*Log[2]^2 + 
   20*ep*Log[2]^2 + 76*ep^2*Log[2]^2 + (8*I)*ep*Pi*Log[2]^2 + 
   (40*I)*ep^2*Pi*Log[2]^2 - (22*ep^2*Pi^2*Log[2]^2)/3 - (16*ep*Log[2]^3)/3 - 
   (80*ep^2*Log[2]^3)/3 - ((32*I)/3)*ep^2*Pi*Log[2]^3 + 
   (16*ep^2*Log[2]^4)/3 - (13*ep*Zeta[3])/3 - (65*ep^2*Zeta[3])/3 - 
   ((26*I)/3)*ep^2*Pi*Zeta[3] + (52*ep^2*Log[2]*Zeta[3])/3, 
 m[57] -> F[57, 0] + ep*F[57, 1], m[58] -> (-1/8*I)*Pi^3 - (I/4)*ep*Pi^3 + 
   (1307*ep*Pi^4)/5760 - (297*ep*GR[0, 0, -(-1)^(1/3), 1])/16 - 
   (99*ep*GR[0, 0, -(-1)^(2/3), -1])/4 - (Pi^2*Log[2])/2 - ep*Pi^2*Log[2] + 
   (I/4)*ep*Pi^3*Log[2] + (21*Zeta[3])/8 + (21*ep*Zeta[3])/4 - 
   ((7*I)/4)*ep*Pi*Zeta[3] - (63*ep*Log[2]*Zeta[3])/4, 
 m[59] -> 37/8 + 1/(2*ep^2) + 7/(4*ep) + (175*ep)/16 + (781*ep^2)/32 + 
   Pi^2/6 + (7*ep*Pi^2)/12 + (37*ep^2*Pi^2)/24 + (11*ep^2*Pi^4)/240 - 
   (19*ep*Zeta[3])/12 - (133*ep^2*Zeta[3])/24, 
 m[60] -> -9/2 - 1/(2*ep^2) - 3/(2*ep) - (27*ep)/2 - (81*ep^2)/2 + 
   (19*ep^2*Pi^4)/240 + 6*Log[2] + (2*Log[2])/ep + 18*ep*Log[2] + 
   54*ep^2*Log[2] - 4*Log[2]^2 - 12*ep*Log[2]^2 - 36*ep^2*Log[2]^2 + 
   (16*ep*Log[2]^3)/3 + 16*ep^2*Log[2]^3 - (16*ep^2*Log[2]^4)/3 + 
   (61*ep*Zeta[3])/12 + (61*ep^2*Zeta[3])/4 - (61*ep^2*Log[2]*Zeta[3])/3, 
 m[61] -> 11/8 + 1/(2*ep^2) + 5/(4*ep) - (55*ep)/16 - (949*ep^2)/32 + 
   (5*Pi^2)/12 + (25*ep*Pi^2)/24 + (55*ep^2*Pi^2)/48 + (101*ep^2*Pi^4)/240 + 
   (11*ep*Zeta[3])/3 + (55*ep^2*Zeta[3])/6, 
 m[62] -> -13/2 - ep^(-1) - (115*ep)/4 - (865*ep^2)/8 - (5971*ep^3)/16 - 
   (2*I)*Pi - (13*I)*ep*Pi - ((115*I)/2)*ep^2*Pi - ((865*I)/4)*ep^3*Pi + 
   (13*ep*Pi^2)/6 + (169*ep^2*Pi^2)/12 + (1495*ep^3*Pi^2)/24 + 
   ((5*I)/3)*ep^2*Pi^3 + ((65*I)/6)*ep^3*Pi^3 - (101*ep^3*Pi^4)/120 + 
   4*Log[2] + 26*ep*Log[2] + 115*ep^2*Log[2] + (865*ep^3*Log[2])/2 + 
   (8*I)*ep*Pi*Log[2] + (52*I)*ep^2*Pi*Log[2] + (230*I)*ep^3*Pi*Log[2] - 
   (26*ep^2*Pi^2*Log[2])/3 - (169*ep^3*Pi^2*Log[2])/3 - 
   ((20*I)/3)*ep^3*Pi^3*Log[2] - 8*ep*Log[2]^2 - 52*ep^2*Log[2]^2 - 
   230*ep^3*Log[2]^2 - (16*I)*ep^2*Pi*Log[2]^2 - (104*I)*ep^3*Pi*Log[2]^2 + 
   (52*ep^3*Pi^2*Log[2]^2)/3 + (32*ep^2*Log[2]^3)/3 + (208*ep^3*Log[2]^3)/3 + 
   ((64*I)/3)*ep^3*Pi*Log[2]^3 - (32*ep^3*Log[2]^4)/3 + (32*ep^2*Zeta[3])/3 + 
   (208*ep^3*Zeta[3])/3 + ((64*I)/3)*ep^3*Pi*Zeta[3] - 
   (128*ep^3*Log[2]*Zeta[3])/3, m[63] -> 59/8 + 3/(2*ep^2) + 17/(4*ep) + 
   (65*ep)/16 - (1117*ep^2)/32 + Pi^2/4 + (49*ep*Pi^2)/24 + 
   (475*ep^2*Pi^2)/48 + (7*ep^2*Pi^4)/240 - 8*ep^2*Pi^2*Log[2] - ep*Zeta[3] + 
   (151*ep^2*Zeta[3])/6, m[64] -> 3/(2*ep^2) + 19/(4*ep) + F[64, 0] + 
   ep*F[64, 1] + ep^2*F[64, 2], m[65] -> 1/(2*ep^2) + 1/(2*ep) + F[65, 0] + 
   ep*F[65, 1] + ep^2*F[65, 2], m[66] -> -1/2 + ep^(-2) + 2/ep - (85*ep)/4 - 
   (907*ep^2)/8 + (11*Pi^2)/12 + (17*ep*Pi^2)/24 - (373*ep^2*Pi^2)/48 + 
   (5663*ep^2*Pi^4)/2880 + (243*ep^2*GR[0, 0, -(-1)^(1/3), 1])/8 + 
   (81*ep^2*GR[0, 0, -(-1)^(2/3), -1])/2 + (3*ep*Pi^2*Log[2])/2 + 
   (3*ep^2*Pi^2*Log[2])/4 + (3*ep^2*Pi^2*Log[2]^2)/2 + (181*ep*Zeta[3])/12 + 
   (157*ep^2*Zeta[3])/24 + (63*ep^2*Log[2]*Zeta[3])/2, m[67] -> Pi^4/64, 
 m[68] -> -1/4*Pi^2 - Pi^2/(8*ep) - (ep*Pi^2)/2 - (ep*Pi^4)/24 + 
   (Pi^2*Log[2])/4 + (ep*Pi^2*Log[2])/2 - (ep*Pi^2*Log[2]^2)/4 - 
   (7*Zeta[3])/4 - (7*ep*Zeta[3])/2 + (7*ep*Log[2]*Zeta[3])/2, 
 m[69] -> -1/8*Pi^2 - Pi^2/(8*ep) - (ep*Pi^2)/8 - (ep*Pi^4)/16 - 
   (7*Zeta[3])/4 - (7*ep*Zeta[3])/4, 
 m[70] -> -1/4*Pi^2 - Pi^2/(8*ep) - (ep*Pi^2)/2 - (I/8)*Pi^3 - 
   (I/4)*ep*Pi^3 + (ep*Pi^4)/48 + (Pi^2*Log[2])/4 + (ep*Pi^2*Log[2])/2 + 
   (I/4)*ep*Pi^3*Log[2] - (ep*Pi^2*Log[2]^2)/4 - (7*Zeta[3])/4 - 
   (7*ep*Zeta[3])/2 - ((7*I)/4)*ep*Pi*Zeta[3] + (7*ep*Log[2]*Zeta[3])/2, 
 m[71] -> -1/8*Pi^2 - Pi^2/(8*ep) - (ep*Pi^2)/8 - (ep*Pi^4)/16 - 
   (7*Zeta[3])/4 - (7*ep*Zeta[3])/4, 
 m[72] -> 12 + ep^(-2) + 4/ep + 32*ep + 80*ep^2 - Pi^2/6 - (2*ep*Pi^2)/3 - 
   2*ep^2*Pi^2 - (7*ep^2*Pi^4)/120 - 16*Log[2] - (4*Log[2])/ep - 
   48*ep*Log[2] - 128*ep^2*Log[2] + (2*ep*Pi^2*Log[2])/3 + 
   (8*ep^2*Pi^2*Log[2])/3 + 8*Log[2]^2 + 32*ep*Log[2]^2 + 96*ep^2*Log[2]^2 - 
   (4*ep^2*Pi^2*Log[2]^2)/3 - (32*ep*Log[2]^3)/3 - (128*ep^2*Log[2]^3)/3 + 
   (32*ep^2*Log[2]^4)/3 - (14*ep*Zeta[3])/3 - (56*ep^2*Zeta[3])/3 + 
   (56*ep^2*Log[2]*Zeta[3])/3, m[73] -> 7 + ep^(-2) + 3/ep + 15*ep + 
   31*ep^2 - (ep^2*Pi^4)/30 - 6*Log[2] - (2*Log[2])/ep - 14*ep*Log[2] - 
   30*ep^2*Log[2] + 2*Log[2]^2 + 6*ep*Log[2]^2 + 14*ep^2*Log[2]^2 - 
   (4*ep*Log[2]^3)/3 - 4*ep^2*Log[2]^3 + (2*ep^2*Log[2]^4)/3 - 
   (8*ep*Zeta[3])/3 - 8*ep^2*Zeta[3] + (16*ep^2*Log[2]*Zeta[3])/3, 
 m[74] -> 7 + ep^(-2) + 3/ep + 15*ep + 31*ep^2 + (3*I)*Pi + (I*Pi)/ep + 
   (7*I)*ep*Pi + (15*I)*ep^2*Pi - Pi^2/2 - (3*ep*Pi^2)/2 - (7*ep^2*Pi^2)/2 - 
   (I/6)*ep*Pi^3 - (I/2)*ep^2*Pi^3 + (ep^2*Pi^4)/120 - 6*Log[2] - 
   (2*Log[2])/ep - 14*ep*Log[2] - 30*ep^2*Log[2] - (2*I)*Pi*Log[2] - 
   (6*I)*ep*Pi*Log[2] - (14*I)*ep^2*Pi*Log[2] + ep*Pi^2*Log[2] + 
   3*ep^2*Pi^2*Log[2] + (I/3)*ep^2*Pi^3*Log[2] + 2*Log[2]^2 + 6*ep*Log[2]^2 + 
   14*ep^2*Log[2]^2 + (2*I)*ep*Pi*Log[2]^2 + (6*I)*ep^2*Pi*Log[2]^2 - 
   ep^2*Pi^2*Log[2]^2 - (4*ep*Log[2]^3)/3 - 4*ep^2*Log[2]^3 - 
   ((4*I)/3)*ep^2*Pi*Log[2]^3 + (2*ep^2*Log[2]^4)/3 - (8*ep*Zeta[3])/3 - 
   8*ep^2*Zeta[3] - ((8*I)/3)*ep^2*Pi*Zeta[3] + (16*ep^2*Log[2]*Zeta[3])/3, 
 m[75] -> 12 + ep^(-2) + 4/ep + 32*ep + 80*ep^2 + (8*I)*Pi + ((2*I)*Pi)/ep + 
   (24*I)*ep*Pi + (64*I)*ep^2*Pi - (13*Pi^2)/6 - (26*ep*Pi^2)/3 - 
   26*ep^2*Pi^2 - ((5*I)/3)*ep*Pi^3 - ((20*I)/3)*ep^2*Pi^3 + 
   (113*ep^2*Pi^4)/120 - 16*Log[2] - (4*Log[2])/ep - 48*ep*Log[2] - 
   128*ep^2*Log[2] - (8*I)*Pi*Log[2] - (32*I)*ep*Pi*Log[2] - 
   (96*I)*ep^2*Pi*Log[2] + (26*ep*Pi^2*Log[2])/3 + (104*ep^2*Pi^2*Log[2])/3 + 
   ((20*I)/3)*ep^2*Pi^3*Log[2] + 8*Log[2]^2 + 32*ep*Log[2]^2 + 
   96*ep^2*Log[2]^2 + (16*I)*ep*Pi*Log[2]^2 + (64*I)*ep^2*Pi*Log[2]^2 - 
   (52*ep^2*Pi^2*Log[2]^2)/3 - (32*ep*Log[2]^3)/3 - (128*ep^2*Log[2]^3)/3 - 
   ((64*I)/3)*ep^2*Pi*Log[2]^3 + (32*ep^2*Log[2]^4)/3 - (14*ep*Zeta[3])/3 - 
   (56*ep^2*Zeta[3])/3 - ((28*I)/3)*ep^2*Pi*Zeta[3] + 
   (56*ep^2*Log[2]*Zeta[3])/3, m[76] -> 3 + ep^(-2) + 2/ep + 4*ep + 5*ep^2 + 
   Pi^2/6 + (ep*Pi^2)/3 + (ep^2*Pi^2)/2 + (7*ep^2*Pi^4)/360 - 
   (2*ep*Zeta[3])/3 - (4*ep^2*Zeta[3])/3}
